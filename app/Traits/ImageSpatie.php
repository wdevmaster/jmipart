<?php

namespace App\Traits;

trait ImageSpatie
{
    /**
     * Returns whether the item has an image
     *
     * @return bool
     */
    public function hasImage()
    {
        return $this->getMedia()->isNotEmpty();
    }

    /**
     * Returns the URL of the item's thumbnail image, or null if there's no image
     *
     * @return string|null
     */
    public function getThumbnailUrl()
    {
        return $this->getFirstMediaUrl('default', 'thumbnail');
    }

    /**
     * Returns the URL of the item's medium image, or null if there's no image
     *
     * @return string|null
     */
    public function getMediumUrl()
    {
        return $this->getFirstMediaUrl('default', 'medium');
    }

    /**
     * Returns the URL of the item's Large image, or null if there's no image
     *
     * @return string|null
     */
    public function getLargeUrl()
    {
        return $this->getFirstMediaUrl('default', 'large');
    }

    /**
     * Returns the URL of the item's (main) image, or null if there's no image
     *
     * @return string|null
     */
    public function getImageUrl()
    {
        return $this->getFirstMediaUrl();
    }
}
