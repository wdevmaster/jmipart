<?php

namespace App;

use Konekt\Enum\Enum; 

class ProductCondition extends Enum
{
    const __default    = self::NEW;

    const NEW               = 'new';
    const USED              = 'used';
    const ANTIQUE           = 'antique';
    const REFURBISHED       = 'refurbished';
    const REMABUFACTURED    = 'remanufactured';

    protected static $labels = [];

    protected static function boot()
    {
        static::$labels = [
            self::NEW               => __('New'),
            self::USED              => __('Used'),
            self::ANTIQUE           => __('Antique'),
            self::REFURBISHED       => __('Refurbished'),
            self::REMABUFACTURED    => __('Remanufactured'),
        ];
    }
}
