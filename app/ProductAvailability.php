<?php

namespace App;

use Konekt\Enum\Enum; 

class ProductAvailability extends Enum
{
    const __default         = self::INVENTORY;

    const AVAILABLE         = 'avaible';
    const OUT_OF_STOCK      = 'out_of_stock';
    const SOLD              = 'sold';
    const LIMITED_SUPPLY    = 'limited_supply';
    const PLEASE_CALL       = 'please_call';
    const INVENTORY         = 'inventory';
    const NOT_FOR_SALE      = 'not_for_sale';
    const TO_BE_DELETED     = 'to_be_deleted';

    protected static $labels = [];

    protected static function boot()
    {
        static::$labels = [
            self::AVAILABLE         => __('Avaible'),
            self::OUT_OF_STOCK      => __('Out of stock'),
            self::SOLD              => __('Sold'),
            self::LIMITED_SUPPLY    => __('Limited supply'),
            self::PLEASE_CALL       => __('Please call'),
            self::INVENTORY         => __('Inventory'),
            self::NOT_FOR_SALE      => __('Not for sale'),
            self::TO_BE_DELETED     => __('To be deleted')
        ];
    }
}
