<?php

namespace App\Helpers;

class Helper 
{
    public static function shorten(String $string)
    {
        return strtolower(substr(strrchr($string, "\\"), 1));
    }
}
