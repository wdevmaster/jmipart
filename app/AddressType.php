<?php

namespace App;

use Konekt\Enum\Enum;

class AddressType extends Enum 
{
    const __default = self::BILLING;

    const BILLING       = 'billing';
    const SHIPPING      = 'shipping';
    const BUSINESS      = 'business';
    const CONTRACT      = 'contract';
    const MAILING       = 'mailing';
    const PICKUP        = 'pickup';
    const RESIDENTIAL   = 'residential';

    protected static $labels = [];

    protected static function boot()
    {
        static::$labels = [
            self::BILLING     => __('Billing'),
            self::BUSINESS    => __('Business'),
            self::CONTRACT    => __('Contract'),
            self::MAILING     => __('Mailing'),
            self::PICKUP      => __('Pickup'),
            self::RESIDENTIAL => __('Residential'),
            self::SHIPPING    => __('Shipping'),
        ];
    }
}
