<?php

namespace App;

use App\Taxon;
use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class Taxonomy extends Model
{
    use Sluggable, SluggableScopeHelpers;

    protected $table = 'taxonomies';

    protected $guarded = ['id', 'created_at', 'updated_at'];

    public static function findOneByName(string $name)
    {
        return static::where('name', $name)->first();
    }

    public function rootLevelTaxons(): Collection
    {
        return Taxon::byTaxonomy($this)
                         ->roots()
                         ->sort()
                         ->get();
    }
    
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }
}
