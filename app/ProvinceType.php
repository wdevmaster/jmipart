<?php

namespace App;

use Konekt\Enum\Enum;

class ProvinceType extends Enum
{
    const __default        = self::PROVINCE;

    const STATE            = 'state';
    const REGION           = 'region';
    const PROVINCE         = 'province';
    const COUNTY           = 'county';
    const TERRITORY        = 'territory';
    const FEDERAL_DISTRICT = 'federal_district';
    const MILITARY         = 'military';
    const UNIT             = 'unit';
    const MUNICIPALITY     = 'municipality';

    protected static $labels = [];

    protected static function boot()
    {
        static::$labels = [
            self::STATE            => __('State'),
            self::REGION           => __('Region'),
            self::PROVINCE         => __('Province'),
            self::COUNTY           => __('County'),
            self::TERRITORY        => __('Territory'),
            self::FEDERAL_DISTRICT => __('Federal District'),
            self::MILITARY         => __('Military'),
            self::UNIT             => __('Geographical Unit'),
            self::MUNICIPALITY     => __('Municipality'),
        ];
    }
}
