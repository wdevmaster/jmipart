<?php

namespace App;

use Konekt\Enum\Enum;

class ProductState extends Enum
{
    const __default = self::DRAFT;

    const DRAFT       = 'draft';
    const INACTIVE    = 'inactive';
    const ACTIVE      = 'active';

    protected static $labels = [];

    protected static $activeStates = [self::ACTIVE];

    protected static function boot()
    {
        static::$labels = [
            self::DRAFT         => __('Draft'),
            self::INACTIVE      => __('Inactive'),
            self::ACTIVE        => __('Active'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function isActive()
    {
        return in_array($this->value, static::$activeStates);
    }
    /**
     * @inheritdoc
     */
    public static function getActiveStates()
    {
        return static::$activeStates;
    }
}