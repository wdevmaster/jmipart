<?php

namespace App\Rules;

use App\User;
use App\Customer;
use Illuminate\Contracts\Validation\Rule;

class UserActiveRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $row = Customer::findByEmail($value);

        if (!$row)
            $row = User::findByEmail($value);

        if ($row)
            return boolval($row->is_active);

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('Your account seems to be inactive.');
    }
}
