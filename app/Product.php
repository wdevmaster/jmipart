<?php

namespace App;

use Spatie\MediaLibrary\Models\Media;
use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Konekt\Enum\Eloquent\CastsEnums;
use App\Traits\ImageSpatie;
use App\Traits\HasTaxons;
use Components\Contracts\Buyable;

class Product extends Model implements HasMedia, Buyable
{
    use CastsEnums, Sluggable, SluggableScopeHelpers, HasMediaTrait, HasTaxons, ImageSpatie;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'products';

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id', 'created_at', 'updated_at'];

    protected $enums = [
        'state' => ProductState::class,
        'condition' => ProductCondition::class,
        'availability' => ProductAvailability::class,
    ];

    public static function boot()
    {
        parent::boot();

        static::creating(function($product) {
            $product->state = ProductState::ACTIVE;
        });
    }

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    public function registerMediaConversions(Media $media = null)
    {
        foreach (config('images.variants') as $name => $settings) {
            $this->addMediaConversion($name)
                 ->width($settings['width'])
                 ->height($settings['height']);
        }
    }

    public function getRouteKeyName()
    {
        return $this->getSlugKeyName();
    }
    
    /**
     * @inheritdoc
     */
    public function isActive()
    {
        return $this->state->isActive();
    }

    /**
     * @return bool
     */
    public function getIsActiveAttribute()
    {
        return $this->isActive();
    }

    public function isOnStock(): bool
    {
        return $this->stock > 0;
    }

    public function getIdStr()
    {
        return implode(',', $this->taxons->pluck('id')->toArray());
    }

    /**
     * @return string
     */
    public function getTitleAttribute()
    {
        return $this->title();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getPrice(): float
    {
        return number_format($this->price, 2);
    }

    public function getFullNameAttribute()
    {
        return $this->price;
    }

    /**
     * Scope for returning the products with active state
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActives($query)
    {
        return $query->whereIn(
            'state',
            ProductState::getActiveStates()
        );
    }
}
