<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Components\Cart\Contracts\CartItem as CartItemContract;
use Components\Contracts\Buyable;
use App\Product;

class CartItem extends Model implements CartItemContract
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cart_items';
    
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id', 'created_at', 'updated_at'];


    public function product()
    {
        return $this->morphTo();
    }

    /**
     * @inheritDoc
     */
    public function getBuyable(): Buyable
    {
        return $this->product;
    }

    /**
     * @inheritDoc
     */
    public function getNameAttribute()
    {
        return $this->product->name;
    }

    /**
     * @inheritDoc
     */
    public function getThumbnailUrl()
    {
        return $this->product->getThumbnailUrl();
    }

    /**
     * @inheritDoc
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @inheritDoc
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @inheritDoc
     */
    public function total()
    {
        return $this->price * $this->quantity;
    }

    /**
     * Property accessor alias to the total() method
     *
     * @return float
     */
    public function getTotalAttribute()
    {
        return $this->total();
    }

    public function getTotalFormat()
    {
        return number_format($this->total(), 2);
    }

    /**
     * Scope to query items of a cart
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param mixed $cart Cart object or cart id
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeOfCart($query, $cart)
    {
        $cartId = is_object($cart) ? $cart->id : $cart;
        return $query->where('cart_id', $cartId);
    }
    /**
     * Scope to query items by product (Buyable)
     *
     * @param Builder $query
     * @param Buyable $product
     *
     * @return Builder
     */
    public function scopeByProduct($query, Product $product)
    {
        return $query->where([
            ['product_id', '=', $product->getId()],
            ['product_type', '=', get_class($product)]
        ]);
    }
}
