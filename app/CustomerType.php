<?php

namespace App;

use Konekt\Enum\Enum;

class CustomerType extends Enum
{
    const __default    = self::INDIVIDUAL;

    const INDIVIDUAL   = 'individual';
    const ORGANIZATION = 'organization';

    protected static $labels = [];

    protected static function boot()
    {
        static::$labels = [
            self::INDIVIDUAL   => __('Individual'),
            self::ORGANIZATION => __('Organization')
        ];
    }

    
}
