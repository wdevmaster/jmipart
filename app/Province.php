<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Konekt\Enum\Eloquent\CastsEnums;
use Illuminate\Support\Str;

class Province extends Model
{
    use CastsEnums;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'provinces';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    protected $enums = [
        'type' => ProvinceType::class
    ];

    /**
     * Relationship to the country the address belongs to
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function country()
    {
        return $this->belongsTo('App\Country', 'country_id');
    }

    /**
     * Relationship with the province to which it belongs
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parent()
    {
        return $this->belongsTo('App\Province', 'parent_id');
    }

    public function removeParent()
    {
        $this->parent()->dissociate();
    }

    public function setParent(Province $province)
    {
        $this->parent()->associate($province);
    }

    public function hasParent()
    {
        return null !== $this->parent_id;
    }

    /**
     * Relationship with the region/state/city it contains
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function children()
    {
        return $this->hasMany('App\Province', 'parent_id');
    }

    public static function findByCountryAndCode($country, string $code)
    {
        $country = is_object($country) ? $country->id : $country;
        return Province::byCountry($country)
                       ->where('code', $code)
                       ->take(1)
                       ->get()
                       ->first();
    }

    public function scopeByCountry($query, $country)
    {
        $country = is_object($country) ? $country->id : $country;
        return $query->where('country_id', $country);
    }

    public function scopeByType($query, $provinceType)
    {
        $type = is_object($provinceType) ? $provinceType->value() : $provinceType;
        return $query->where('type', $type);
    }
    
    public function scopeSortByName($query)
    {
        return $query->orderBy('name');
    }
}
