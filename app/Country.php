<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'countries';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['created_at', 'updated_at'];

    protected $casts = [
        'is_eu_member' => 'bool'
    ];

    public function provinces()
    {
        return $this->hasMany('App\Province', 'country_id', 'id');
    }

    public function states()
    {
        return $this->provinces()->byType(ProvinceType::STATE());
    }
    public function counties()
    {
        return $this->provinces()->byType(ProvinceType::COUNTY());
    }
    public function regions()
    {
        return $this->provinces()->byType(ProvinceType::REGION());
    }
    public function territories()
    {
        return $this->provinces()->byType(ProvinceType::TERRITORY());
    }
    public function units()
    {
        return $this->provinces()->byType(ProvinceType::UNIT());
    }
}
