<?php

namespace App;

use Konekt\Enum\Enum;

class CartState extends Enum 
{
    const __default  = self::ACTIVE;

    const ACTIVE     = 'active';
    const CHECKOUT   = 'checkout';
    const COMPLETED  = 'completed';
    const ABANDONDED = 'abandoned';

    protected static $labels = [];

    protected static $activeStates = [self::ACTIVE, self::CHECKOUT];

    protected static function boot()
    {
        static::$labels = [
            self::ACTIVE     => __('Active'),
            self::CHECKOUT   => __('Checkout'),
            self::COMPLETED  => __('Completed'),
            self::ABANDONDED => __('Abandoned')
        ];
    }

    /**
     * @inheritDoc
     */
    public function isActive(): bool
    {
        return in_array($this->value, static::$activeStates);
    }
    
    /**
     * @inheritDoc
     */
    public static function getActiveStates(): array
    {
        return static::$activeStates;
    }
}
