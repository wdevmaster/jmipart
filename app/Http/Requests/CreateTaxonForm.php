<?php

namespace App\Http\Requests;

use App\Taxon;
use Illuminate\Foundation\Http\FormRequest;

class CreateTaxonForm extends FormRequest
{
    protected $defaultParent = false;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'parent' => 'sometimes|exists:' . app(Taxon::class)->getTable() . ',id'
        ];
    }

    /**
     * @inheritdoc
     */
    public function getDefaultParent()
    {
        if ($id = $this->query('parent')) {
            return Taxon::find($id);
        }
        return null;
    }

    public function getNextPriority(Taxon $taxon): int
    {
        // Workaround due to `neighbours` relation not working on root level taxons
        if ($taxon->isRootLevel()) {
            $lastNeighbour = Taxon::byTaxonomy($taxon->taxonomy_id)->roots()->sortReverse()->first();
        } else {
            $lastNeighbour = $taxon->lastNeighbour();
        }
        return $lastNeighbour ? $lastNeighbour->priority + 10 : 10;
    }
}
