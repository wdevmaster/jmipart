<?php

namespace App\Http\Requests;

use App\ProductState;
use App\ProductCondition;
use App\ProductAvailability;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class UpdateProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:2|max:255',
            'sku'  => [
                'required',
                Rule::unique('products')->ignore($this->route('product')),
                ],
            'condition'     => ['required', Rule::in(ProductCondition::values())],
            'availability'  => ['required', Rule::in(ProductAvailability::values())],
            'price'         => 'required',
            'stock'         => 'nullable|numeric',
            'taxons'        => 'required|string',
            'delete-img'    => 'nullable|string',
        ];
    }

    public function getTaxons()
    {
        $strTaxons = $this->get('taxons');
        return $strTaxons ? explode(',', $strTaxons) : [];
    }

    public function getMedias()
    {
        $strMedia = $this->get('delete-img');
        return $strMedia ? explode(',', $strMedia) : [];
    }
}
