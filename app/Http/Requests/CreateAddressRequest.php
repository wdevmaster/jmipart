<?php

namespace App\Http\Requests;

use App\Province;
use App\Customer;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class CreateAddressRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'for'       => ['sometimes', Rule::in(['customer'])],
            'forId'         => 'required_with:for',
            'name'          => 'required|min:2|max:255',
            'country_id'    => 'required',
            'province_code' => 'string',
            'province'      => 'required',
            'city'          => 'required|string',
            'postalcode'    => 'required|string',
            'address'       => 'required|string|min:2|max:255',
        ];
    }

    public function getFor()
    {
        if ($id = $this->input('forId')) {
            return Customer::find($id);
        }
        return null;
    }

    public function getProvince()
    {
        if ($code = $this->input('province_code') ) {
            return Province::firstOrCreate(['code' => $code], [
                'country_id' => $this->input('country_id'),
                'name' => $this->input('province')
            ]);
        }
        return null;
    }
}
