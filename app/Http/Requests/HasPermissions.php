<?php

namespace App\Http\Requests;

trait HasPermissions
{
    /**
     * Returns the permissions array
     *
     * @return array
     */
    public function permissions()
    {
        $perms = $this->get('permissions');
        return is_array($perms) ? array_keys($perms) : [];
    }
}
