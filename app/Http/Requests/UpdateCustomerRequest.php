<?php

namespace App\Http\Requests;

use App\CustomerType;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class UpdateCustomerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type'          => ['required', Rule::in(CustomerType::values())],
            'firstname'     => 'required_if:type,individual',
            'lastname'      => 'required_if:type,individual',
            'company_name'  => 'required_if:type,organization',
            'email'         => 'required|email',
            'password'      => 'nullable|min:6',
        ];
    }
}
