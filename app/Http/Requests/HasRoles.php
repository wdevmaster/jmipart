<?php

namespace App\Http\Requests;

trait HasRoles
{
    /**
     * Returns the roles array
     *
     * @return array
     */
    public function roles()
    {
        $roles = $this->get('roles');
        return is_array($roles) ? array_keys($roles) : [];
    }
}
