<?php

namespace App\Http\Controllers;

use App\Customer;
use App\CustomerType;
use Illuminate\Http\Request;
use App\Http\Requests\CreateCustomerRequest as CreateCustomer;
use App\Http\Requests\UpdateCustomerRequest as UpdateCustomer;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.customer.index', [
            'customers' => Customer::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $customer = app(Customer::class);
        
        return view('admin.customer.create', [
            'customer' => $customer,
            'types' => CustomerType::choices()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateCustomer $request)
    {
        $request->merge([
            'password'  => bcrypt($request->get('password')),
        ]);

        try {
            Customer::create($request->all());

            return redirect()->route('customer.index')
                    ->with('success', __('Customer has been created'));

        } catch (\Exception $e) {
            return redirect()->back()
                    ->with('error', __('Error: :msg', ['msg' => $e->getMessage()]));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(!$customer = Customer::find($id))  
            return redirect()->route('customer.index')
                    ->with('error', __('The customer was not found'));

        return view('admin.customer.show', [
            'customer' => $customer
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(!$customer = Customer::find($id))  
            return redirect()->route('customer.index')
                    ->with('error', __('The customer was not found'));

        return view('admin.customer.edit', [
            'customer' => $customer,
            'types'  => CustomerType::choices()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCustomer $request, $id)
    {
        if(!$customer = Customer::find($id))  
            return redirect()->route('customer.index')
                    ->with('error', __('The customer was not found'));

        $data = $request->except(['password']);

        if ($request->has('password'))
            $data['password'] = bcrypt($request->get('password'));

        try {

            $customer->update($data);

            return redirect()->route('customer.index')
                    ->with('success', __('Customer has been updated'));
        } catch (\Exception $e) {
            return redirect()->back()
                    ->with('error', __('Error: :msg', ['msg' => $e->getMessage()]));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        if(!$customer = Customer::find($request->id))
            abort(404);

        $customer->delete();
    }
}
