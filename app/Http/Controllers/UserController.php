<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Caffeinated\Shinobi\Models\Role;
use App\Http\Requests\CreateUserRequest as CreateUser;
use App\Http\Requests\UpdateUserRequest as UpdateUser;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::with('roles')->get();
        return view('admin.user.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::where('slug', 'not like', '%customer%')->get();
        return view('admin.user.create', compact('roles'));
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateUser $request)
    {
        $request->merge([
            'password'  => bcrypt($request->get('password')),
        ]);

        try {
            $user = User::create($request->except('roles'));
            $user->assignRoles($request->roles());

            return redirect()->route('user.index')
                    ->with('success', __('User has been created'));
        } catch (\Exception $e) {
            return redirect()->back()
                    ->with('error', __('Error: :msg', ['msg' => $e->getMessage()]));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(!$user = User::find($id))  
            return redirect()->route('user.index')
                ->with('success', __('The user was not found'));

        $roles = Role::where('slug', 'not like', '%customer%')->get();
        return view('admin.user.edit', compact('roles', 'user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUser $request, $id)
    {
        if(!$user = User::find($id))  
            return redirect()->route('user.index')
                ->with('success', __('The user was not found'));

        $data = $request->except(['password', 'roles']);

        if ($request->has('password'))
            $data['password'] = bcrypt($request->get('password'));
        try {
            $user->update($data);
            $user->syncRoles($request->roles());

            return redirect()->route('user.index')
                    ->with('success', __('User has been updated'));
        } catch (\Exception $e) {
            return redirect()->back()
                    ->with('error', __('Error: :msg', ['msg' => $e->getMessage()]));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        if(!$user = User::find($request->id))
            abort(404);

        $user->delete();
    }
}
