<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateProductRequest as CreateProduct;
use App\Http\Requests\UpdateProductRequest as UpdateProduct;
use Spatie\MediaLibrary\Models\Media;
use App\ProductAvailability;
use Illuminate\Http\Request;
use App\ProductCondition;
use App\ProductState;
use App\Taxonomy;
use App\Taxon;
use App\Product;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.product.index', [
            'products' => Product::paginate(100)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.product.create', [
            'product'       => app(Product::class),
            'states'        => ProductState::choices(),
            'conditions'    => ProductCondition::choices(),
            'availability'  => ProductAvailability::choices(),
            'taxonomies'    => Taxonomy::get()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateProduct $request)
    {
        $request->merge([
            'price'  => floatval($request->get('price')),
        ]);
        try {
            $product = Product::create($request->except('images', 'taxons'));
            $product->addTaxons(Taxon::find($request->getTaxons()));
            $msg = __(':name has been created', ['name' => $product->name]);

            try {
                if (!empty($request->files->filter('images'))) {
                    $product->addMultipleMediaFromRequest(['images'])->each(function ($fileAdder) {
                        $fileAdder->toMediaCollection();
                    });
                }
            } catch (\Exception $e) { 
                return redirect()->back()
                    ->with('error', __('Error: :msg', ['msg' => $e->getMessage()]));
            }
            
            return redirect()->route('product.index')
                        ->with('success', $msg);

        } catch (\Exception $e) {
            return redirect()->back()
                    ->with('error', __('Error: :msg', ['msg' => $e->getMessage()]));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(!$product = Product::find($id))  
                return redirect()
                            ->route('product.index')
                            ->with('error', __('The product was not found'));

        return view('admin.product.show', [
            'product'       => $product,
            'states'        => ProductState::choices(),
            'conditions'    => ProductCondition::choices(),
            'Availability'  => ProductAvailability::choices()
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(!$product = Product::find($id))  
                return redirect()
                            ->route('product.index')
                            ->with('error', __('The product was not found'));

        return view('admin.product.edit', [
            'product'       => $product,
            'states'        => ProductState::choices(),
            'conditions'    => ProductCondition::choices(),
            'availability'  => ProductAvailability::choices(),
            'taxonomies'    => Taxonomy::get()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateProduct $request, $id)
    {
        $request->merge([
            'price'  => floatval($request->get('price')),
        ]);

        try {
            if(!$product = Product::find($id))  
                return redirect()
                            ->route('product.index')
                            ->with('error', __('The product was not found'));

            if ($request->has('delete-img'))  {
                foreach ($request->getMedias() as $media) {
                    Media::find($media)->delete();
                }
            }
                
            $product->update($request->except('images', 'taxons', 'delete-img'));
            $product->taxons()->sync(Taxon::find($request->getTaxons()));
            $msg = __(':name has been updated', ['name' => $product->name]);

            try {
                if (!empty($request->files->filter('images'))) {
                    $product->addMultipleMediaFromRequest(['images'])->each(function ($fileAdder) {
                        $fileAdder->toMediaCollection();
                    });
                }
            } catch (\Exception $e) { 
                return redirect()->back()
                    ->with('error', __('Error: :msg', ['msg' => $e->getMessage()]));
            }
            
            return redirect()->route('product.index')
                        ->with('success', $msg);

        } catch (\Exception $e) {
            return redirect()->back()
                    ->with('error', __('Error: :msg', ['msg' => $e->getMessage()]));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        if(!$product = Product::find($request->id))
            abort(404);

        $product->taxons()->detach();

        $product->delete();
    }
}
