<?php

namespace App\Http\Controllers;

use App\Taxon;
use App\Taxonomy;
use Illuminate\Http\Request;
use App\Http\Requests\CreateTaxonForm;
use App\Http\Requests\CreateTaxonRequest as CreateTaxon;
use App\Http\Requests\UpdateTaxonRequest as UpdateTaxon;

class TaxonController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(CreateTaxonForm $request, Taxonomy $taxonomy)
    {
        $taxon = app(Taxon::class);
        $taxon->taxonomy_id = $taxonomy->id;

        if ($defaultParent = $request->getDefaultParent())
            $taxon->parent_id = $defaultParent->id;
        
        $taxon->priority = $request->getNextPriority($taxon);

        return view('admin.taxon.create', [
            'taxons'   => Taxon::byTaxonomy($taxonomy)->get()->pluck('name', 'id'),
            'taxonomy' => $taxonomy,
            'taxon'    => $taxon
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Taxonomy $taxonomy, CreateTaxon $request)
    {
        try {
            $taxon = Taxon::create(
                array_merge($request->except('image'), ['taxonomy_id' => $taxonomy->id])
            );

            if ($file = $request->file('image'))
                $taxon->addMedia($file)->toMediaCollection();

            return redirect()->route('taxonomy.show', $taxonomy->id)
                    ->with('success', __(':name :taxonomy has been created', [
                        'name' => $taxon->name,
                        'taxonomy' => str_singular($taxonomy->name)
                    ]));
        } catch (\Exception $e) {
            return redirect()->back()
                    ->with('error', __('Error: :msg', ['msg' => $e->getMessage()]));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Taxonomy $taxonomy, $id)
    {
        if(!$taxon = Taxon::find($id))  
            return redirect()->route('taxonomy.show', $taxonomy->id)
                    ->with('error', __('The taxonomy was not found'));

        return view('admin.taxon.edit', [
            'taxons'   => Taxon::byTaxonomy($taxonomy)->get()->pluck('name', 'id'),
            'taxonomy' => $taxonomy,
            'taxon'    => $taxon
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Taxonomy $taxonomy, UpdateTaxon $request, $id)
    {
        if(!$taxon = Taxon::find($id))  
            return redirect()->route('taxonomy.show', $taxonomy->id)
                    ->with('error', __('The taxonomy was not found'));
        
        try {
            $taxon->update($request->except('image'));
            
            if ($file = $request->file('image'))
                $taxon->addMedia($file)
                      ->toMediaCollection();

            return redirect()->route('taxonomy.show', $taxonomy->id)
                    ->with('success', __(':name has been updated', ['name' => $taxon->name]));
        } catch (\Exception $e) {
            return redirect()->back()
                    ->with('error', __('Error: :msg', ['msg' => $e->getMessage()]));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        if(!$taxon = Taxon::find($request->id))
            abort(404);

        $taxon->delete();
    }
}
