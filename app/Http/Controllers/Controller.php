<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function setPermissions($array)
    {
        $elemts = [
            'create'    => 'store',
            'edit'      => 'update'
        ];
        foreach ($array as $row) {
            $slugName =  strrchr($row['slug'], '.');

            foreach ($elemts as $key => $value) {
                if ($slugName == '.'.$key)
                    $array->push(['slug' =>str_replace('.'.$key, '.'.$value, $row['slug'])]);
            }
        }

        return $array->pluck('slug')->toArray();
    }

}
