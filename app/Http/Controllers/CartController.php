<?php

namespace App\Http\Controllers;

use App\Product;
use App\CartItem;
use Illuminate\Http\Request;
use Components\Cart\Facades\Cart;

class CartController extends Controller
{
    public function add(Request $request, Product $product)
    {
        try {
            Cart::addItem($product, $request->qty);

            return redirect()->back()
                    ->with('success', __('name: has been added to cart', [
                        'name' => $product->name
                    ]));
        } catch (\Exception $e) {
            dd($e->getMessage());
            return redirect()->back()
                    ->with('error', __('Error: :msg', ['msg' => $e->getMessage()]));
        }
    }

    public function remove(CartItem $cart_item)
    {
        try {
            Cart::removeItem($cart_item);

            return redirect()->route('cart.show')
                    ->with('success', __('name: as been removed from cart', [
                        'name' => $cart_item->name
                    ]));
        } catch (\Exception $e) {
            return redirect()->back()
                    ->with('error', __('Error: :msg', ['msg' => $e->getMessage()]));
        }
    }

    public function update(CartItem $cart_item, Request $request)
    {
        $qty = (int) $request->get('qty', $cart_item->getQuantity());
        try {
            $cart_item->update([
                'quantity' => $qty
            ]);

            return redirect()->route('cart.show')
                    ->with('success', __('name: as has been updated', [
                        'name' => $cart_item->name
                    ]));
        } catch (\Exception $e) {
            return redirect()->back()
                    ->with('error', __('Error: :msg', ['msg' => $e->getMessage()]));
        }
    }

    public function show()
    {
        return view('cart');
    }
}
