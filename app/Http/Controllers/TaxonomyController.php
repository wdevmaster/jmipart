<?php

namespace App\Http\Controllers;

use App\Taxonomy;
use Illuminate\Http\Request;
use App\Http\Requests\CreateTaxonomyRequest as CreateTaxonomy;
use App\Http\Requests\UpdateTaxonomyRequest as UpdateTaxonomy;

class TaxonomyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.taxonomy.index', [
            'taxonomies' => Taxonomy::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.taxonomy.create', [
            'taxonomy' => app(Taxonomy::class)
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateTaxonomy $request)
    {
        try {
            $taxonomy = Taxonomy::create($request->all());

            return redirect()->route('taxonomy.index')
                    ->with('success', __(':name has been created', ['name' => $taxonomy->name]));

        } catch (\Exception $e) {
            return redirect()->back()
                    ->with('error', __('Error: :msg', ['msg' => $e->getMessage()]));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(!$taxonomy = Taxonomy::find($id))  
            return redirect()->route('taxonomy.index')
                    ->with('error', __('The taxonomy was not found'));

        return view('admin.taxonomy.show', [
            'taxonomy' => $taxonomy
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(!$taxonomy = Taxonomy::find($id))  
            return redirect()->route('taxonomy.index')
                    ->with('error', __('The taxonomy was not found'));

        return view('admin.taxonomy.edit', [
            'taxonomy' => $taxonomy
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateTaxonomy $request, $id)
    {
        if(!$taxonomy = Taxonomy::find($id))  
            return redirect()->route('taxonomy.index')
                    ->with('error', __('The taxonomy was not found'));

        try {
            $taxonomy->update($request->all());

            return redirect()->route('taxonomy.index')
                    ->with('success', __(':name has been updated', ['name' => $taxonomy->name]));

        } catch (\Exception $e) {
            return redirect()->back()
                    ->with('error', __('Error: :msg', ['msg' => $e->getMessage()]));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        if(!$taxonomy = Taxonomy::find($request->id))
            abort(404);

        $taxonomy->delete();
    }
}
