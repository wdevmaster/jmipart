<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Caffeinated\Shinobi\Models\Role;
use Caffeinated\Shinobi\Models\Permission;
use App\Http\Requests\CreateRoleRequest as CreateRole;
use App\Http\Requests\UpdateRoleRequest as UpdateRole;


class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Role::with('users')
                    ->where('slug', 'not like', '%customer%')
                    ->get();
        return view('admin.role.index', compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $permissions = Permission::where('slug', 'not like', '%store%')
                                ->where('slug', 'not like', '%update%')
                                ->get();
        return view('admin.role.create', compact('permissions'));
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateRole $request)
    {
        try {
            $role = Role::create([
                'name' => $request->name,
                'slug' => str_slug($request->name),
            ]);

            $role->givePermissionTo($request->permissions());
            
            return redirect()->route('role.index')
                    ->with('success', __('Role has been created'));
            

        } catch (\Exception $e) {
            
            return redirect()->back()
                    ->with('error', __('Error: :msg', ['msg' => $e->getMessage()]));
            
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(!$role = Role::find($id))  
            return redirect()
                        ->route('role.index')
                        ->with('error', __('The role was not found'));
        
        $permissions = Permission::where('slug', 'not like', '%store%')
                                ->where('slug', 'not like', '%update%')
                                ->get();
                                
        return view('admin.role.edit', compact('role', 'permissions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRole $request, $id)
    {
        try {
            if(!$role = Role::find($id))  
                return redirect()
                            ->route('role.index')
                            ->with('error', __('The role was not found'));

            $role->update([
                'name' => $request->name,
                'slug' => str_slug($request->name),
            ]);
            
            $role->syncPermissions($request->permissions());

            return redirect()
                        ->route('role.index')
                        ->with('success', __('Role has been updated'));

        } catch (\Exception $e) {
            return redirect()->back()
                    ->with('error', __('Error: :msg', ['msg' => $e->getMessage()]));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        if(!$role = Role::find($request->id))  
            abort(404);
        
        $role->users()->update([
            'is_active' => false
        ]);

        $role->delete();
    }
}
