<?php

namespace App\Http\Controllers;

use App\Taxon;
use App\Product;
use App\Taxonomy;
use Illuminate\Http\Request;
use App\Search\ProductFinder;
use App\Http\Requests\ProductIndexRequest;

class WebController extends Controller
{

    /** @var ProductFinder */
    private $productFinder;

    public function __construct(ProductFinder $productFinder)
    {
        $this->productFinder = $productFinder;
    }

    public function welcome()
    {
        return view('welcome', [
            'categories' => Taxonomy::where('slug', 'categories')->first()->rootLevelTaxons(),
            'newProducts' => Taxon::with(['products' => function($query) {
                return $query->latest()->get();
            }])->take(3)->get(),
            'brands' => Taxonomy::where('slug', 'brands')->first()->rootLevelTaxons(),
        ]);
    }

    public function indexProduct(Request $request, $taxon = null)
    {
        $taxonomies = Taxonomy::get();

        if ($taxon = Taxon::findBySelug($taxon)) {
            $this->productFinder->withinTaxon($taxon);
        }

        if ($search = $request->get('search'))
            $this->productFinder->nameContains($search);

        return view('products', [
            'products'   => $this->productFinder->paginate(12),
            'taxonomies' => $taxonomies,
            'taxon'      => $taxon,
        ]);
    }

    
    public function showProduct(Product $product)
    {
        return view('product', ['product' => $product]);
    }
}
