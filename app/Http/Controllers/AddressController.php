<?php

namespace App\Http\Controllers;

use App\Country;
use App\Address;
use App\Province;
use App\AddressType;
use Illuminate\Http\Request;
use App\Http\Requests\CreateAddressFormRequest as CreateAddressForm;
use App\Http\Requests\CreateAddressRequest as CreateAddress;

class AddressController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(CreateAddressForm $request)
    {
        $address = app(Address::class);

        return view('admin.address.create', [
            'address'   => $address,
            'types'     => AddressType::choices(),
            'countries' => Country::all(),
            'provinces' => Province::all(),
            'for'       => $request->getFor()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateAddress $request)
    {
        if ($province = $request->getProvince())
            $request->merge([
                'province_id'  => $province->id,
            ]);
            
        try {
            $address = Address::create($request->except([
                'for', 'forId', 'province_code', 'province'
            ]));

            if ($for = $request->getFor()) {
                $relation = camel_case(str_plural(class_basename(get_class($for))));
                $address->{$relation}()->attach($for->id);

                $message = __('Address has been created for :name', ['name' => $for->name]);
            } else {
                $message = __('Address has been created');
            }

            return redirect()->route('customer.show', $for->id)
                    ->with('success', $message);

        } catch (\Exception $e) {
            return redirect()->back()
                    ->with('error', __('Error: :msg', ['msg' => $e->getMessage()]));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        if(!$address = Address::find($request->id))
            abort(404);

        $address->delete();
    }
}
