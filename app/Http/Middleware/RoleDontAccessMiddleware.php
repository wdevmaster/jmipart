<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RoleDontAccessMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {
        if (Auth::user()->hasRole($role)) {
            if ($request->ajax()) {
                return response('Unauthorized.', 401);
            }

            return redirect('/home');
        }

        return $next($request);
    }
}
