<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Konekt\Enum\Eloquent\CastsEnums;

class Customer extends Authenticatable
{
    use Notifiable, SoftDeletes, CastsEnums;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'customers';

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id', 'created_at', 'updated_at'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token' ];

    /**
     * The model's default values for attributes.
     *
     * @var array
     */
    protected $attributes = [
        'is_active' => true,
    ];

    protected $casts = [
        'is_active'         => 'boolean',
        'email_verified_at' => 'datetime',
        'last_login_at'     => 'datetime',
        'last_purchase_at'  => 'datetime'
    ];

    protected $enums = [
        'type' => CustomerType::class
    ];

    public function getName()
    {
        if ($this->type->isOrganization()) {
            return $this->company_name;
        }
        return sprintf('%s %s', $this->firstname, $this->lastname);
    }

    protected function getNameAttribute()
    {
        return $this->getName();
    }

    public static function findByEmail($email)
    {
        return self::where('email', $email)->first();
    }

    /**
     * Relation for address' customers
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function addresses()
    {
        return $this->morphToMany('App\Address', 'addressable');
    }
}
