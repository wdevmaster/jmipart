<?php

namespace App;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Konekt\Enum\Eloquent\CastsEnums;
use Illuminate\Support\Collection;

class Cart extends Model
{
    use CastsEnums;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'carts';
    
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */  
    protected $guarded = ['id', 'created_at', 'updated_at'];

    protected $enums = [
        'state' => CartState::class
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function items()
    {
        return $this->hasMany('App\CartItem', 'cart_id', 'id');
    }
    /**
     * @inheritDoc
     */
    public function getItems(): Collection
    {
        return $this->items;
    }

    /**
     * @inheritDoc
     */
    public function itemCount()
    {
        return $this->items->sum('quantity');
    }

     /**
     * @inheritDoc
     */
    public function addItem(Product $product, $qty = 1, $params = [])
    {
        $item = $this->items()->ofCart($this)->byProduct($product)->first();

        if ($item) {
            $item->quantity += $qty;
            $item->save();
        } else {
            $item = $this->items()->create(
                $this->getDefaultCartItem($product, $qty)
            );
        }

        $this->load('items');

        return $item;
    }

    public function getDefaultCartItem(Product $product, $qty)
    {
        return [
            'product_id'    =>  $product->id,
            'product_type'  =>  get_class($product),
            'quantity'      =>  $qty,
            'price'         =>  $product->price
        ];
    }

    /**
     * @inheritDoc
     */
    public function removeItem($item)
    {
        if ($item) {
            $item->delete();
        }
        $this->load('items');
    }

    /**
     * @inheritDoc
     */
    public function removeProduct(Product $product)
    {
        $item = $this->items()->ofCart($this)->byProduct($product)->first();
        $this->removeItem($item);
    }

    /**
     * @inheritDoc
     */
    public function clear()
    {
        $this->items()->ofCart($this)->delete();
        $this->load('items');
    }

    /**
     * @inheritDoc
     */
    public function total()
    {
        return $this->items->sum('total');
    }

    /**
     * The cart's user relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function customer()
    {
        return $this->belongsTo(config('auth.providers.customer.model'));
    }

    /**
     * @inheritDoc
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * @inheritDoc
     */
    public function setUser($customer)
    {
        if ($customer instanceof Authenticatable) {
            $customer = $customer->id;
        }
        $this->customer_id = $customer;
    }

    public function scopeActives($query)
    {
        return $query->whereIn('state', CartState::getActiveStates());
    }

    public function scopeOfUser($query, $customer)
    {
        return $query->where('customer_id', is_object($customer) ? $customer->id : $customer);
    }
}
