<?php

namespace Tests\Feature\Controller;

use Helper;
use App\User;
use App\Country;
use App\Address;
use App\Province;
use App\Customer;
use Tests\TestCase;
use App\AddressType;
use Illuminate\Foundation\Testing\WithFaker;

class AddressControllerTest extends TestCase
{
    use  WithFaker;

    const ROUTE_TO_SHOW_CUSTOMER    = 'customer.show';
    const ROUTE_TO_CREATE_ADDRESSES = 'address.create';
    const ROUTE_TO_STORE_ADDRESSES  = 'address.store';
    const ROUTE_TO_DELETE_ADDRESSES = 'address.delete';

    protected $auth;
    protected $customer;
    
    public function setUp():  void
    {
        parent::setUp();

        $this->auth = factory(User::class)->create([
            'is_active' => true
        ]); 
        $this->customer = factory(Customer::class)->create();
    }

    /** @test */
    public function guests_cannot_access_the_address_CRUD_routes()
    {
        $response = $this->get(route(self::ROUTE_TO_CREATE_ADDRESSES));
        $response->assertRedirect('/login');
    }

    /** @test */
    public function create_new_address()
    {
        $country = factory(Country::class, 3)->create()->random();
        $data = [
            'for'           => Helper::shorten(get_class($this->customer)),
            'forId'         => $this->customer->id,
            'type'          => $type = $this->faker->randomElement(AddressType::toArray()),
            'name'          => $this->faker->sentence(2, true),
            'country_id'    => $country->id,
            'province_code' => $this->faker->stateAbbr,
            'province'      => $this->faker->state,
            'city'          => $this->faker->city,
            'postalcode'    => $this->faker->postcode,
            'address'       => $this->faker->streetAddress,
            'is_default'    => $this->faker->randomElement([true, false])
        ];
        
        $response = $this->actingAs($this->auth, 'admin')
                        ->post(route(self::ROUTE_TO_STORE_ADDRESSES), $data);

        $response->assertRedirect(route(self::ROUTE_TO_SHOW_CUSTOMER, $this->customer->id));
        $this->assertDatabaseHas('provinces', [
            'country_id' => $data['country_id'],
            'code' => $data['province_code'],
            'name' => $data['province']
        ]);
        unset($data['for'], $data['forId'], $data['province_code'], $data['province']);
        $this->assertDatabaseHas('addresses', $data);
    }

    /** @test */
    public function validate_when_creating_a_address()
    {
        $data = [
            'for'           => Helper::shorten(get_class($this->customer)),
            'forId'         => $this->customer->id,
            'type'          => $type = $this->faker->randomElement(AddressType::toArray()),
            'name'          => '',
            'country_id'    => '',
            'province_code' => '',
            'province'      => '',
            'city'          => '',
            'postalcode'    => '',
            'address'       => '',
            'is_default'    => false
        ];

        $response = $this->actingAs($this->auth, 'admin')
                        ->post(route(self::ROUTE_TO_STORE_ADDRESSES), $data);

        $response->assertSessionHasErrors('name')
                 ->assertSessionHasErrors('country_id')
                 ->assertSessionHasErrors('province')
                 ->assertSessionHasErrors('city')
                 ->assertSessionHasErrors('postalcode')
                 ->assertSessionHasErrors('address');
    }

    /** @test */
    public function delete_existing_address()
    {
        $address = factory(Address::class, 3)
                        ->create()
                        ->each(function($address) {
                            $address->customers()->attach($this->customer->id);
                        })->random();

        $response = $this->actingAs($this->auth, 'admin')
                        ->delete(route(self::ROUTE_TO_DELETE_ADDRESSES),[
                            'id' => $address->id
                        ]);

        $response->assertSuccessful();
        $this->assertSoftDeleted('addresses', [
            'id'  => $address->id
        ]);
    }

    /** @test */
    public function validate_if_the_address_id_excites_in_delete()
    {
        $addresses = factory(Address::class, 3)
                        ->create()
                        ->each(function($address) {
                            $address->customers()->attach($this->customer->id);
                        });
        $address = $addresses->random(); 

        $response = $this->actingAs($this->auth, 'admin')
                        ->delete(route(self::ROUTE_TO_DELETE_ADDRESSES),[
                            'id' => $addresses->count()+1
                        ]);

        $response->assertStatus(404);
        $this->assertDatabaseHas('addresses', [
            'id'  => $address->id
        ]);
    }
}
