<?php

namespace Tests\Feature\Controller\Auth;

use App\User;
use App\Customer;
use Tests\TestCase;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Foundation\Testing\WithFaker;

class ResetPasswordControllerTest extends TestCase
{
    use WithFaker;

    const ROUTE_PASSWORD_RESET = 'password.reset';
    const ROUTE_PASSWORD_UPDATE = 'password.update';
    const USER_ORIGINAL_PASSWORD = 'secret';


    /** @test */
    public function show_password_reset_page()
    {
        $customer = factory(Customer::class)->create();
        $token = Password::broker()->createToken($customer);

        $this
            ->get(route(self::ROUTE_PASSWORD_RESET, [
                'token' => $token,
            ]))
            ->assertSuccessful()
            ->assertSee(__('Reset Password'))
            ->assertSee($customer->email)
            ->assertSee(__('Password'))
            ->assertSee(__('Confirm Password'));
    }

    /** @test */
    public function submit_password_reset_email_not_found()
    {
        $customer = factory(Customer::class)->create([
            'password' => bcrypt(self::USER_ORIGINAL_PASSWORD),
        ]);

        $token = Password::broker()->createToken($customer);

        $password = str_random();

        $this
            ->followingRedirects()
            ->from(route(self::ROUTE_PASSWORD_RESET, [
                'token' => $token,
            ]))
            ->post(route(self::ROUTE_PASSWORD_UPDATE), [
                'token' => $token,
                'email' => $this->faker->unique()->safeEmail,
                'password' => $password,
                'password_confirmation' => $password,
            ])
            ->assertSuccessful()
            ->assertSee(e(__('passwords.user')));

        $customer->refresh();

        $this->assertFalse(Hash::check($password, $customer->password));

        $this->assertTrue(Hash::check(self::USER_ORIGINAL_PASSWORD,
            $customer->password));
    }

    /** @test */
    public function submit_password_reset_password_mismatch()
    {
        $customer = factory(Customer::class)->create([
            'password' => bcrypt(self::USER_ORIGINAL_PASSWORD),
        ]);

        $token = Password::broker()->createToken($customer);

        $password = str_random();
        $password_confirmation = str_random();

        $this
            ->followingRedirects()
            ->from(route(self::ROUTE_PASSWORD_RESET, [
                'token' => $token,
            ]))
            ->post(route(self::ROUTE_PASSWORD_UPDATE), [
                'token' => $token,
                'email' => $customer->email,
                'password' => $password,
                'password_confirmation' => $password_confirmation,
            ])
            ->assertSuccessful()
            ->assertSee(__('validation.confirmed', [
                'attribute' => 'password',
            ]));

        $customer->refresh();

        $this->assertFalse(Hash::check($password, $customer->password));

        $this->assertTrue(Hash::check(self::USER_ORIGINAL_PASSWORD,
            $customer->password));
    }

    /** @test */
    public function submit_password_reset_password_too_short()
    {
        $customer = factory(Customer::class)->create([
            'password' => bcrypt(self::USER_ORIGINAL_PASSWORD),
        ]);

        $token = Password::broker()->createToken($customer);

        $password = str_random(5);

        $this
            ->followingRedirects()
            ->from(route(self::ROUTE_PASSWORD_RESET, [
                'token' => $token,
            ]))
            ->post(route(self::ROUTE_PASSWORD_UPDATE), [
                'token' => $token,
                'email' => $customer->email,
                'password' => $password,
                'password_confirmation' => $password,
            ])
            ->assertSuccessful()
            ->assertSee(__('validation.min.string', [
                'attribute' => 'password',
                'min' => 6,
            ]));

        $customer->refresh();

        $this->assertFalse(Hash::check($password, $customer->password));

        $this->assertTrue(Hash::check(self::USER_ORIGINAL_PASSWORD,
            $customer->password));
    }

    /** @test */
    public function submit_password_reset()
    {
        $customer = factory(Customer::class)->create([
            'password' => bcrypt(self::USER_ORIGINAL_PASSWORD),
        ]);

        $token = Password::broker()->createToken($customer);

        $password = str_random();

        $this
            ->followingRedirects()
            ->from(route(self::ROUTE_PASSWORD_RESET, [
                'token' => $token,
            ]))
            ->post(route(self::ROUTE_PASSWORD_UPDATE), [
                'token' => $token,
                'email' => $customer->email,
                'password' => $password,
                'password_confirmation' => $password,
            ])
            ->assertSuccessful();

        $customer->refresh();

        $this->assertFalse(Hash::check(self::USER_ORIGINAL_PASSWORD,
            $customer->password));

        $this->assertTrue(Hash::check($password, $customer->password));
    }
}
