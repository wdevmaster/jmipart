<?php

namespace Tests\Feature\Controller\Auth;

use App\Customer;
use Tests\TestCase;
use Caffeinated\Shinobi\Models\Role;
use Illuminate\Foundation\Testing\WithFaker;

class RegisterControllerTest extends TestCase
{
    use WithFaker;

    const USER_PASSWORD = 'secret';

    /** @test */
    public function new_customer_register()
    {
        //$this->withoutExceptionHandling();
        $data = [
            'firstname' => $this->faker->firstName('male'|'female'), 
            'lastname'  => $this->faker->lastName, 
            'email'     => $this->faker->unique()->safeEmail,
            'password'  => self::USER_PASSWORD,
            'password_confirmation' => self::USER_PASSWORD
        ];

        $response = $this
                        ->followingRedirects()
                        ->post('/register', $data);


        $response->assertSuccessful();
        unset($data['password'], $data['password_confirmation']);
        $this->assertDatabaseHas('customers', $data);
    }

    /** @test */
    public function demand_the_registration_data()
    {
        //$this->withoutExceptionHandling();
        $data = [
            'firstname' => '', 
            'lastname'  => '',
            'email'     => '',
            'password'  => '',
            'password_confirmation' => ''
        ];

        $response = $this
                        ->followingRedirects()
                        ->from('/register')
                        ->post('/register', $data);

        $response
                ->assertOk()
                ->assertSee(__('validation.required', ['attribute' => 'firstname']))
                ->assertSee(__('validation.required', ['attribute' => 'lastname']))
                ->assertSee(__('validation.required', ['attribute' => 'email']))
                ->assertSee(__('validation.required', ['attribute' => 'password']));
    }

    /** @test */
    public function unique_email_register()
    {
        //$this->withoutExceptionHandling();
        factory(Customer::class)->create([
            'email' => $email = $this->faker->unique()->safeEmail,
        ]);
        $data = [
            'name'      => $this->faker->name,
            'email'     => $email,
            'password'  => self::USER_PASSWORD,
            'password_confirmation' => self::USER_PASSWORD
        ];

        $response = $this
                        ->followingRedirects()
                        ->from('/register')
                        ->post('/register', $data);

        $response
                ->assertOk()
                ->assertSee(__('validation.unique', ['attribute' => 'email']));
    }

}
