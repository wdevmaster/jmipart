<?php

namespace Tests\Feature\Controller\Auth;

use App\User;
use App\Customer;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;

class LoginControllerTest extends TestCase
{
    use WithFaker;

    const USER_PASSWORD = 'secret';

    /** @test */
    public function record_of_date_of_the_login_customer()
    {
        $this->withoutExceptionHandling();

        $customer = factory(Customer::class)->create([
            'password' => bcrypt($password = self::USER_PASSWORD),
            'is_active' => true
        ]);
        $credentials = [
            'email' => $customer->email,
            'password' => $password,
        ];

        $response = $this
                        ->followingRedirects()
                        ->from('/login')
                        ->post('/login', $credentials);

        $response->assertSuccessful();
        $this->assertAuthenticated();
        $this->assertDatabaseHas('customers', [
            'email' => $customer->email,
            'last_login_at' => date('Y-m-d H:i:s')
        ]);
    }

    /** @test */
    public function record_of_date_of_the_login_admin()
    {
        $this->withoutExceptionHandling();

        $user = factory(user::class)->create([
            'password' => bcrypt($password = self::USER_PASSWORD),
            'is_active' => true
        ]);
        $credentials = [
            'email' => $user->email,
            'password' => $password,
        ];

        $response = $this
                        ->followingRedirects()
                        ->from('/login')
                        ->post('/login', $credentials);

        $response->assertSuccessful();
        $this->assertAuthenticated();
        $this->assertDatabaseHas('users', [
            'email' => $user->email,
            'last_login_at' => date('Y-m-d H:i:s')
        ]);
    }

    /** @test */
    public function login_counter_log_customer()
    {
        $this->withoutExceptionHandling();

        $customer = factory(Customer::class)->create([
            'password' => bcrypt($password = self::USER_PASSWORD),
            'is_active' => true,
            'login_count' => $rand = rand(0, 10)
        ]);
        $credentials = [
            'email' => $customer->email,
            'password' => $password,
        ];

        $response = $this
                        ->followingRedirects()
                        ->from('/login')
                        ->post('/login', $credentials);

        $response->assertSuccessful();
        $this->assertAuthenticated();
        $this->assertDatabaseHas('customers', [
            'email' => $customer->email,
            'login_count' => $rand+1
        ]);
    }

    /** @test */
    public function login_counter_log_admin()
    {
        $this->withoutExceptionHandling();

        $user = factory(User::class)->create([
            'password' => bcrypt($password = self::USER_PASSWORD),
            'is_active' => true,
            'login_count' => $rand = rand(0, 10)
        ]);
        $credentials = [
            'email' => $user->email,
            'password' => $password,
        ];

        $response = $this
                        ->followingRedirects()
                        ->from('/login')
                        ->post('/login', $credentials);

        $response->assertSuccessful();
        $this->assertAuthenticated();
        $this->assertDatabaseHas('users', [
            'email' => $user->email,
            'login_count' => $rand+1
        ]);
    }

    /** @test */
    public function deny_inactive_account_access_customer()
    {
        $customer = factory(Customer::class)->create([
            'password' => bcrypt($password = self::USER_PASSWORD),
            'is_active' => false,
        ]);
        $credentials = [
            'email' => $customer->email,
            'password' => $password,
        ];

        $response = $this
                        ->followingRedirects()
                        ->from('/login')
                        ->post('/login', $credentials);

        $this->assertGuest();
        $this->assertCredentials($credentials);
        $response
            ->assertSuccessful()
            ->assertSee(__('Your account seems to be inactive.'));
    }

    /** @test */
    public function deny_inactive_account_access_admin()
    {
        $user = factory(User::class)->create([
            'password' => bcrypt($password = self::USER_PASSWORD),
            'is_active' => false,
        ]);
        $credentials = [
            'email' => $user->email,
            'password' => $password,
        ];

        $response = $this
                        ->followingRedirects()
                        ->from('/login')
                        ->post('/login', $credentials);

        $this->assertGuest();
        $this->assertCredentials($credentials, 'admin');
        $response
            ->assertSuccessful()
            ->assertSee(__('Your account seems to be inactive.'));
    }

    /** @test */
    public function customer_cannot_login_with_incorrect_credentials()
    {
        $customer = factory(Customer::class)->create();
        $credentials = [
            'email' => 'user@email.com',
            'password' => '123456',
        ];

        $response = $this
                        ->followingRedirects()
                        ->from('/login')
                        ->post('/login', $credentials);

        $this->assertGuest();
        $response
            ->assertOk()
            ->assertSee(__('auth.failed'));
    }

    /** @test */
    public function admin_cannot_login_with_incorrect_credentials()
    {
        $user = factory(User::class)->create();
        $credentials = [
            'email' => 'user@email.com',
            'password' => '123456',
        ];

        $response = $this
                        ->followingRedirects()
                        ->from('/login')
                        ->post('/login', $credentials);

        $this->assertGuest('admin');
        $response
            ->assertOk()
            ->assertSee(__('auth.failed'));
    }

}
