<?php

namespace Tests\Feature\Controller\Auth;

use App\Customer;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Notification;
use Illuminate\Auth\Notifications\ResetPassword;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ForgotPasswordControllerTest extends TestCase
{
    use WithFaker;

    const ROUTE_PASSWORD_EMAIL = 'password.email';
    const ROUTE_PASSWORD_REQUEST = 'password.request';

    /** @test */
    public function show_password_reset_request_page()
    {
        $this
            ->get(route(self::ROUTE_PASSWORD_REQUEST))
            ->assertSuccessful()
            ->assertSee(__('Reset Password'))
            ->assertSee(__('E-Mail Address'))
            ->assertSee(__('Send Password Reset Link'));

    }

    /** @test */
    public function submit_password_reset_request_invalid_email()
    {
        $this
            ->followingRedirects()
            ->from(route(self::ROUTE_PASSWORD_REQUEST))
            ->post(route(self::ROUTE_PASSWORD_EMAIL), [
                'email' => str_random(),
            ])
            ->assertSuccessful()
            ->assertSee(__('validation.email', [
                'attribute' => 'email',
            ]));
    }

    /** @test */
    public function submit_password_reset_request_email_not_found()
    {
        $this
            ->followingRedirects()
            ->from(route(self::ROUTE_PASSWORD_REQUEST))
            ->post(route(self::ROUTE_PASSWORD_EMAIL), [ 
                'email' => $this->faker->unique()->safeEmail,
            ])
            ->assertSuccessful()
            ->assertSee(e(__('passwords.user')));
    } 

    /** @test */
    public function deny_reset_email_delivery_on_inactive_account()
    {
        Notification::fake();
        $customer = factory(Customer::class)->create([
            'is_active' => false
        ]);

        $this
            ->followingRedirects()
            ->from(route(self::ROUTE_PASSWORD_REQUEST))
            ->post(route(self::ROUTE_PASSWORD_EMAIL), [ 
                'email' => $customer->email,
            ])
            ->assertSuccessful()
            ->assertSee(__('Your account seems to be inactive.'));

        Notification::assertNotSentTo($customer, ResetPassword::class);
    }

    /** @test */
    public function submit_password_reset_request()
    {
        Notification::fake();
        $customer = factory(Customer::class)->create([
            'is_active' => true
        ]);

        $this
            ->followingRedirects()
            ->from(route(self::ROUTE_PASSWORD_REQUEST))
            ->post(route(self::ROUTE_PASSWORD_EMAIL), [
                'email' => $customer->email,
            ])
            ->assertSuccessful()
            ->assertSee(__('passwords.sent'));

        Notification::assertSentTo($customer, ResetPassword::class);
    }


}
