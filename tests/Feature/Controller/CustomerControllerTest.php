<?php

namespace Tests\Feature\Controller;

use App\User;
use App\Customer;
use Tests\TestCase;
use App\CustomerType;
use Illuminate\Foundation\Testing\WithFaker;

class CustomerControllerTest extends TestCase
{
    use WithFaker;

    const ROUTE_TO_INDEX_CUSTOMER   = 'customer.index';
    const ROUTE_TO_CREATE_CUSTOMER  = 'customer.create';
    const ROUTE_TO_STORE_CUSTOMER   = 'customer.store';
    const ROUTE_TO_SHOW_CUSTOMER    = 'customer.show';
    const ROUTE_TO_EDIT_CUSTOMER    = 'customer.edit';
    const ROUTE_TO_UPDATE_CUSTOMER  = 'customer.update';
    const ROUTE_TO_DELETE_CUSTOMER  = 'customer.delete';
    const USER_PASSWORD = 'secret';

    protected $auth;

    public function setUp():  void
    {
        parent::setUp();

        $this->auth = factory(User::class)->create([
            'is_active' => true
        ]); 
    }

    /** @test */
    public function guests_cannot_access_the_customers_CRUD_routes()
    {
        $response = $this->get(route(self::ROUTE_TO_INDEX_CUSTOMER));
        $response->assertRedirect('/login');
    }

    /** @test */
    public function create_new_customer()
    {
        //$this->withoutExceptionHandling();
        $data = [
            'type'      => $type = $this->faker->randomElement(CustomerType::toArray()),
            'firstname' => $this->faker->firstName('male'|'female'), 
            'lastname'  => $this->faker->lastName, 
            'email'     => $this->faker->unique()->safeEmail,
            'password'  => self::USER_PASSWORD,
        ];

        if ($type == 'organization')
            $data = array_merge($data, [
                'company_name'      => $this->faker->company,
                'tax_nr'            => $this->faker->ein,
                'registration_nr'   => $this->faker->ein,
            ]);


        $response = $this->actingAs($this->auth, 'admin')
                        ->post(route(self::ROUTE_TO_STORE_CUSTOMER), $data);

        $response->assertRedirect(route(self::ROUTE_TO_INDEX_CUSTOMER));
        unset($data['password']);
        $this->assertDatabaseHas('customers', $data);
    }

    /** @test */
    public function validate_when_creating_a_new_customer()
    {
        $data = [
            'type'      => $type = $this->faker->randomElement(CustomerType::toArray()),
            'firstname' => '', 
            'lastname'  => '', 
            'email'     => '',
            'password'  => '',
        ];
        if ($type == 'organization')
            $data = array_merge($data, [
                'company_name'      => '',
                'tax_nr'            => '',
                'registration_nr'   => '',
            ]);

        $response = $this->actingAs($this->auth, 'admin')
                         ->post(route(self::ROUTE_TO_STORE_CUSTOMER), $data);

        $response->assertSessionHasErrors('email')
                 ->assertSessionHasErrors('password');

        if ($type == 'organization')
            $response->assertSessionHasErrors('company_name');
        elseif($type == 'individual')
            $response->assertSessionHasErrors('firstname')
                     ->assertSessionHasErrors('lastname');
    }

    /** @test */
    public function validate_unique_when_creating_a_new_customer()
    {
        $customer = factory(Customer::class)->create();
        $data = [
            'type'      => $type = $this->faker->randomElement(CustomerType::toArray()),
            'firstname' => $this->faker->firstName('male'|'female'), 
            'lastname'  => $this->faker->lastName, 
            'email'     => $customer->email,
            'password'  => self::USER_PASSWORD,
        ];

        if ($type == 'organization')
            $data = array_merge($data, [
                'company_name'      => $this->faker->company,
                'tax_nr'            => $this->faker->ein,
                'registration_nr'   => $this->faker->ein,
            ]);


        $response = $this->actingAs($this->auth, 'admin')
                        ->post(route(self::ROUTE_TO_STORE_CUSTOMER), $data);

        $response->assertSessionHasErrors('email');
    }

    /** @test */
    public function update_existing_customer()
    {
        //$this->withoutExceptionHandling();
        $customer = factory(Customer::class)->create();
        $data = [
            'type'      => $type = $this->faker->randomElement(CustomerType::toArray()),
            'firstname' => $this->faker->firstName('male'|'female'), 
            'lastname'  => $this->faker->lastName, 
            'email'     => $customer->email,
        ];

        if ($type == 'organization')
            $data = array_merge($data, [
                'company_name'      => $this->faker->company,
                'tax_nr'            => $this->faker->ein,
                'registration_nr'   => $this->faker->ein,
            ]);
        
        $response = $this->actingAs($this->auth, 'admin')
                        ->put(route(self::ROUTE_TO_UPDATE_CUSTOMER, $customer->id), $data);

        $response->assertRedirect(route(self::ROUTE_TO_INDEX_CUSTOMER));
        unset($data['password']);
        $this->assertDatabaseHas('customers', $data);
        $this->assertDatabaseMissing('customers', [
            'firstname' => $customer->firstname,
            'lastname'  => $customer->lastname
        ]);
    }


    /** @test */
    public function validate_if_the_customer_id_excites_in_edit()
    {
        $customer = factory(Customer::class)->create();

        $response = $this->actingAs($this->auth, 'admin')
                        ->get(route(self::ROUTE_TO_EDIT_CUSTOMER, $customer->id+1));

        $response->assertRedirect(route(self::ROUTE_TO_INDEX_CUSTOMER));
    }

    /** @test */
    public function validate_if_the_customer_id_excites_in_show()
    {
        $customer = factory(Customer::class)->create();

        $response = $this->actingAs($this->auth, 'admin')
                        ->get(route(self::ROUTE_TO_SHOW_CUSTOMER, $customer->id+1));

        $response->assertRedirect(route(self::ROUTE_TO_INDEX_CUSTOMER));
    }

    /** @test */
    public function validate_if_the_customer_id_excites_in_update()
    {
        $customer = factory(Customer::class)->create();
        $data = [
            'type'      => $type = $this->faker->randomElement(CustomerType::toArray()),
            'firstname' => $this->faker->firstName('male'|'female'), 
            'lastname'  => $this->faker->lastName, 
            'email'     => $customer->email,
        ];

        if ($type == 'organization')
            $data = array_merge($data, [
                'company_name'      => $this->faker->company,
                'tax_nr'            => $this->faker->ein,
                'registration_nr'   => $this->faker->ein,
            ]);
        
        $response = $this->actingAs($this->auth, 'admin')
                        ->put(route(self::ROUTE_TO_UPDATE_CUSTOMER, $customer->id+1), $data);

        $response->assertRedirect(route(self::ROUTE_TO_INDEX_CUSTOMER));
    }

    /** @test */
    public function validate_the_data_when_updating_the_customer()
    {
        $customer = factory(Customer::class)->create();
        $data = [
            'type'      => $type = $this->faker->randomElement(CustomerType::toArray()),
            'firstname' => '', 
            'lastname'  => '', 
            'email'     => '',
            'password'  => '',
        ];
        if ($type == 'organization')
            $data = array_merge($data, [
                'company_name'      => '',
                'tax_nr'            => '',
                'registration_nr'   => '',
            ]);

        $response = $this->actingAs($this->auth, 'admin')
                        ->put(route(self::ROUTE_TO_UPDATE_CUSTOMER, $customer->id+1), $data);

        $response->assertSessionHasErrors('email');

        if ($type  == 'organization')
            $response->assertSessionHasErrors('company_name');
        elseif($type  == 'individual')
            $response->assertSessionHasErrors('firstname')
                     ->assertSessionHasErrors('lastname');
    }

    /** @test */
    public function delete_existing_customer()
    {
        $customer = factory(Customer::class)->create();

        $response = $this->actingAs($this->auth, 'admin')
                        ->delete(route(self::ROUTE_TO_DELETE_CUSTOMER),[
                            'id' => $customer->id
                        ]);

        $response->assertSuccessful();
        $this->assertSoftDeleted('customers', [
            'email'  => $customer->email
        ]);
    }

    /** @test */
    public function validate_if_the_customer_id_excites_in_delete()
    {
        $customer = factory(Customer::class)->create();

        $response = $this->actingAs($this->auth, 'admin')
                        ->delete(route(self::ROUTE_TO_DELETE_CUSTOMER),[
                            'id' => $customer->id+1
                        ]);

        $response->assertStatus(404);
        $this->assertDatabaseHas('customers', [
            'email'  => $customer->email
        ]);
    }
}
