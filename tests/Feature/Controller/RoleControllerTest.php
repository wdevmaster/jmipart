<?php

namespace Tests\Feature\Controller;

use App\User;
use Tests\TestCase;
use Illuminate\Support\Str;
use Caffeinated\Shinobi\Models\Role;
use Illuminate\Support\Facades\Artisan;
use Caffeinated\Shinobi\Models\Permission;
use Illuminate\Foundation\Testing\WithFaker;

class RoleControllerTest extends TestCase
{
    use WithFaker;

    const ROUTE_TO_INDEX_ROLE  = 'role.index';
    const ROUTE_TO_CREATE_ROLE  = 'role.create';
    const ROUTE_TO_STORE_ROLE   = 'role.store';
    const ROUTE_TO_EDIT_ROLE    = 'role.edit';
    const ROUTE_TO_UPDATE_ROLE  = 'role.update';
    const ROUTE_TO_DELETE_ROLE  = 'role.delete';

    protected $user;

    public function setUp():  void
    {
        parent::setUp();
        $this->user = factory(User::class)->create();
        Artisan::call('db:seed', ['--class' => 'PermissionsTableSeeder']);
    }

    /** @test */
    public function guests_cannot_access_the_CRUD_routes_of_the_roles()
    {
        $response = $this->get(route(self::ROUTE_TO_INDEX_ROLE));
        $response->assertRedirect('/login');
    }

    /** @test */
    public function create_a_new_role()
    { 
        $this->withoutExceptionHandling();
        $permissions = $this->seedPermissions();
        $data = [
            'name' => $this->faker->jobTitle,
            'permissions' => $permissions->toArray()
        ];

        $response = $this->actingAs($this->user, 'admin')
                        ->post(route(self::ROUTE_TO_STORE_ROLE), $data);
        
        $response->assertRedirect(route(self::ROUTE_TO_INDEX_ROLE));
        $this->assertDatabaseHas('roles', [
            'name' => $data['name'],
            'slug' => str_slug($data['name'])
        ]);
        $role = Role::where('slug', Str::slug($data['name']))->first();
        foreach ($permissions->pluck('id') as $permission_id) {
            $this->assertDatabaseHas('permission_role', [
                'role_id' => $role->id,
                'permission_id' => $permission_id
            ]);
        }
    }

    /** @test */
    public function validate_when_creating_a_new_role()
    {
        $response = $this->actingAs($this->user, 'admin')
                         ->post(route(self::ROUTE_TO_STORE_ROLE), [
                            'name' => ''
                        ]);

        $response->assertSessionHasErrors('name');
    }

    /** @test */
    public function validate_unique_when_creating_a_new_role()
    {
        $role = factory(Role::class)->create();

        $response = $this->actingAs($this->user, 'admin')
                         ->post(route(self::ROUTE_TO_STORE_ROLE), [
                            'name' => $role->name
                        ]);

        $response->assertSessionHasErrors('name');
    }

    /** @test */
    public function update_existing_role()
    {
        $permissions = $this->seedPermissions();
        $otherPermissions = $this->seedPermissions();
        $role = factory(Role::class)->create();
        $role->givePermissionTo($permissions->pluck('slug')->toArray());
        $data = [
            'name' => $this->faker->jobTitle,
            'permissions' => $otherPermissions->toArray()
        ];

        $response = $this->actingAs($this->user, 'admin')
                        ->put(route(self::ROUTE_TO_UPDATE_ROLE, $role->id), $data);

        $response->assertRedirect(route(self::ROUTE_TO_INDEX_ROLE));
        $this->assertDatabaseHas('roles', [
            'name' => $data['name'],
            'slug' => str_slug($data['name'])
        ]);
        $this->assertDatabaseMissing('roles', [
            'name' => $role->name
        ]);
        foreach ($otherPermissions->pluck('id') as $otherPermissions_id) {
            $this->assertDatabaseHas('permission_role', [
                'role_id' => $role->id,
                'permission_id' => $otherPermissions_id
            ]);
        }
    }

    /** @test */
    public function validate_the_data_when_updating_the_role()
    {
        $role = factory(Role::class)->create();
        $data = [
            'name' => ''
        ];

        $response = $this->actingAs($this->user, 'admin')
                        ->put(route(self::ROUTE_TO_UPDATE_ROLE, $role->id), $data);

        $response->assertSessionHasErrors('name');
    }

    /** @test */
    public function validate_if_the_role_id_excites_in_edit()
    {
        //$this->withoutExceptionHandling();
        $role = factory(Role::class)->create();

        $response = $this->actingAs($this->user, 'admin')
                        ->get(route(self::ROUTE_TO_EDIT_ROLE, $role->id+1));

        $response->assertRedirect(route(self::ROUTE_TO_INDEX_ROLE));
    }

    /** @test */
    public function validate_if_the_role_id_excites_in_update()
    {
        //$this->withoutExceptionHandling();
        $permissions = $this->seedPermissions();
        $otherPermissions = $this->seedPermissions();
        $role = factory(Role::class)->create();
        $role->givePermissionTo($permissions->pluck('slug')->toArray());
        $data = [
            'name' => $this->faker->jobTitle,
            'permissions' => $otherPermissions->toArray()
        ];

        $response = $this->actingAs($this->user, 'admin')
                        ->put(route(self::ROUTE_TO_UPDATE_ROLE, $role->id), $data);

        $response->assertRedirect(route(self::ROUTE_TO_INDEX_ROLE));
    }

    /** @test */
    public function delete_existing_role()
    {
        $role = factory(Role::class)->create();

        $response = $this->actingAs($this->user, 'admin')
                        ->delete(route(self::ROUTE_TO_DELETE_ROLE),[
                            'id' => $role->id
                        ]);

        $response->assertSuccessful();
        $this->assertDatabaseMissing('roles', [
            'id'    => $role->id,
            'name'  => $role->name
        ]);          
    }

    /** @test */
    public function disable_user_with_role_to_delete()
    {
        $user = factory(User::class)->create([
            'is_active' => true
        ]);
        $role = factory(Role::class)->create();
        $user->assignRoles($role->slug);

        $response = $this->actingAs($this->user, 'admin')
                        ->delete(route(self::ROUTE_TO_DELETE_ROLE),[
                            'id' => $role->id
                        ]);
                        
        $response->assertSuccessful();
        $this->assertDatabaseMissing('roles', [
            'id'    => $role->id,
            'name'  => $role->name
        ]);
        $this->assertDatabaseMissing('role_user', [
            'role_id' => $role->id,
            'user_id' => $user->id
        ]);
        $this->assertDatabaseHas('users', [
            'email' => $user->email,
            'is_active' => false
        ]);      
    }

    /** @test */
    public function validate_if_the_role_id_excites_in_delete()
    {
        //$this->withoutExceptionHandling();
        $role = factory(Role::class)->create();

        $response = $this->actingAs($this->user, 'admin')
                        ->delete(route(self::ROUTE_TO_DELETE_ROLE),[
                            'id' => $role->id+1
                        ]);

        $response->assertStatus(404);
        $this->assertDatabaseHas('roles', [
            'name' => $role->name
        ]);
    }

}
