<?php

namespace Tests\Feature\Controller;

use App\User;
use Tests\TestCase;
use Caffeinated\Shinobi\Models\Role;
use Illuminate\Foundation\Testing\WithFaker;

class UserControllerTest extends TestCase
{
    use WithFaker;

    const ROUTE_TO_INDEX_USER   = 'user.index';
    const ROUTE_TO_CREATE_USER  = 'user.create';
    const ROUTE_TO_STORE_USER   = 'user.store';
    const ROUTE_TO_EDIT_USER    = 'user.edit';
    const ROUTE_TO_UPDATE_USER  = 'user.update';
    const ROUTE_TO_DELETE_USER  = 'user.delete';
    const USER_PASSWORD = 'secret';

    protected $user;

    public function setUp():  void
    {
        parent::setUp();

        $this->user = factory(User::class)->create([
            'is_active' => true
        ]); 
    }

    /** @test */
    public function guests_cannot_access_the_users_CRUD_routes()
    {
        $response = $this->get(route(self::ROUTE_TO_INDEX_USER));
        $response->assertRedirect('/login');
    }

    /** @test */
    public function create_new_user()
    {
        //$this->withoutExceptionHandling();
        $role = factory(Role::class, 3)->create()->random();
        $data = [
            'name'      => $this->faker->firstName('male'|'female'),
            'email'     => $this->faker->unique()->safeEmail,
            'password'  => self::USER_PASSWORD,
            'is_active' => $this->faker->randomElement([1, 0]),
            'roles'     => [
                $role->slug => true
            ]
        ];

        $response = $this->actingAs($this->user, 'admin')
                        ->post(route(self::ROUTE_TO_STORE_USER), $data);

        $response->assertRedirect(route(self::ROUTE_TO_INDEX_USER));
        unset($data['password'], $data['roles']);
        $this->assertDatabaseHas('users', $data);
        $user = User::findByEmail($data['email']);
        $this->assertDatabaseHas('role_user', [
            'user_id' => $user->id,
            'role_id' => $role->id
        ]);
    }

    /** @test */
    public function validate_when_creating_a_new_user()
    {
        $data = [
            'name'      => '',
            'email'     => '',
            'password'  => '',
            'is_active' => false,
        ];

        $response = $this->actingAs($this->user, 'admin')
                        ->post(route(self::ROUTE_TO_STORE_USER), $data);

        $response->assertSessionHasErrors('name')
                 ->assertSessionHasErrors('email')
                 ->assertSessionHasErrors('password')
                 ->assertSessionHasErrors('roles');
    }

    /** @test */
    public function validate_unique_when_creating_a_new_user()
    {
        $role = factory(Role::class, 3)->create()->random();
        $data = [
            'name'      => $this->faker->firstName('male'|'female'),
            'email'     => $this->user->email,
            'password'  => self::USER_PASSWORD,
            'is_active' => $this->faker->randomElement([1, 0]),
            'roles'     => [
                $role->slug => true
            ]
        ];

        $response = $this->actingAs($this->user, 'admin')
                        ->post(route(self::ROUTE_TO_STORE_USER), $data);

        $response->assertSessionHasErrors('email');
    }

    /** @test */
    public function update_existing_user()
    {
        //$this->withoutExceptionHandling();
        $user = factory(User::class)->create();
        $role = factory(Role::class, 3)->create()->random();
        $data = [
            'name'      => $this->faker->firstName('male'|'female'),
            'email'     => $this->faker->unique()->safeEmail,
            'is_active' => $this->faker->randomElement([1, 0]),
            'roles'     => [
                $role->slug => true
            ]
        ];
        if ($this->faker->randomElement([true, false]))
            $data['password'] = self::USER_PASSWORD;

        $response = $this->actingAs($this->user, 'admin')
                        ->put(route(self::ROUTE_TO_UPDATE_USER, $user->id), $data);

        $response->assertRedirect(route(self::ROUTE_TO_INDEX_USER));
        unset($data['password'], $data['roles']);
        $this->assertDatabaseHas('users', $data);
        $this->assertDatabaseMissing('users', [
            'email' => $user->email
        ]);
        $this->assertDatabaseHas('role_user', [
            'user_id' => $user->id,
            'role_id' => $role->id
        ]);
    }

    /** @test */
    public function validate_if_the_user_id_excites_in_edit()
    {
        $user = factory(User::class)->create();

        $response = $this->actingAs($this->user, 'admin')
                        ->get(route(self::ROUTE_TO_EDIT_USER, $user->id+1));

        $response->assertRedirect(route(self::ROUTE_TO_INDEX_USER));
    }

    /** @test */
    public function validate_if_the_user_id_excites_in_update()
    {
        $user = factory(User::class)->create();
        $role = factory(Role::class, 3)->create()->random();
        $data = [
            'name'      => $this->faker->firstName('male'|'female'),
            'email'     => $this->faker->unique()->safeEmail,
            'is_active' => $this->faker->randomElement([1, 0]),
            'roles'     => [
                $role->slug => true
            ]
        ];

        $response = $this->actingAs($user, 'admin')
                        ->put(route(self::ROUTE_TO_UPDATE_USER, $user->id+1), $data);

        $response->assertRedirect(route(self::ROUTE_TO_INDEX_USER));
    }

    /** @test */
    public function delete_existing_user()
    {
        $user = factory(User::class)->create();

        $response = $this->actingAs($user, 'admin')
                        ->delete(route(self::ROUTE_TO_DELETE_USER),[
                            'id' => $user->id
                        ]);

        $response->assertSuccessful();
        $this->assertSoftDeleted('users', [
            'email'  => $user->email
        ]);
    }

    /** @test */
    public function validate_if_the_user_id_excites_in_delete()
    {
        $user = factory(User::class)->create();

        $response = $this->actingAs($this->user, 'admin')
                        ->delete(route(self::ROUTE_TO_DELETE_USER),[
                            'id' => $user->id+1
                        ]);

        $response->assertStatus(404);
        $this->assertDatabaseHas('users', [
            'email'  => $user->email
        ]);
    }
}
