<?php

namespace Tests\Feature\Controller;

use App\User;
use App\Taxon;
use App\Taxonomy;
use Tests\TestCase;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Testing\WithFaker;

class TaxonControllerTest extends TestCase
{
    use WithFaker;
    const ROUTE_TO_SHOW_TAXONOMIES= 'taxonomy.show';
    const ROUTE_TO_CREATE_TAXONS  = 'taxon.create';
    const ROUTE_TO_STORE_TAXONS   = 'taxon.store';
    const ROUTE_TO_EDIT_TAXONS    = 'taxon.edit';
    const ROUTE_TO_UPDATE_TAXONS  = 'taxon.update';
    const ROUTE_TO_DELETE_TAXONS  = 'taxon.delete';

    protected $auth;
    protected $taxonomy;

    public function setUp():  void
    {
        parent::setUp();
        $this->taxonomy = factory(Taxonomy::class, 3)->create()->random();
        $this->auth = factory(User::class)->create([
            'is_active' => true
        ]); 
    }

    /** @test */
    public function guests_cannot_access_the_taxon_CRUD_routes()
    {
        $response = $this->get(route(self::ROUTE_TO_CREATE_TAXONS, $this->taxonomy->id));
        $response->assertRedirect('/login');
    }

    /** @test */
    public function create_new_taxon()
    {
        $this->withoutExceptionHandling();
        Storage::fake('public');
        $nbWords = $this->faker->numberBetween(1, 5);
        $otherSlug = $this->faker->randomElement([ true , false ]);
        $data = [ 
            'name' => $name = $this->faker->sentence($nbWords, true),
            'image' => $file = UploadedFile::fake()->image($this->faker->word.'.jpg')
        ];
        if (!$this->faker->randomElement([ true , false ]))
            $data['slug'] = $otherSlug ? $name : $this->faker->sentence($nbWords, true);
        else
            $data['slug'] = '';

        $response = $this->actingAs($this->auth, 'admin')
                        ->post(route(self::ROUTE_TO_STORE_TAXONS, $this->taxonomy->id), $data);

        $response->assertRedirect(route(self::ROUTE_TO_SHOW_TAXONOMIES, $this->taxonomy->id));
        unset($data['slug'], $data['image']);
        $this->assertDatabaseHas('taxons', array_merge($data, [
            'taxonomy_id' => $this->taxonomy->id
        ]));
        foreach (Storage::disk('public')->allFiles() as $image) 
            $pathImg = substr($image, 2) == $file->getClientOriginalName();
        $this->assertTrue($pathImg);
    }

    /** @test */
    public function validate_when_creating_a_new_taxon()
    {
        $data = [ 
            'name' => '', 
            'image' => UploadedFile::fake()->image($this->faker->word.'.doc')
        ];

        $response = $this->actingAs($this->auth, 'admin')
                        ->post(route(self::ROUTE_TO_STORE_TAXONS, $this->taxonomy->id), $data);

        $response->assertSessionHasErrors('name')
                 ->assertSessionHasErrors('image');
    }

    /** @test */
    public function validate_if_the_taxon_id_excites_in_edit()
    {
        $taxon = factory(Taxon::class)->create();

        $response = $this->actingAs($this->auth, 'admin')
                        ->get(route(self::ROUTE_TO_EDIT_TAXONS, [
                            'taxonomy' => $this->taxonomy->id,
                            'taxon' => $taxon->id+1
                        ]));

        $response->assertRedirect(route(self::ROUTE_TO_SHOW_TAXONOMIES, $this->taxonomy->id));
    }

    /** @test */
    public function update_existing_taxon()
    {
        Storage::fake('public');
        $taxon = factory(Taxon::class, 3)->create([
            'taxonomy_id' => $this->taxonomy->id,
            'priority' => 10
        ])->random();
        $file = UploadedFile::fake()->image($this->faker->word.'.jpg');
        $taxon->addMedia($file)->toMediaCollection();
        $nbWords = $this->faker->numberBetween(1, 5);
        $otherSlug = $this->faker->randomElement([ true , false ]);
        $priority = $this->faker->randomElement([ true , false ]) ? 
                                    $taxon->priority * rand(1, 3) : 
                                    $taxon->priority;
        $data = [ 
            'name' => $name = $this->faker->sentence($nbWords, true),
            'priority' => $priority,
            'image' => $otherFile = UploadedFile::fake()->image($this->faker->word.'.jpg')
        ];
        if (!$this->faker->randomElement([ true , false ]))
            $data['slug'] = $otherSlug ? $name : $this->faker->sentence($nbWords, true);
        else
            $data['slug'] = '';

        $response = $this->actingAs($this->auth, 'admin')
                        ->put(route(self::ROUTE_TO_UPDATE_TAXONS, [
                            'taxonomy' => $this->taxonomy->id,
                            'taxon' => $taxon->id
                        ]), $data);

        $response->assertRedirect(route(self::ROUTE_TO_SHOW_TAXONOMIES, $this->taxonomy->id));
        unset($data['slug'], $data['image']);
        $this->assertDatabaseHas('taxons', array_merge($data, [
            'id' => $taxon->id,
            'taxonomy_id' => $this->taxonomy->id
        ]));

        foreach (Storage::disk('public')->allFiles() as $image) 
            $pathImg = substr($image, 2) == $otherFile->getClientOriginalName();
        $this->assertTrue($pathImg);
    }

    /** @test */
    public function validate_if_the_taxon_id_excites_in_update()
    {
        $taxon = factory(Taxon::class, 3)->create([
            'taxonomy_id' => $this->taxonomy->id,
            'priority' => 10
        ])->random();
        $nbWords = $this->faker->numberBetween(1, 5);
        $otherSlug = $this->faker->randomElement([ true , false ]);
        $priority = $this->faker->randomElement([ true , false ]) ? 
                                    $taxon->priority * rand(1, 3) : 
                                    $taxon->priority;
        $data = [ 
            'name' => $name = $this->faker->sentence($nbWords, true) ,
            'priority' => $priority
        ];
        if (!$this->faker->randomElement([ true , false ]))
            $data['slug'] = $otherSlug ? $name : $this->faker->sentence($nbWords, true);
        else
            $data['slug'] = '';

        $response = $this->actingAs($this->auth, 'admin')
                        ->put(route(self::ROUTE_TO_UPDATE_TAXONS, [
                            'taxonomy' => $this->taxonomy->id,
                            'taxon' => $taxon->id+1
                        ]), $data);

        $response->assertRedirect(route(self::ROUTE_TO_SHOW_TAXONOMIES, $this->taxonomy->id));
    }

    /** @test */
    public function validate_the_data_when_updating_the_taxon()
    {
        $taxon = factory(Taxon::class, 3)->create([
            'taxonomy_id' => $this->taxonomy->id,
            'priority' => 10
        ])->random();
        $data = [ 
            'name' => '',
            'image' => UploadedFile::fake()->image($this->faker->word.'.doc')
        ];

        $response = $this->actingAs($this->auth, 'admin')
                        ->put(route(self::ROUTE_TO_UPDATE_TAXONS, [
                            'taxonomy' => $this->taxonomy->id,
                            'taxon' => $taxon->id
                        ]), $data);

        $response->assertSessionHasErrors('name')
                 ->assertSessionHasErrors('image');
    }

    /** @test */
    public function delete_existing_taxon()
    {
        $taxon = factory(Taxon::class, 3)->create([
            'taxonomy_id' => $this->taxonomy->id,
            'priority' => 10
        ])->random();

        $response = $this->actingAs($this->auth, 'admin')
                        ->delete(route(self::ROUTE_TO_DELETE_TAXONS, [
                            'taxonomy' => $this->taxonomy->id,
                        ]),[ 'id' => $taxon->id ]);

        $response->assertSuccessful();
        $this->assertDatabaseMissing('taxons', [
            'id' => $taxon->id,
            'taxonomy_id' => $this->taxonomy->id,
        ]);
    }

    /** @test */
    public function validate_if_the_taxon_id_excites_in_delete()
    {
        $taxon = factory(Taxon::class)->create([
            'taxonomy_id' => $this->taxonomy->id,
            'priority' => 10
        ]);

        $response = $this->actingAs($this->auth, 'admin')
                        ->delete(route(self::ROUTE_TO_DELETE_TAXONS, [
                            'taxonomy' => $this->taxonomy->id,
                        ]),[ 'id' => $taxon->id+1 ]);

        $response->assertStatus(404);
        $this->assertDatabaseHas('taxons', [
            'id' => $taxon->id,
            'taxonomy_id' => $this->taxonomy->id,
        ]);
    }
}
