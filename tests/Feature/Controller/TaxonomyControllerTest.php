<?php

namespace Tests\Feature\Controller;

use App\User;
use App\Taxonomy;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;

class TaxonomyControllerTest extends TestCase
{
    use WithFaker;

    const ROUTE_TO_INDEX_TAXONOMIES   = 'taxonomy.index';
    const ROUTE_TO_CREATE_TAXONOMIES  = 'taxonomy.create';
    const ROUTE_TO_STORE_TAXONOMIES   = 'taxonomy.store';
    const ROUTE_TO_SHOW_TAXONOMIES    = 'taxonomy.show';
    const ROUTE_TO_EDIT_TAXONOMIES    = 'taxonomy.edit';
    const ROUTE_TO_UPDATE_TAXONOMIES  = 'taxonomy.update';
    const ROUTE_TO_DELETE_TAXONOMIES  = 'taxonomy.delete';

    protected $auth;

    public function setUp():  void
    {
        parent::setUp();

        $this->auth = factory(User::class)->create([
            'is_active' => true
        ]); 
    }

    /** @test */
    public function guests_cannot_access_the_taxonomies_CRUD_routes()
    {
        $response = $this->get(route(self::ROUTE_TO_INDEX_TAXONOMIES));
        $response->assertRedirect('/login');
    }

    /** @test */
    public function create_new_taxonomy()
    {
        $nbWords = $this->faker->numberBetween(1, 5);
        $otherSlug = $this->faker->randomElement([ true , false ]);
        $data = [ 'name' => $name = $this->faker->sentence($nbWords, true) ];

        if (!$this->faker->randomElement([ true , false ]))
            $data['slug'] = $otherSlug ? $name : $this->faker->sentence($nbWords, true);
        else
            $data['slug'] = '';

        $response = $this->actingAs($this->auth, 'admin')
                        ->post(route(self::ROUTE_TO_STORE_TAXONOMIES), $data);

        $response->assertRedirect(route(self::ROUTE_TO_INDEX_TAXONOMIES));
        unset($data['slug']);
        $this->assertDatabaseHas('taxonomies', $data);
    }

    /** @test */
    public function validate_when_creating_a_new_taxonomy()
    {
        $data = [ 'name' => '' ];

        $response = $this->actingAs($this->auth, 'admin')
                        ->post(route(self::ROUTE_TO_STORE_TAXONOMIES), $data);

        $response->assertSessionHasErrors('name');
    }

    /** @test */
    public function validate_if_the_taxonomy_id_excites_in_show()
    {
        $taxonomy = factory(Taxonomy::class)->create();

        $response = $this->actingAs($this->auth, 'admin')
                        ->get(route(self::ROUTE_TO_SHOW_TAXONOMIES, $taxonomy->id+1));

        $response->assertRedirect(route(self::ROUTE_TO_INDEX_TAXONOMIES));
    }

    /** @test */
    public function validate_if_the_taxonomy_id_excites_in_edit()
    {
        $taxonomy = factory(Taxonomy::class)->create();

        $response = $this->actingAs($this->auth, 'admin')
                        ->get(route(self::ROUTE_TO_UPDATE_TAXONOMIES, $taxonomy->id+1));

        $response->assertRedirect(route(self::ROUTE_TO_INDEX_TAXONOMIES));
    }

    /** @test */
    public function update_existing_taxonomy()
    {
        $taxonomy = factory(Taxonomy::class, 3)->create()->random();
        $nbWords = $this->faker->numberBetween(1, 5);
        $otherSlug = $this->faker->randomElement([ true , false ]);
        $data = [ 'name' => $name = $this->faker->sentence($nbWords, true) ];

        if (!$this->faker->randomElement([ true , false ]))
            $data['slug'] = $otherSlug ? $name : $this->faker->sentence($nbWords, true);
        else
            $data['slug'] = '';

        $response = $this->actingAs($this->auth, 'admin')
                        ->put(route(self::ROUTE_TO_UPDATE_TAXONOMIES, $taxonomy->id), $data);

        $response->assertRedirect(route(self::ROUTE_TO_INDEX_TAXONOMIES));
        unset($data['slug']);
        $this->assertDatabaseHas('taxonomies', $data);
    }

    /** @test */
    public function validate_if_the_taxonomy_id_excites_in_update()
    {
        $taxonomy = factory(Taxonomy::class)->create();
        $nbWords = $this->faker->numberBetween(1, 5);
        $otherSlug = $this->faker->randomElement([ true , false ]);
        $data = [ 'name' => $name = $this->faker->sentence($nbWords, true) ];

        if (!$this->faker->randomElement([ true , false ]))
            $data['slug'] = $otherSlug ? $name : $this->faker->sentence($nbWords, true);
        else
            $data['slug'] = '';

        $response = $this->actingAs($this->auth, 'admin')
                        ->put(route(self::ROUTE_TO_UPDATE_TAXONOMIES, $taxonomy->id+1), $data);
        
        $response->assertRedirect(route(self::ROUTE_TO_INDEX_TAXONOMIES));
    }

    /** @test */
    public function validate_the_data_when_updating_the_taxonomy()
    {
        $taxonomy = factory(Taxonomy::class, 3)->create()->random();
        $data = [ 'name' => '' ];

        $response = $this->actingAs($this->auth, 'admin')
                        ->put(route(self::ROUTE_TO_UPDATE_TAXONOMIES, $taxonomy->id), $data);

        $response->assertSessionHasErrors('name');
    }

    /** @test */
    public function delete_existing_taxonomy()
    {
        $taxonomy = factory(Taxonomy::class, 3)->create()->random();

        $response = $this->actingAs($this->auth, 'admin')
                        ->delete(route(self::ROUTE_TO_DELETE_TAXONOMIES),[
                            'id' => $taxonomy->id
                        ]);

        $response->assertSuccessful();
        $this->assertDatabaseMissing('taxonomies', [
            'id'  => $taxonomy->id
        ]);
    }

    /** @test */
    public function validate_if_the_taxonomy_id_excites_in_delete()
    {
        $taxonomy = factory(Taxonomy::class)->create();

        $response = $this->actingAs($this->auth, 'admin')
                        ->delete(route(self::ROUTE_TO_DELETE_TAXONOMIES),[
                            'id' => $taxonomy->id+1
                        ]);

        $response->assertStatus(404);
        $this->assertDatabaseHas('taxonomies', [
            'id' => $taxonomy->id
        ]);
    }
}
