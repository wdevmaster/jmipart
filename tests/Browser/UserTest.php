<?php

namespace Tests\Browser;

use App\User;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Caffeinated\Shinobi\Models\Role;
use Tests\Browser\Pages\User\EditPage;
use Tests\Browser\Pages\User\IndexPage;
use Tests\Browser\Pages\User\CreatePage;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Foundation\Testing\WithFaker;

class UserTest extends DuskTestCase
{
    use WithFaker;

    const ROUTE_TO_INDEX_USER   = 'user.index';
    const ROUTE_TO_CREATE_USER  = 'user.create';
    const ROUTE_TO_STORE_USER   = 'user.store';
    const ROUTE_TO_EDIT_USER    = 'user.edit';
    const ROUTE_TO_UPDATE_USER  = 'user.update';
    const ROUTE_TO_DELETE_USER  = 'user.delete';
    const USER_PASSWORD = 'secret';

    protected $user;

    public function setUp(): void
    {
        parent::setUp();

        Artisan::call('db:seed');
        
        $this->user = $user = User::first();
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user, 'admin');
        });
    }

    /** @test */
    public function index_view_of_admin_users()
    {
        factory(User::class, 3)->create(['is_active' => true]);
        $users = User::with('roles')->get();
        $this->browse(function (Browser $browser) use ($users) {
            $browser->visit(new IndexPage)
                    ->assertSeeIndex()
                    ->assertSeeTableData($users);
        });
    }

    /** @test */
    public function create_new_user()
    {
        $role = factory(Role::class, 3)->create()->random();
        $data = [
            'name'      => $this->faker->firstName('male'|'female'),
            'email'     => $this->faker->unique()->safeEmail,
            'password'  => self::USER_PASSWORD,
            'is_active' => $this->faker->randomElement([1, 0]),
            'role'      => $role
        ];
        $this->browse(function (Browser $browser) use ($data) {
            $browser->visit(new IndexPage)
                    ->assertSeeLink(__('New User'))
                    ->clickLink(__('New User'))
                    ->on(new CreatePage)
                    ->assertSeeCreate()
                    ->createUser($data)
                    ->assertRouteIs(self::ROUTE_TO_INDEX_USER)
                    ->assertSeeIn('.alert', __('User has been created'));
        });
    }

    /** @test */
    public function validate_form_input_create_user()
    {
        $data = [
            'name'      => '',
            'email'     => '',
            'password'  => '',
        ];

        $this->browse(function (Browser $browser) use ($data) {
            $browser->visit(new IndexPage)
                    ->assertSeeLink(__('New User'))
                    ->clickLink(__('New User'))
                    ->on(new CreatePage)
                    ->assertSee(__('Enter Account Details'))
                    ->createUser($data)
                    ->assertRouteIs(self::ROUTE_TO_CREATE_USER)
                    ->assertSeeIn('.error-name', __('validation.required', [
                        'attribute' => __('name')
                    ]))
                    ->assertSeeIn('.error-email', __('validation.required', [
                        'attribute' => __('email')
                    ]))
                    ->assertSeeIn('.error-password', __('validation.required', [
                        'attribute' => __('password')
                    ]))
                    ->assertSeeIn('.error-roles', __('validation.required', [
                        'attribute' => __('roles')
                    ]));
        });
    }

    /** @test */
    public function validate_unique_when_creating_a_new_user()
    {
        $user = factory(User::class)->create();
        $role = factory(Role::class, 3)->create()->random();
        $data = [
            'name'      => $this->faker->firstName('male'|'female'),
            'email'     => $user->email,
            'password'  => self::USER_PASSWORD,
            'is_active' => $this->faker->randomElement([1, 0]),
            'role'      => $role
        ];

        $this->browse(function (Browser $browser) use ($data) {
            $browser->visit(new IndexPage)
                    ->assertSeeLink(__('New User'))
                    ->clickLink(__('New User'))
                    ->on(new CreatePage)
                    ->assertSee(__('Enter Account Details'))
                    ->createUser($data)
                    ->assertRouteIs(self::ROUTE_TO_CREATE_USER)
                    ->assertSeeIn('.error-email', __('validation.unique', [
                        'attribute' => __('email')
                    ]));
        });
    }

    /** @test */
    public function appearance_of_action_btn_user()
    {
        $user = factory(User::class, 3)->create()->random();

        $this->browse(function (Browser $browser) use($user) {
            $browser->visit(new IndexPage)
                    ->assertSee(__('Users'))
                    ->assertSeeActionBtn($user);
        });
    }

    /** @test */
    public function no_btn_for_user_with_full_access()
    {   
        $user = $this->user;
        $anotherUser = factory(User::class)->create();
        $moreUser = factory(User::class, 3)->create()->random();
        $role = factory(Role::class)->create();
        $role->givePermissionTo('user.index', 'user.edit', 'user.delete');
        $anotherUser->assignRoles($role->slug);

        $this->browse(function (Browser $browser) use($user, $anotherUser, $moreUser) {
            $browser->loginAs($anotherUser, 'admin')
                    ->visit(new IndexPage)
                    ->assertSee(__('Users'))
                    ->assertSeeActionBtn($user, false)
                    ->assertSeeActionBtn($moreUser);
        });
    }


    /** @test */
    public function no_delete_btn_for_users_with_logueados()
    {
        $user = factory(User::class)->create();
        $role = factory(Role::class)->create();
        $role->givePermissionTo('user.index', 'user.edit', 'user.delete');
        $user->assignRoles($role->slug);
        $anotherUser = factory(User::class, 3)->create()->random();

        $this->browse(function (Browser $browser) use($user, $anotherUser) {
            $browser->loginAs($user, 'admin') 
                    ->visit(new IndexPage)
                    ->assertSee(__('Users'))
                    ->assertSeeActionBtn($anotherUser)
                    ->with('.table .row-'.$user->id, function($tr) use ($user) {
                        $tr->assertSee($user->email)
                           ->assertVisible('.btn-edit')
                           ->assertSeeLink(__('Edit'))
                           ->assertMissing('.btn-delete');
                    });
        });
    }

    /** @test */
    public function no_delete_button_for_users_with_logueados_with_full_access()
    {
        $user = $this->user;
        $anotherUser = factory(User::class, 3)->create()->random();

        $this->browse(function (Browser $browser) use($user, $anotherUser) {
            $browser->loginAs($user, 'admin') 
                    ->visit(new IndexPage)
                    ->assertSee(__('Users'))
                    ->assertSeeActionBtn($anotherUser)
                    ->with('.table .row-'.$user->id, function($tr) use ($user) {
                        $tr->assertSee($user->email)
                           ->assertVisible('.btn-edit')
                           ->assertSeeLink(__('Edit'))
                           ->assertMissing('.btn-delete');
                    });
        });
    }

    /** @test */
    public function edit_user()
    {
        $user = factory(User::class, 3)->create()->random();
        $user->assignRoles(factory(Role::class)->create()->slug);
        $role = factory(Role::class, 3)->create()->random();
        $data = [
            'name'      => $this->faker->firstName('male'|'female'),
            'email'     => $this->faker->unique()->safeEmail,
            'password'  => '123456',
            'is_active' => $this->faker->randomElement([1, 0]),
            'role'      => $role
        ];

        $this->browse(function (Browser $browser) use($user, $data) {
            $browser->visit(new IndexPage)
                    ->assertSee(__('Users'))
                    ->assertSeeActionBtn($user)
                    ->with('.table .row-'.$user->id, function($row) use ($user) {
                        $row->clickLink(__('Edit'));
                    })
                    ->on(new EditPage($user))
                    ->assertSeeEdit()
                    ->editUser($data)
                    ->assertRouteIs(self::ROUTE_TO_INDEX_USER)
                    ->assertSeeIn('.alert', __('User has been updated'));
        });
    }

    /** @test */
    public function deny_access_to_non_existent_users()
    {
        $n = User::count() + 1;
        $this->browse(function (Browser $browser)  use ($n){
            $browser->visit(route(self::ROUTE_TO_EDIT_USER, rand($n, $n+3)))
                    ->assertRouteIs(self::ROUTE_TO_INDEX_USER)
                    ->assertSeeIn('.alert', __('The user was not found'));
        });
    }

    /** @test */
    public function delete_user()
    {
        $user = factory(User::class, 3)->create()->random();
        $user->assignRoles(factory(Role::class)->create()->slug);

        $this->browse(function (Browser $browser) use($user) {
            $browser->visit(new IndexPage)
                    ->assertSee(__('Users'))
                    ->assertSeeActionBtn($user)
                    ->assertSeeAlert($user)
                    ->deleteUser($user);
        });
    }

    /** @test */
    public function not_delete_role()
    {
        $user = factory(User::class, 3)->create()->random();
        $user->assignRoles(factory(Role::class)->create()->slug);

        $this->browse(function (Browser $browser) use($user) {
            $browser->visit(new IndexPage)
                    ->assertSee(__('Users'))
                    ->assertSeeActionBtn($user)
                    ->assertSeeAlert($user)
                    ->notDeleteUser($user);
        });
    }
}
 