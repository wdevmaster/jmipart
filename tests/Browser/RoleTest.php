<?php

namespace Tests\Browser;

use App\User;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Caffeinated\Shinobi\Models\Role;
use Tests\Browser\Pages\Role\IndexPage;
use Tests\Browser\Pages\Role\CreatePage;
use Tests\Browser\Pages\Role\EditPage;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Foundation\Testing\WithFaker;


class RoleTest extends DuskTestCase
{
    use WithFaker;

    const ROUTE_TO_INDEX_ROLE   = 'role.index';
    const ROUTE_TO_CREATE_ROLE  = 'role.create';
    const ROUTE_TO_STORE_ROLE   = 'role.store';
    const ROUTE_TO_EDIT_ROLE    = 'role.edit';
    const ROUTE_TO_UPDATE_ROLE  = 'role.update';
    const ROUTE_TO_DELETE_ROLE  = 'role.delete';

    public function setUp(): void
    {
        parent::setUp();

        Artisan::call('db:seed');
        
        $user = User::first();
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user, 'admin');
        });
    }

    /** @test */
    public function index_view_of_admin_roles()
    {
        $this->withoutExceptionHandling();
        $roles = Role::with('users')->get();
        $this->browse(function (Browser $browser) use ($roles) {
            $browser->visit(new IndexPage)
                    ->assertSeeIndex()
                    ->assertSeeTableData($roles);
        });
    }

    /** @test */
    public function see_all_roles_execpt_customer()
    {
        $roles = Role::with('users')->get();
        $this->browse(function (Browser $browser) use ($roles) {
            $browser->visit(new IndexPage)
                    ->assertSee(__('Roles'))
                    ->assertSeeTableData($roles);
        });
    }

    /** @test */
    public function create_new_role()
    {
        $permissions = $this->seedPermissions();
        $data = [
            'name' => $this->faker->jobTitle,
            'permissions' => $permissions
        ];
        $this->browse(function (Browser $browser) use ($data) {
            $browser->visit(new IndexPage)
                    ->assertSee(__('Roles'))
                    ->clickLink(__('New Role'))
                    ->on(new CreatePage)
                    ->assertSeeCreate()
                    ->createRole($data)
                    ->assertRouteIs(self::ROUTE_TO_INDEX_ROLE)
                    ->assertSeeIn('.alert', __('Role has been created'));
        });
    }

    /** @test */
    public function validate_form_input_create_roles()
    {
        $permissions = $this->seedPermissions();
        $data = [
            'name' => '',
        ];

        $this->browse(function (Browser $browser) use ($data) {
            $browser->visit(new IndexPage)
                    ->assertSee(__('Roles'))
                    ->clickLink(__('New Role'))
                    ->on(new CreatePage)
                    ->assertSee(__('New Role Details'))
                    ->createRole($data)
                    ->assertRouteIs(self::ROUTE_TO_CREATE_ROLE)
                    ->assertSeeIn('.error-name', __('validation.required', [
                        'attribute' => __('name')
                    ]));
        });
    }

    /** @test */
    public function appearance_of_action_btn_role()
    {
        $roles = factory(Role::class, 3)->create();
        $role = $roles->random();

        $this->browse(function (Browser $browser) use($role) {
            $browser->visit(new IndexPage)
                    ->assertSee(__('Roles'))
                    ->assertSeeActionBtn($role);
        });
    }

    /** @test */
    public function no_btn_for_role_with_full_access()
    {   
        $role = Role::first();
        factory(Role::class, 3)->create();
        
        $this->browse(function (Browser $browser) use($role) {
            $browser->visit(new IndexPage)
                    ->assertSee(__('Roles'))
                    ->assertSeeActionBtn($role, false);
        });
    }

    /** @test */
    public function update_role()
    {
        $roles = factory(Role::class, 3)->create();
        $role = $roles->random();
        $role->givePermissionTo($this->seedPermissions()->pluck('slug')->toArray());
        $data = [
            'name' => $this->faker->jobTitle,
            'permissions' => $this->seedPermissions()
        ];

        $this->browse(function (Browser $browser) use($role, $data) {
            $browser->visit(new IndexPage)
                    ->assertSee(__('Roles'))
                    ->assertSeeActionBtn($role)
                    ->with('.table .row-'.$role->id, function($row) use ($role) {
                        $row->clickLink(__('Edit'));
                    })
                    ->on(new EditPage($role))
                    ->assertSeeEdit()
                    ->editRole($data)
                    ->assertRouteIs(self::ROUTE_TO_INDEX_ROLE)
                    ->assertSeeIn('.alert', __('Role has been updated'));
        });

    }

    /** @test */
    public function deny_access_to_non_existent_roles()
    {
        $n = Role::count() + 1;
        $this->browse(function (Browser $browser)  use ($n){
            $browser->visit(route(self::ROUTE_TO_EDIT_ROLE, rand($n, $n+3)))
                    ->assertRouteIs(self::ROUTE_TO_INDEX_ROLE)
                    ->assertSeeIn('.alert', __('The role was not found'));
        });
    }

    /** @test */
    public function delete_role()
    {
        $roles = factory(Role::class, 3)->create();
        $role = $roles->random();

        $this->browse(function (Browser $browser) use($role) {
            $browser->visit(new IndexPage)
                    ->assertSee(__('Roles'))
                    ->assertSeeActionBtn($role)
                    ->assertSeeAlert($role)
                    ->deleteRole($role);
        });
    }

    /** @test */
    public function not_delete_role()
    {
        $roles = factory(Role::class, 3)->create();
        $role = $roles->random();

        $this->browse(function (Browser $browser) use($role) {
            $browser->visit(new IndexPage)
                    ->assertSee(__('Roles'))
                    ->assertSeeActionBtn($role)
                    ->assertSeeAlert($role)
                    ->notDeleteRole($role);
        });
    }
}
