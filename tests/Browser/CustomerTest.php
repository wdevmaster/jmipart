<?php

namespace Tests\Browser;

use App\User;
use App\Customer;
use App\CustomerType;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Support\Facades\Artisan;
use Tests\Browser\Pages\Customer\EditPage;
use Tests\Browser\Pages\Customer\ShowPage;
use Tests\Browser\Pages\Customer\IndexPage;
use Tests\Browser\Pages\Customer\CreatePage;
use Illuminate\Foundation\Testing\WithFaker;

class CustomerTest extends DuskTestCase
{
    use WithFaker;

    const ROUTE_TO_INDEX_CUSTOMER   = 'customer.index';
    const ROUTE_TO_CREATE_CUSTOMER  = 'customer.create';
    const ROUTE_TO_STORE_CUSTOMER   = 'customer.store';
    const ROUTE_TO_SHOW_CUSTOMER    = 'customer.show';
    const ROUTE_TO_EDIT_CUSTOMER    = 'customer.edit';
    const ROUTE_TO_UPDATE_CUSTOMER  = 'customer.update';
    const ROUTE_TO_DELETE_CUSTOMER  = 'customer.delete';
    const USER_PASSWORD = 'secret';

    public function setUp(): void
    {
        parent::setUp();

        Artisan::call('db:seed');
        
        $auth = User::first();
        $this->browse(function (Browser $browser) use ($auth) {
            $browser->loginAs($auth, 'admin');
        });
    }

    /** @test */
    public function index_view_of_admin_customer()
    {
        factory(Customer::class, 3)->create();
        $customers = Customer::get();

        $this->browse(function (Browser $browser) use ($customers) {
            $browser->visit(new IndexPage)
                    ->assertSeeIndex()
                    ->assertSeeTableData($customers);
        });
    }

    /** @test */
    public function appearance_of_action_btn_customer()
    {
        $customer = factory(Customer::class, 3)->create()->random();

        $this->browse(function (Browser $browser) use($customer) {
            $browser->visit(new IndexPage)
                    ->assertSee(__('Customers'))
                    ->assertSeeActionBtn($customer);
        });
    }

    /** @test */
    public function delete_customer()
    {
        $customer = factory(Customer::class, 3)->create()->random();

        $this->browse(function (Browser $browser) use($customer) {
            $browser->visit(new IndexPage)
                    ->assertSee(__('Customers'))
                    ->assertSeeActionBtn($customer)
                    ->assertSeeAlert($customer)
                    ->deleteCustomer($customer);
        });
    }

    /** @test */
    public function not_delete_customer()
    {
        $customer = factory(Customer::class, 3)->create()->random();

        $this->browse(function (Browser $browser) use($customer) {
            $browser->visit(new IndexPage)
                    ->assertSee(__('Customers'))
                    ->assertSeeActionBtn($customer)
                    ->assertSeeAlert($customer)
                    ->notDeleteCustomer($customer);
        });
    }

    /** @test */
    public function create_new_customer()
    {
        $types = CustomerType::choices();
        $data = [
            'type'      => $type = $this->faker->randomElement(CustomerType::toArray()),
            'firstname' => $this->faker->firstName('male'|'female'), 
            'lastname'  => $this->faker->lastName, 
            'email'     => $this->faker->unique()->safeEmail,
            'password'  => self::USER_PASSWORD,
        ];

        if ($type == 'organization')
            $data = array_merge($data, [
                'company_name'      => $this->faker->company,
                'tax_nr'            => $this->faker->ein,
                'registration_nr'   => $this->faker->ein,
            ]);

        $this->browse(function (Browser $browser) use ($data, $types) {
            $browser->visit(new IndexPage)
                    ->assertSeeLink(__('Create Customer'))
                    ->clickLink(__('Create Customer'))
                    ->on(new CreatePage)
                    ->assertSeeCreate($types)
                    ->createCustomer($data)
                    ->assertRouteIs(self::ROUTE_TO_INDEX_CUSTOMER)
                    ->assertSeeIn('.alert', __('Customer has been created'));
        });
    }

    /** @test */
    public function validate_form_input_create_customer()
    {
        $data = [
            'type'      => $type = $this->faker->randomElement(CustomerType::toArray()),
            'firstname' => '', 
            'lastname'  => '', 
            'email'     => '',
            'password'  => '',
        ];
        if ($type == 'organization')
            $data = array_merge($data, [
                'company_name'      => '',
                'tax_nr'            => '',
                'registration_nr'   => '',
            ]);

        $this->browse(function (Browser $browser) use ($data, $type) {
            $browser->visit(new IndexPage)
                    ->assertSeeLink(__('Create Customer'))
                    ->clickLink(__('Create Customer'))
                    ->on(new CreatePage)
                    ->assertSee(__('Enter Customer Details'))
                    ->createCustomer($data)
                    ->assertRouteIs(self::ROUTE_TO_CREATE_CUSTOMER)
                    ->assertSeeIn('.error-email', __('validation.required', [
                        'attribute' => __('email')
                    ]))
                    ->assertSeeIn('.error-password', __('validation.required', [
                        'attribute' => __('password')
                    ]));

            if ($type == 'organization')
                $browser->assertSeeIn('.error-company_name', __('validation.required_if', [
                    'attribute' => __('company name'),
                    'other'     => 'type',
                    'value'     =>  $type
                ]));
            elseif($type == 'individual')
                $browser->assertSeeIn('.error-firstname', __('validation.required_if', [
                    'attribute' => __('firstname'),
                    'other'     => 'type',
                    'value'     =>  $type
                ]))
                ->assertSeeIn('.error-lastname', __('validation.required_if', [
                    'attribute' => __('lastname'),
                    'other'     => 'type',
                    'value'     =>  $type
                ]));
        });
    }

    /** @test */
    public function validate_unique_when_creating_a_new_customer()
    {
        $customer = factory(Customer::class)->create();
        $types = CustomerType::choices();
        $data = [
            'type'      => $type = $this->faker->randomElement(CustomerType::toArray()),
            'firstname' => $this->faker->firstName('male'|'female'), 
            'lastname'  => $this->faker->lastName, 
            'email'     => $customer->email,
            'password'  => self::USER_PASSWORD,
        ];

        if ($type == 'organization')
            $data = array_merge($data, [
                'company_name'      => $this->faker->company,
                'tax_nr'            => $this->faker->ein,
                'registration_nr'   => $this->faker->ein,
            ]);

        $this->browse(function (Browser $browser) use ($data) {
            $browser->visit(new IndexPage)
                    ->assertSeeLink(__('Create Customer'))
                    ->clickLink(__('Create Customer'))
                    ->on(new CreatePage)
                    ->assertSee(__('Enter Customer Details'))
                    ->createCustomer($data)
                    ->assertRouteIs(self::ROUTE_TO_CREATE_CUSTOMER)
                    ->assertSeeIn('.error-email', __('validation.unique', [
                        'attribute' => __('email')
                    ]));
        });
    }

    /** @test */
    public function edit_customer()
    {
        $customer = factory(Customer::class)->create();
        $types = CustomerType::choices();
        $data = [
            'type'      => $type = $this->faker->randomElement(CustomerType::toArray()),
            'firstname' => $this->faker->firstName('male'|'female'), 
            'lastname'  => $this->faker->lastName, 
            'email'     => $this->faker->unique()->safeEmail,
            'password'  => self::USER_PASSWORD,
        ];

        if ($type == 'organization')
            $data = array_merge($data, [
                'company_name'      => $this->faker->company,
                'tax_nr'            => $this->faker->ein,
                'registration_nr'   => $this->faker->ein,
            ]);
        
        $this->browse(function (Browser $browser) use($customer, $data) {
            $browser->visit(new IndexPage)
                    ->assertSee(__('Customer'))
                    ->assertSeeActionBtn($customer)
                    ->with('.table .row-'.$customer->id, function($row) use ($customer) {
                        $row->clickLink(__('Edit'));
                    })
                    ->on(new EditPage($customer))
                    ->assertSeeEdit()
                    ->editCustomer($data)
                    ->assertRouteIs(self::ROUTE_TO_INDEX_CUSTOMER)
                    ->assertSeeIn('.alert', __('Customer has been updated'));
        });
    }

    /** @test */
    public function deny_access_to_non_existent_customer()
    {
        factory(Customer::class, rand(1, 5))->create();
        $n = Customer::count() + 1;
        $this->browse(function (Browser $browser)  use ($n){
            $browser->visit(route(self::ROUTE_TO_EDIT_CUSTOMER, rand($n, $n+3)))
                    ->assertRouteIs(self::ROUTE_TO_INDEX_CUSTOMER)
                    ->assertSeeIn('.alert', __('The customer was not found'));
        });
    }

    /** @test */
    public function show_customer()
    {   
        $customer = factory(Customer::class)->create();
        $this->browse(function (Browser $browser) use ($customer){
            $browser->visit(new IndexPage)
                    ->assertSeeLink(__('Create Customer'))
                    ->clickLink($customer->name)
                    ->on(new ShowPage($customer))
                    ->assertSeeShow();
        });
    }

    /** @test */
    public function customer_address_list()
    {
        $customer = factory(Customer::class)->create();
        $this->browse(function (Browser $browser) use ($customer){
            $browser->visit(new IndexPage)
                    ->assertSeeLink(__('Create Customer'))
                    ->clickLink($customer->name)
                    ->on(new ShowPage($customer))
                    ->assertSee(strtoupper($customer->name))
                    ->assertSeeTableAddresses();
        });
    }
}
