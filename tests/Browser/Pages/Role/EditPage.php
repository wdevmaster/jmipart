<?php

namespace Tests\Browser\Pages\Role;

use Laravel\Dusk\Browser;
use Tests\Browser\Pages\Page;

class EditPage extends Page
{
    const ROUTE_TO_EDIT_ROLE    = 'role.edit';
    const ROUTE_TO_UPDATE_ROLE  = 'role.update';

    protected $role;

    public function __construct($role) {
        $this->role = $role;
    }

    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return $this->getUrlRouter(route(self::ROUTE_TO_EDIT_ROLE, $this->role->id));
    }

    /**
     * Assert that the browser is on the page.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertPathIs($this->url())
                ->assertTitleContains(__('Editing role').' '.$this->role->name);
    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements()
    {
        return [
            '@name'    => 'input[name=name]',
        ];
    }

    public function assertSeeEdit(Browser $browser)
    {
        $browser->assertSee(__('Role Details'))
                ->assertVisible('@name')
                ->assertInputValue('@name', $this->role->name)
                ->assertSee(__('Save'))
                ->assertSee(__('Role permissions'))
                ->assertSeeLink(__('Cancel'))
                ->assertSeePermissions(false, $this->role);
    }

    public function editRole(Browser $browser, $data)
    {
        $browser->type('@name', $data['name']);

        if (array_key_exists('permissions', $data))
            foreach ($data['permissions']->pluck('slug') as $permission_slug)
                $browser->check('permissions['.$permission_slug.'][slug]');

        $browser->press(__('Save'));
    }
}
