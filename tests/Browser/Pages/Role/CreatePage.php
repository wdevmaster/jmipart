<?php

namespace Tests\Browser\Pages\Role;

use Laravel\Dusk\Browser;
use Tests\Browser\Pages\Page;

class CreatePage extends Page
{
    const ROUTE_TO_CREATE_ROLE  = 'role.create';
    const ROUTE_TO_STORE_ROLE   = 'role.store';
    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return $this->getUrlRouter(route(self::ROUTE_TO_CREATE_ROLE));
    }

    /**
     * Assert that the browser is on the page.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertPathIs($this->url())
                ->assertTitleContains(__('Create new role'));
    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements()
    {
        return [
            '@name'    => 'input[name=name]',
        ];
    }

    public function assertSeeCreate(Browser $browser)
    {
        $browser->assertSee(__('New Role Details'))
                ->assertVisible('@name')
                ->assertSee(__('Create role'))
                ->assertSee(__('Role permissions'))
                ->assertSeeLink(__('Cancel'))
                ->assertSeePermissions(false);
    }

    public function createRole(Browser $browser, $data)
    {
        $browser->type('@name', $data['name']);

        if (array_key_exists('permissions', $data))
            foreach ($data['permissions']->pluck('slug') as $permission_slug)
                $browser->check('permissions['.$permission_slug.'][slug]');

        $browser->press(__('Create role'));
    }
}
