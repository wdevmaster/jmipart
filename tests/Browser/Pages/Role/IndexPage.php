<?php

namespace Tests\Browser\Pages\Role;

use Laravel\Dusk\Browser;
use Tests\Browser\Pages\Page;

class IndexPage extends Page
{
    const ROUTE_TO_INDEX_ROLE   = 'role.index';
    const ROUTE_TO_CREATE_ROLE  = 'role.create';
    const ROUTE_TO_EDIT_ROLE    = 'role.edit';

    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return $this->getUrlRouter(route(self::ROUTE_TO_INDEX_ROLE));
    }

    /**
     * Assert that the browser is on the page.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertPathIs($this->url())
                ->assertTitleContains(__('Roles'));
    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements()
    {
        return [
            '@element' => '#selector',
        ];
    }

    public function assertSeeIndex(Browser $browser)
    {
        $browser->assertSee(__('Roles'))
                ->assertSeeLink(__('New Role'))
                ->with('.table', function($table) {
                    $table->with('thead', function($thead) {
                        $thead->assertSee(__('Name'))
                              ->assertSee( __('Users'))
                              ->assertSee(__('Last update'));
                    });
                });
    }

    public function assertSeeTableData(Browser $browser, $roles)
    {
        $browser->with('.table', function($table) use ($roles) {
            foreach ($roles as $role) {
                if ($role->slug == 'customer')
                    $table->assertDontSee($role->name);
                else
                    $table->assertSee($role->name)
                        ->assertSee($role->users->count());
            }
        });
    }

    public function assertSeeActionBtn(Browser $browser, $role, $dontSee = true)
    {    
        $browser->with('.table .row-'.$role->id, function($tr) use ($role, $dontSee) {
            $tr->assertSee($role->name)
                ->assertSee($role->users->count());
            if ($dontSee)
                $tr->assertVisible('.btn-edit')
                   ->assertSeeLink(__('Edit'))
                   ->assertVisible('.btn-delete')
                   ->assertSee(__('Remove'));
            else
                $tr->assertMissing('.btn-edit')
                   ->assertMissing('.btn-delete');
        });
    }

    public function assertSeeAlert(Browser $browser, $role)
    {
        $browser->with('.table .row-'.$role->id, function($tr) {
            $tr->click('.btn-delete');
        })
        ->waitFor('.swal2-container')
        ->waitForText(__('Are you sure?'));
    }

    public function deleteRole(Browser $browser, $role)
    {
        $browser->press(__('Yes, delete it!'))
                ->waitForText(__('Deleted!'))
                ->press(__('Ok'))
                ->waitUntilMissing('.swal2-container')
                ->assertMissing('.table .row-'.$role->id)
                ->assertDontSee($role->name);
    }

    public function notDeleteRole(Browser $browser, $role)
    {
        $browser->press(__('No, cancel!'))
                ->waitUntilMissing('.swal2-container')
                ->assertPresent('.table .row-'.$role->id)
                ->assertSee($role->name);
    }
}
