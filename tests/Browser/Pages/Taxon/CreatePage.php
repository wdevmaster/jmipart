<?php

namespace Tests\Browser\Pages\Taxon;

use Laravel\Dusk\Browser;
use Tests\Browser\Pages\Page;

class CreatePage extends Page
{
    const ROUTE_TO_CREATE_TAXONS  = 'taxon.create';
    const ROUTE_TO_STORE_TAXONS   = 'taxon.store';

    protected $taxonomy;

    public function __construct($taxonomy) {
        $this->taxonomy = $taxonomy;
    }

    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return $this->getUrlRouter(route(self::ROUTE_TO_CREATE_TAXONS, $this->taxonomy->id));
    }

    /**
     * Assert that the browser is on the page.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertPathIs($this->url())
                ->assertTitleContains(__('Create :category', [
                    'category' => str_singular($this->taxonomy->name)
                ]));
    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements()
    {
        return [
            '@name'     => 'input[name=name]',
            '@slug'     => 'input[name=slug]',
            '@parent'   => 'select[name=parent_id]',
            '@priority' => 'input[name=priority]',
            '@image'    => 'input[name=image]'
        ];
    }

    public function assertSeeCreate(Browser $browser)
    {
        $browser->assertSee(__(':category Details', ['category' => str_singular($this->taxonomy->name)]))
                ->assertVisible('@name')
                ->assertVisible('@slug')
                ->assertVisible('@parent')
                ->assertVisible('@priority')
                ->assertVisible('@image');
    }

    public function createTaxon(Browser $browser, $data)
    {
        $browser->type('@name', $data['name'])
                ->type('@slug', $data['slug']);

        if (isset($data['parent_id']))
            $browser->select('@parent', $data['parent_id']);

        if (isset($data['priority']))
            $browser->type('@priority', $data['priority']);

        if (isset($data['image']))
            $browser->attach('image', storage_path('app/testing/test_upload.jpg'));

        $browser->press(__('Create :category', ['category' => str_singular($this->taxonomy->name)]));
    }
}
