<?php

namespace Tests\Browser\Pages\Taxon;

use Laravel\Dusk\Browser;
use Tests\Browser\Pages\Page;

class EditPage extends Page
{
    const ROUTE_TO_EDIT_TAXONS    = 'taxon.edit';
    const ROUTE_TO_UPDATE_TAXONS  = 'taxon.update';

    protected $taxon;
    protected $taxonomy;

    public function __construct($taxonomy, $taxon) {
        $this->taxonomy = $taxonomy;
        $this->taxon = $taxon;
    }

    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return $this->getUrlRouter(route(self::ROUTE_TO_EDIT_TAXONS, [
            'taxonomy' => $this->taxonomy->id,
            'taxon' => $this->taxon->id
        ]));
    }

    /**
     * Assert that the browser is on the page.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertPathIs($this->url())
                ->assertTitleContains(__('Editing').' '.$this->taxon->name);
    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements()
    {
        return [
            '@name'     => 'input[name=name]',
            '@slug'     => 'input[name=slug]',
            '@parent'   => 'select[name=parent_id]',
            '@priority' => 'input[name=priority]',
            '@image'    => 'input[name=image]'
        ];
    }

    public function assertSeeEdit(Browser $browser)
    {
        $browser->assertSee(__(':category Data', ['category' => str_singular($this->taxonomy->name)]))
                ->assertInputValue('@name', $this->taxon->name)
                ->assertInputValue('@slug', $this->taxon->slug);
        
        if ($this->taxon->parent_id)
            $browser->assertSelected('@parent', $this->taxon->parent_id);

        $browser->assertInputValue('@priority', $this->taxon->priority)
                ->assertVisible('@image')
                ->assertSee(__('Save'))
                ->assertSeeLink(__('Cancel'));
    }

    public function editTaxon(Browser $browser, $data)
    {
        $browser->type('@name', $data['name'])
                ->type('@slug', $data['slug']);

        if (isset($data['parent_id']))
            $browser->select('@parent', $data['parent_id']);

        if (isset($data['priority']))
            $browser->type('@priority', $data['priority']);

        if (isset($data['image']))
            $browser->attach('image', storage_path('app/testing/test_upload.jpg'));
        
        $browser->press(__('Save'));
    }
}
