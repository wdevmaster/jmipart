<?php

namespace Tests\Browser\Pages;

use Laravel\Dusk\Browser;
use Laravel\Dusk\Page as BasePage;
use Caffeinated\Shinobi\Models\Role;
use Caffeinated\Shinobi\Models\Permission;

abstract class Page extends BasePage
{
    /**
     * Get the global element shortcuts for the site.
     *
     * @return array
     */
    public static function siteElements()
    {
        return [
            '@element' => '#selector',
        ];
    }

    public function getUrlRouter($route)
    {
        if($route)
            return str_replace(env('APP_URL'), '', $route);
    }

    public function assertSeePermissions(Browser $browser, $checked = true, $role = null)
    {
        $permissions = Permission::where('slug', 'not like', '%store%')
                        ->where('slug', 'not like', '%update%')
                        ->get();

        foreach ($permissions as $permission) {
            $browser->assertSee($permission->name);

            if (!$role)
                if ($checked)
                    $browser->assertChecked('permissions['.$permission->slug.'][slug]');
                else 
                    $browser->assertNotChecked('permissions['.$permission->slug.'][slug]');
            else
                if($role->hasPermissionTo($permission->slug))
                    $browser->assertChecked('permissions['.$permission->slug.'][slug]');
                else
                    $browser->assertNotChecked('permissions['.$permission->slug.'][slug]');
        }
    }

    public function assertSeeRoles(Browser $browser, $checked = true, $user = null)
    {
        $roles = Role::where('slug', 'not like', '%customer%')->get();

        foreach ($roles as $role) {
            $browser->assertSee($role->name);

            if (!$user)
                if($checked)
                    $browser->assertChecked('roles['.$role->slug.']');
                else 
                    $browser->assertNotChecked('roles['.$role->slug.']');
            else
                if($user->hasRole($role->slug))
                    $browser->assertChecked('roles['.$role->slug.']');
                else
                    $browser->assertNotChecked('roles['.$role->slug.']');
        }
    }
}
