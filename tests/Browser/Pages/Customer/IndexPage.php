<?php

namespace Tests\Browser\Pages\Customer;

use Laravel\Dusk\Browser;
use Tests\Browser\Pages\Page;

class IndexPage extends Page
{
    const ROUTE_TO_INDEX_CUSTOMER   = 'customer.index';
    const ROUTE_TO_CREATE_CUSTOMER  = 'customer.create';
    const ROUTE_TO_EDIT_CUSTOMER    = 'customer.edit';

    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return $this->getUrlRouter(route(self::ROUTE_TO_INDEX_CUSTOMER));
    }

    /**
     * Assert that the browser is on the page.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertPathIs($this->url())
                ->assertTitleContains(__('Customers'));
    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements()
    {
        return [
            '@element' => '#selector',
        ];
    }

    public function assertSeeIndex(Browser $browser)
    {
        $browser->assertSee(__('Customers'))
                ->assertSeeLink(__('Create Customer'))
                ->with('.table', function($table) {
                    $table->with('thead', function($thead) {
                        $thead->assertSee(__('Name'))
                              ->assertSee( __('Type'))
                              ->assertSee(__('Registered'));
                    });
                });
    }

    public function assertSeeTableData(Browser $browser, $customers)
    {
        $browser->with('.table', function($table) use ($customers) {
            foreach ($customers as $customer) {
                    $table->assertSee($customer->name)
                          ->assertSee($customer->type->label());
            }
        });
    }

    public function assertSeeActionBtn(Browser $browser, $customer)
    {    
        $browser->with('.table .row-'.$customer->id, function($tr) use ($customer) {
            $tr->assertSee($customer->name)
               ->assertVisible('.btn-edit')
               ->assertSeeLink(__('Edit'))
               ->assertVisible('.btn-delete')
               ->assertSee(__('Remove'));
        });
    }

    public function assertSeeAlert(Browser $browser, $customer)
    {
        $browser->with('.table .row-'.$customer->id, function($tr) {
            $tr->click('.btn-delete');
        })
        ->waitFor('.swal2-container')
        ->waitForText(__('Are you sure?'));
    }

    public function deleteCustomer(Browser $browser, $customer)
    {
        $browser->press(__('Yes, delete it!'))
                ->waitForText(__('Deleted!'))
                ->press(__('Ok'))
                ->waitUntilMissing('.swal2-container')
                ->assertMissing('.table .row-'.$customer->id)
                ->assertDontSee($customer->name);
    }

    public function notDeleteCustomer(Browser $browser, $customer)
    {
        $browser->press(__('No, cancel!'))
                ->waitUntilMissing('.swal2-container')
                ->assertPresent('.table .row-'.$customer->id)
                ->assertSee($customer->name);
    }
}
