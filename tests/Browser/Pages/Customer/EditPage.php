<?php

namespace Tests\Browser\Pages\Customer;

use Laravel\Dusk\Browser;
use Tests\Browser\Pages\Page;

class EditPage extends Page
{
    const ROUTE_TO_EDIT_CUSTOMER    = 'customer.edit';
    const ROUTE_TO_UPDATE_CUSTOMER  = 'customer.update';

    protected $customer;

    public function __construct($customer) {
        $this->customer = $customer;
    }
    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return $this->getUrlRouter(route(self::ROUTE_TO_EDIT_CUSTOMER, $this->customer->id));
    }

    /**
     * Assert that the browser is on the page.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertPathIs($this->url())
                ->assertTitleContains(__('Editing').' '.$this->customer->name);
    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements()
    {
        return [
            '@type'             => 'input[name=type]',
            '@firstname'        => 'input[name=firstname]',
            '@lastname'         => 'input[name=lastname]',
            '@email'            => 'input[name=email]',
            '@password'         => 'input[name=password]',
            '@company_name'     => 'input[name=company_name]',
            '@tax_nr'           => 'input[name=tax_nr]',
            '@registration_nr'  => 'input[name=registration_nr]',
        ];
    }

    public function assertSeeEdit(Browser $browser)
    {
        $browser->assertSee(__('Customer Details'))
                ->assertVisible('@type')
                ->assertInputValue('@firstname', $this->customer->firstname)
                ->assertInputValue('@lastname', $this->customer->lastname)
                ->assertInputValue('@email', $this->customer->email);

        if ($this->customer->type->isOrganization())
            $browser->assertInputValue('@company_name', $this->customer->company_name)
                    ->assertInputValue('@tax_nr', $this->customer->tax_nr)
                    ->assertInputValue('@tax_nr', $this->customer->tax_nr);

        $browser->assertSee(__('Save'))
                ->assertSeeLink(__('Cancel'));
    }

    public function editCustomer(Browser $browser, $data)
    {
        $browser->radio('type', $data['type'])
                ->type('@firstname', $data['firstname'])
                ->type('@lastname', $data['lastname'])
                ->type('@email', $data['email'])
                ->type('@password', $data['password']);

        if ($data['type'] == 'organization')
            $browser->waitFor('@company_name')
                    ->type('@company_name', $data['company_name'])
                    ->type('@tax_nr', $data['tax_nr'])
                    ->type('@registration_nr', $data['registration_nr']);

        $browser->press(__('Save'));
    }
}
