<?php

namespace Tests\Browser\Pages\Customer;

use Laravel\Dusk\Browser;
use Tests\Browser\Pages\Page;

class CreatePage extends Page
{
    const ROUTE_TO_CREATE_CUSTOMER  = 'customer.create';
    const ROUTE_TO_STORE_CUSTOMER   = 'customer.store';

    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return $this->getUrlRouter(route(self::ROUTE_TO_CREATE_CUSTOMER));
    }

    /**
     * Assert that the browser is on the page.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertPathIs($this->url())
                ->assertTitleContains(__('Create new customer'));
    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements()
    {
        return [
            '@type'             => 'input[name=type]',
            '@firstname'        => 'input[name=firstname]',
            '@lastname'         => 'input[name=lastname]',
            '@email'            => 'input[name=email]',
            '@password'         => 'input[name=password]',
            '@company_name'     => 'input[name=company_name]',
            '@tax_nr'           => 'input[name=tax_nr]',
            '@registration_nr'  => 'input[name=registration_nr]',
        ];
    }

    public function assertSeeCreate(Browser $browser, $types)
    {
        $browser->assertSee(__('Enter Customer Details'));
        foreach ($types as $key => $type) {
            $browser->assertSee($type);

            if ($type == 'individual')
                $browser->assertRadioSelected('@type', $key);
            elseif ($type == 'organization')
                $browser->assertRadioNotSelected('@type', $key);      
        }
        $browser->assertVisible('@firstname')
                ->assertVisible('@lastname')
                ->assertVisible('@email')
                ->assertVisible('@password')
                ->assertPresent('@company_name')
                ->assertMissing('@company_name')
                ->assertPresent('@tax_nr')
                ->assertMissing('@tax_nr')
                ->assertPresent('@registration_nr')
                ->assertMissing('@registration_nr');
    }

    public function createCustomer(Browser $browser, $data)
    {   
        $browser->radio('type', $data['type'])
                ->type('@firstname', $data['firstname'])
                ->type('@lastname', $data['lastname'])
                ->type('@email', $data['email'])
                ->type('@password', $data['password']);

        if ($data['type'] == 'organization')
            $browser->waitFor('@company_name')
                    ->type('@company_name', $data['company_name'])
                    ->type('@tax_nr', $data['tax_nr'])
                    ->type('@registration_nr', $data['registration_nr']);

        $browser->press(__('Create customer'));
    }
}
