<?php

namespace Tests\Browser\Pages\Customer;

use Laravel\Dusk\Browser;
use Tests\Browser\Pages\Page;

class ShowPage extends Page
{
    const ROUTE_TO_SHOW_CUSTOMER    = 'customer.show';

    protected $customer;

    public function __construct($customer) {
        $this->customer = $customer;
    }

    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return $this->getUrlRouter(route(self::ROUTE_TO_SHOW_CUSTOMER, $this->customer->id));
    }

    /**
     * Assert that the browser is on the page.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertPathIs($this->url())
                ->assertTitleContains(__('Viewing').' '.$this->customer->name);
    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements()
    {
        return [
            '@element' => '#selector',
        ];
    }

    public function assertSeeShow(Browser $browser)
    {
        $browser->assertSeeLink(__('Go back'))
                ->assertSee(strtoupper($this->customer->name))
                ->assertSee(strtoupper($this->customer->type->label()));

        if (!$this->customer->is_active)
            $browser->assertSee(strtoupper(__('Inactive')));

        $browser->assertSee(strtoupper(__('Last purchase')))
                ->assertSee(strtoupper(__('Customer since')))
                ->assertSee(__('Edit customer'))
                ->assertSee(__('Delete customer'));
    }

    public function assertSeeTableAddresses(Browser $browser, $address = null)
    {
        $browser->with('.card-address', function($card) use ($address) {
            $card->assertSee(__('Addresses'))
                 ->assertSeeLink(__('New Address'))
                 ->assertSee(__('Name'))
                 ->assertSee(__('Type'))
                 ->assertSee(__('Address'))
                 ->assertSee(__('Country'));

            if ($address == null)
                $card->assertSee(__('Customer has no addresses yet'));
            else 
                $card->with('.table .row-'.$address->id, function($tr) use ($address) {
                    $tr->assertSee($address->name)
                       ->assertSee($address->type->label())
                       ->assertVisible('.btn-delete')
                       ->assertSee(__('Remove'));
                });
        });
    }

    public function assertSeeAlert(Browser $browser, $address)
    {
        $browser->with('.table .row-'.$address->id, function($tr) {
            $tr->click('.btn-delete');
        })
        ->waitFor('.swal2-container')
        ->waitForText(__('Are you sure?'));
    }

    public function deleteCustomer(Browser $browser, $row)
    {
        $browser->press(__('Yes, delete it!'))
                ->waitForText(__('Deleted!'))
                ->press(__('Ok'))
                ->waitUntilMissing('.swal2-container')
                ->assertMissing('.table .row-'.$row->id)
                ->assertDontSee($row->name);
    }

    public function notDeleteData(Browser $browser, $row)
    {
        $browser->press(__('No, cancel!'))
                ->waitUntilMissing('.swal2-container')
                ->assertPresent('.table .row-'.$row->id)
                ->assertSee($row->name);
    }
}
