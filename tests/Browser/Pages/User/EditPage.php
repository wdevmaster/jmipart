<?php

namespace Tests\Browser\Pages\User;

use Laravel\Dusk\Browser;
use Tests\Browser\Pages\Page;

class EditPage extends Page
{
    const ROUTE_TO_EDIT_USER    = 'user.edit';
    const ROUTE_TO_UPDATE_USER  = 'user.update';

    protected $user;

    public function __construct($user) {
        $this->user = $user;
    }

    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return $this->getUrlRouter(route(self::ROUTE_TO_EDIT_USER, $this->user->id));
    }

    /**
     * Assert that the browser is on the page.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertPathIs($this->url())
                ->assertTitleContains(__('Editing').' '.$this->user->name);
    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements()
    {
        return [
            '@name'     => 'input[name=name]',
            '@email'    => 'input[name=email]',
            '@password' => 'input[name=password]',
        ];
    }

    public function assertSeeEdit(Browser $browser)
    {
        $browser->assertSee(__('User Account Data'))
                ->assertVisible('@name')
                ->assertInputValue('@name', $this->user->name)
                ->assertVisible('@email')
                ->assertInputValue('@email', $this->user->email)
                ->assertVisible('@password')
                ->assertRadioSelected('is_active', (int)$this->user->is_active)
                ->assertSee(__('Roles'))
                ->assertSeeRoles(false, $this->user)
                ->assertSee(__('Save'))
                ->assertSeeLink(__('Cancel'));
    }

    public function editUser(Browser $browser, $data)
    {
        $browser->type('@name', $data['name'])
                ->type('@email', $data['email'])
                ->type('@password', $data['password']);

        if (isset($data["is_active"]))
            $browser->radio('is_active', $data['is_active']);

        if (isset($data["role"])) {
            $role_slug = $data["role"]->slug;
            $browser->check('roles['.$role_slug.']');
        }

        $browser->press(__('Save'));
    }
}
