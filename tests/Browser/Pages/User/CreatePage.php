<?php

namespace Tests\Browser\Pages\User;

use Laravel\Dusk\Browser;
use Tests\Browser\Pages\Page;

class CreatePage extends Page
{
    const ROUTE_TO_CREATE_USER  = 'user.create';
    const ROUTE_TO_STORE_User   = 'user.store';
    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return $this->getUrlRouter(route(self::ROUTE_TO_CREATE_USER));
    }

    /**
     * Assert that the browser is on the page.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertPathIs($this->url())
                ->assertTitleContains(__('Create new user'));
    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements()
    {
        return [
            '@name'     => 'input[name=name]',
            '@email'    => 'input[name=email]',
            '@password' => 'input[name=password]',
        ];
    }

    public function assertSeeCreate(Browser $browser)
    {
        $browser->assertSee(__('Enter Account Details'))
                ->assertVisible('@name')
                ->assertVisible('@email')
                ->assertVisible('@password')
                ->assertRadioSelected('is_active', 0)
                ->assertRadioNotSelected('is_active', 1)
                ->assertSee(__('Roles'))
                ->assertSeeRoles(false)
                ->assertSee(__('Create user'))
                ->assertSeeLink(__('Cancel'));
    }

    public function createUser(Browser $browser, $data)
    {   
        $browser->type('@name', $data['name'])
                ->type('@email', $data['email'])
                ->type('@password', $data['password']);

        if (isset($data["is_active"]))
            $browser->radio('is_active', $data['is_active']);

        if (isset($data["role"])) {
            $role_slug = $data["role"]->slug;
            $browser->check('roles['.$role_slug.']');
        }

        $browser->press(__('Create user'));
    }
}
