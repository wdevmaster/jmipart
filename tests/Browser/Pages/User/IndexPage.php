<?php

namespace Tests\Browser\Pages\User;

use Laravel\Dusk\Browser;
use Tests\Browser\Pages\Page;

class IndexPage extends Page
{
    const ROUTE_TO_INDEX_USER   = 'user.index';
    const ROUTE_TO_CREATE_USER  = 'user.create';
    const ROUTE_TO_EDIT_USER    = 'user.edit';

    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return $this->getUrlRouter(route(self::ROUTE_TO_INDEX_USER));
    }

    /**
     * Assert that the browser is on the page.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertPathIs($this->url())
                ->assertTitleContains(__('Users'));
    }

    public function assertSeeIndex(Browser $browser)
    {
        $browser->assertSee(__('Users'))
                ->assertSeeLink(__('New User'))
                ->with('.table', function($table) {
                    $table->with('thead', function($thead) {
                        $thead->assertSee(__('E-mail'))
                              ->assertSee( __('Name'))
                              ->assertSee(__('Name'))
                              ->assertSee(__('Last login'));
                    });
                });
    }

    public function assertSeeTableData(Browser $browser, $users)
    {
        $browser->with('.table', function($table) use ($users) {
            foreach ($users as $user) {
                $table->assertSee($user->email)
                      ->assertSee($user->name);
            }
        });
    }

    public function assertSeeActionBtn(Browser $browser, $user, $dontSee = true)
    {    
        $browser->with('.table .row-'.$user->id, function($tr) use ($user, $dontSee) {
            $tr->assertSee($user->email);
            if ($dontSee)
                $tr->assertVisible('.btn-edit')
                   ->assertSeeLink(__('Edit'))
                   ->assertVisible('.btn-delete')
                   ->assertSee(__('Remove'));
            else
                $tr->assertMissing('.btn-edit')
                   ->assertMissing('.btn-delete');
        });
    }

    public function assertSeeAlert(Browser $browser, $user)
    {
        $browser->with('.table .row-'.$user->id, function($tr) {
            $tr->click('.btn-delete');
        })
        ->waitFor('.swal2-container')
        ->waitForText(__('Are you sure?'));
    }

    public function deleteUser(Browser $browser, $user)
    {
        $browser->press(__('Yes, delete it!'))
                ->waitForText(__('Deleted!'))
                ->press(__('Ok'))
                ->waitUntilMissing('.swal2-container')
                ->assertMissing('.table .row-'.$user->id)
                ->assertDontSee($user->name);
    }

    public function notDeleteUser(Browser $browser, $user)
    {
        $browser->press(__('No, cancel!'))
                ->waitUntilMissing('.swal2-container')
                ->assertPresent('.table .row-'.$user->id)
                ->assertSee($user->name);
    }
}
