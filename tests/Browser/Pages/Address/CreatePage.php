<?php

namespace Tests\Browser\Pages\Address;

use App\AddressType;
use Laravel\Dusk\Browser;
use Tests\Browser\Pages\Page;

class CreatePage extends Page
{
    const ROUTE_TO_CREATE_ADRESSES  = 'address.create';
    const ROUTE_TO_STORE_ADRESSES   = 'address.store';

    protected $customer;

    public function __construct($customer) {
        $this->customer = $customer;
    }

    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return $this->getUrlRouter(route(self::ROUTE_TO_CREATE_ADRESSES));
    }

    /**
     * Assert that the browser is on the page.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertPathIs($this->url())
                ->assertTitleContains(__('Create new address'));
    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements()
    {
        return [
            '@type'         => 'input[name=type]',
            '@name'         => 'input[name=name]',
            '@countryId'    => 'select[name=country_id]',
            '@province'     => 'input[name=province]',
            '@city'         => 'input[name=city]',
            '@postalcode'   => 'input[name=postalcode]',
            '@address'      => 'input[name=address]',
            '@search'       => 'input.mapboxgl-ctrl-geocoder--input'
        ];
    }

    public function assertSeeCreate(Browser $browser, $types)
    {
        $browser->assertSee(__('Address Details'))
                ->assertSee(__('Address for'))
                ->assertSee($this->customer->name);

        foreach ($types as $key => $type)
            $browser->assertSee($type);

        $browser->assertRadioSelected('@type', AddressType::defaultValue())
                ->assertVisible('@name')
                ->waitFor('@search')
                ->waitFor('.mapboxgl-canvas-container ')
                ->assertVisible('@countryId')
                ->assertVisible('@province')
                ->assertVisible('@city')
                ->assertVisible('@postalcode')
                ->assertVisible('@address');
    }

    public function createAddress(Browser $browser, $data)
    {
        $browser->click('label[for='.$data['type'].']')
                ->type('@name', $data['name'])
                ->type('@search', $data['address']);
                
        if ($data['address'])
            $browser->waitFor('.suggestions')
                    ->keys('@search', ['{ENTER}']);

        $browser->select('@countryId', $data['country_id'])
                ->type('@province', $data['province'])
                ->type('@city', $data['city'])
                ->type('@postalcode', $data['postalcode'])
                ->type('@address', $data['address']);
                
        if ($data['is_default'])
            $browser->click('label[for=checkIsDefault]');

        $browser->screenshot('createAddress')->press(__('Create address'));
    }
}
