<?php

namespace Tests\Browser\Pages\Taxonomy;

use Laravel\Dusk\Browser;
use Tests\Browser\Pages\Page;

class CreatePage extends Page
{
    const ROUTE_TO_CREATE_TAXONOMIES  = 'taxonomy.create';
    const ROUTE_TO_STORE_TAXONOMIES   = 'taxonomy.store';

    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return $this->getUrlRouter(route(self::ROUTE_TO_CREATE_TAXONOMIES));
    }

    /**
     * Assert that the browser is on the page.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertPathIs($this->url())
                ->assertTitleContains(__('Create Category Tree'));
    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements()
    {
        return [
            '@name' => 'input[name=name]',
            '@slug' => 'input[name=slug]',
        ];
    }

    public function assertSeeCreate(Browser $browser)
    {
        $browser->assertSee(__('Category Tree Details'))
                ->assertVisible('@name')
                ->assertVisible('@slug');
    }

    public function createTaxonomy(Browser $browser, $data)
    {
        $browser->type('@name', $data['name'])
                ->type('@slug', $data['slug'])
                ->press(__('Create category tree'));
    }
}
