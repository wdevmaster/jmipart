<?php

namespace Tests\Browser\Pages\Taxonomy;

use Laravel\Dusk\Browser;
use Tests\Browser\Pages\Page;

class ShowPage extends Page
{
    const ROUTE_TO_SHOW_TAXONOMIES    = 'taxonomy.show';

    protected $taxonomy;

    public function __construct($taxonomy) {
        $this->taxonomy = $taxonomy;
    }

    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return $this->getUrlRouter(route(self::ROUTE_TO_SHOW_TAXONOMIES, $this->taxonomy->id));
    }

    /**
     * Assert that the browser is on the page.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertPathIs($this->url())
                ->assertTitleContains(__('Viewing').' '.$this->taxonomy->name);
    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements()
    {
        return [
            '@element' => '#selector',
        ];
    }

    public function assertSeeShow(Browser $browser)
    {
        $browser->assertSeeLink(__('Go back'))
                ->assertSeeLink(__('Add :category', ['category' => str_singular($this->taxonomy->name)]))
                ->with('.table tbody', function($tbody) {
                    $tbody->assertSee(__('No category tree has been registered yet'));
                })
                ->assertSee(__('Edit Category Tree'))
                ->assertSee(__('Delete Category Tree'));
    }
}
