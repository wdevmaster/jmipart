<?php

namespace Tests\Browser\Pages\Taxonomy;

use Laravel\Dusk\Browser;
use Tests\Browser\Pages\Page;

class IndexPage extends Page
{
    const ROUTE_TO_INDEX_TAXONOMIES   = 'taxonomy.index';
    const ROUTE_TO_CREATE_TAXONOMIES  = 'taxonomy.create';
    const ROUTE_TO_SHOW_TAXONOMIES    = 'taxonomy.show';
    const ROUTE_TO_EDIT_TAXONOMIES    = 'taxonomy.edit';

    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return $this->getUrlRouter(route(self::ROUTE_TO_INDEX_TAXONOMIES));
    }

    /**
     * Assert that the browser is on the page.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertPathIs($this->url())
                ->assertTitleContains(__('Category Trees'));
    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements()
    {
        return [
            '@element' => '#selector',
        ];
    }

    public function assertSeeIndex(Browser $browser, $void = true)
    {
        $browser->assertSee(__('Category Trees'))
                ->assertSeeLink(__('New Category Tree'))
                ->with('.table thead', function($thead) {
                    $thead->assertSee(__('Name'))
                          ->assertSee( __('Slug'))
                          ->assertSee(__('Created'));
                });
                
        if (!$void)
            $browser->with('.table tbody', function($tbody) {
                $tbody->assertSee(__('No category tree has been registered yet'));
            });
    }

    public function assertSeeTableData(Browser $browser, $taxonomies)
    {
        $browser->with('.table tbody', function($table) use ($taxonomies) {
            foreach ($taxonomies as $taxonomy) 
                $table->assertSee($taxonomy->name)
                      ->assertSee($taxonomy->slug);
        });
    }

    public function assertSeeActionBtn(Browser $browser, $taxonomy)
    {
        $browser->with('.table .row-'.$taxonomy->id, function($tr) use ($taxonomy) {
            $tr->assertSee($taxonomy->name)
               ->assertVisible('.btn-edit')
               ->assertSeeLink(__('Edit'))
               ->assertVisible('.btn-delete')
               ->assertSee(__('Remove'));
        });
    }

    public function assertSeeAlert(Browser $browser, $taxonomy)
    {
        $browser->with('.table .row-'.$taxonomy->id, function($tr) {
            $tr->click('.btn-delete');
        })
        ->waitFor('.swal2-container')
        ->waitForText(__('Are you sure?'));
    }

    public function deleteTaxonomy(Browser $browser, $taxonomy)
    {
        $browser->press(__('Yes, delete it!'))
                ->waitForText(__('Deleted!'))
                ->press(__('Ok'))
                ->waitUntilMissing('.swal2-container')
                ->assertMissing('.table .row-'.$taxonomy->id)
                ->assertDontSee($taxonomy->name);
    }

    public function notDeleteTaxonomy(Browser $browser, $taxonomy)
    {
        $browser->press(__('No, cancel!'))
                ->waitUntilMissing('.swal2-container')
                ->assertPresent('.table .row-'.$taxonomy->id)
                ->assertSee($taxonomy->name);
    }
}
