<?php

namespace Tests\Browser\Pages\Taxonomy;

use Laravel\Dusk\Browser;
use Tests\Browser\Pages\Page;

class EditPage extends Page
{
    const ROUTE_TO_EDIT_TAXONOMIES    = 'taxonomy.edit';
    const ROUTE_TO_UPDATE_TAXONOMIES  = 'taxonomy.update';

    protected $taxonomy;

    public function __construct($taxonomy) {
        $this->taxonomy = $taxonomy;
    }

    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return $this->getUrlRouter(route(self::ROUTE_TO_EDIT_TAXONOMIES, $this->taxonomy->id));
    }

    /**
     * Assert that the browser is on the page.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertPathIs($this->url())
                ->assertTitleContains(__('Editing').' '.$this->taxonomy->name);
    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements()
    {
        return [
            '@name' => 'input[name=name]',
            '@slug' => 'input[name=slug]',
        ];
    }

    public function assertSeeEdit(Browser $browser)
    {
        $browser->assertSee(__('Category Tree Data'))
                ->assertInputValue('@name', $this->taxonomy->name)
                ->assertInputValue('@slug', $this->taxonomy->slug)
                ->assertSee(__('Save'))
                ->assertSeeLink(__('Cancel'));
    }

    public function editTaxonomy(Browser $browser, $data)
    {
        $browser->type('@name', $data['name'])
                ->type('@slug', $data['slug'])
                ->press(__('Save'));
    }
}
