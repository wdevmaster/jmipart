<?php

namespace Tests\Browser;

use App\User;
use App\Taxon;
use App\Taxonomy;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Support\Facades\Artisan;
use Tests\Browser\Pages\Taxon\EditPage;
use Tests\Browser\Pages\Taxon\CreatePage;
use Illuminate\Foundation\Testing\WithFaker;

class TaxonTest extends DuskTestCase
{
    use WithFaker;

    const ROUTE_TO_SHOW_TAXONOMIES= 'taxonomy.show';
    const ROUTE_TO_CREATE_TAXONS  = 'taxon.create';
    const ROUTE_TO_STORE_TAXONS   = 'taxon.store';
    const ROUTE_TO_EDIT_TAXONS    = 'taxon.edit';
    const ROUTE_TO_UPDATE_TAXONS  = 'taxon.update';
    const ROUTE_TO_DELETE_TAXONS  = 'taxon.delete';

    protected $taxonomy;

    public function setUp(): void
    {
        parent::setUp();

        Artisan::call('db:seed');
        
        $auth = User::first();
        $this->taxonomy = factory(Taxonomy::class, 3)->create()->random();
        $this->browse(function (Browser $browser) use ($auth) {
            $browser->loginAs($auth, 'admin');
        });
    }

    /** @test */
    public function see_the_taxon_tree_inside_taxonomy()
    {
        if($this->faker->randomElement([ true , false ])) {
            $taxon = factory(Taxon::class, 3)->create(['taxonomy_id' => $this->taxonomy->id])->random();
        }else 
            $taxon = null;

        $this->browse(function (Browser $browser) use ($taxon){
            $browser->visit(route(self::ROUTE_TO_SHOW_TAXONOMIES, $this->taxonomy->id))
                    ->assertSeeLink(__('Go back'))
                    ->assertSeeLink(__('Add :category', ['category' => str_singular($this->taxonomy->name)]));

            if ($taxon)
                $browser->with('.table .row-'.$taxon->id, function($tr) use ($taxon) {
                    $tr->assertSeeLink($taxon->name)
                    ->assertSee($taxon->children->count())
                    ->assertVisible('.btn-create')
                    ->assertSeeLink(__('Add Child :category', ['category' => str_singular($taxon->name)]))
                    ->assertVisible('.btn-delete')
                    ->assertSee(__('Remove'));
                });
            else
                $browser->with('.table tbody', function($tbody) {
                            $tbody->assertSee(__('No category tree has been registered yet'));
                        });

            $browser->assertSee(__('Edit Category Tree'))
                    ->assertSee(__('Delete Category Tree'));
        });
    }

    /** @test */
    public function create_a_new_taxon_within_taxonomy()
    {
        $nbWords = $this->faker->numberBetween(1, 5);
        $otherSlug = $this->faker->randomElement([ true , false ]);
        $data = [ 
            'name' => $name = $this->faker->sentence($nbWords, true),
            'image' => $this->faker->randomElement([ true , false ])
        ];
        if (!$this->faker->randomElement([ true , false ]))
            $data['slug'] = $otherSlug ? $name : $this->faker->sentence($nbWords, true);
        else
            $data['slug'] = '';

        $this->browse(function (Browser $browser) use ($data){
            $browser->visit(route(self::ROUTE_TO_SHOW_TAXONOMIES, $this->taxonomy->id))
                    ->clickLink(__('Add :category', ['category' => str_singular($this->taxonomy->name)]))
                    ->on(new CreatePage($this->taxonomy))
                    ->assertSeeCreate()
                    ->createTaxon($data)
                    ->assertRouteIs(self::ROUTE_TO_SHOW_TAXONOMIES, $this->taxonomy->id)
                    ->assertSeeIn('.alert', __(':name :taxonomy has been created', [
                        'name' => $data['name'],
                        'taxonomy' => str_singular($this->taxonomy->name)
                    ]))
                    ->with('.table tbody', function($table) use ($data) {
                        $table->assertSee($data['name']);
                    });
        });
    }

    /** @test */
    public function validate_form_entry_to_create_taxon_within_taxonomy()
    {
        $data = [ 'name' => '', 'slug' => '' ];

        $this->browse(function (Browser $browser) use ($data){
            $browser->visit(route(self::ROUTE_TO_SHOW_TAXONOMIES, $this->taxonomy->id))
                    ->clickLink(__('Add :category', ['category' => str_singular($this->taxonomy->name)]))
                    ->on(new CreatePage($this->taxonomy))
                    ->assertSee(__(':category Details', ['category' => str_singular($this->taxonomy->name)]))
                    ->createTaxon($data)
                    ->assertRouteIs(self::ROUTE_TO_CREATE_TAXONS, $this->taxonomy->id)
                    ->assertSeeIn('.error-name', __('validation.required', [
                        'attribute' => __('name')
                    ]));
        });
    }

    /** @test */
    public function create_a_new_taxon_inside_another_taxon()
    {
        $taxon = factory(Taxon::class, 3)->create(['taxonomy_id' => $this->taxonomy->id])->random();
        $nbWords = $this->faker->numberBetween(1, 5);
        $otherSlug = $this->faker->randomElement([ true , false ]);
        $data = [ 
            'name' => $name = $this->faker->sentence($nbWords, true) 
        ];
        if (!$this->faker->randomElement([ true , false ]))
            $data['slug'] = $otherSlug ? $name : $this->faker->sentence($nbWords, true);
        else
            $data['slug'] = '';

        $this->browse(function (Browser $browser) use ($taxon, $data) {
            $browser->visit(route(self::ROUTE_TO_SHOW_TAXONOMIES, $this->taxonomy->id))
                    ->with('.table .row-'.$taxon->id, function($tr) use ($taxon) {
                        $tr->clickLink(__('Add Child :category', ['category' => str_singular($taxon->name)]));
                    })
                    ->on(new CreatePage($this->taxonomy))
                    ->assertSee(__(':category Details', ['category' => str_singular($this->taxonomy->name)]))
                    ->createTaxon($data)
                    ->assertRouteIs(self::ROUTE_TO_SHOW_TAXONOMIES, $this->taxonomy->id)
                    ->assertSeeIn('.alert', __(':name :taxonomy has been created', [
                        'name' => $data['name'],
                        'taxonomy' => str_singular($this->taxonomy->name)
                    ]))
                    ->click('.row-'.$taxon->id)
                    ->assertSee($data['name']);
        });
    }

    /** @test */
    public function validate_form_entry_to_create_taxon_within_taxon()
    {
        $taxon = factory(Taxon::class, 3)->create(['taxonomy_id' => $this->taxonomy->id])->random();
        $data = [ 'name' => '', 'slug' => '' ];

        $this->browse(function (Browser $browser) use ($taxon, $data) {
            $browser->visit(route(self::ROUTE_TO_SHOW_TAXONOMIES, $this->taxonomy->id))
                    ->with('.table .row-'.$taxon->id, function($tr) use ($taxon) {
                        $tr->clickLink(__('Add Child :category', ['category' => str_singular($taxon->name)]));
                    })
                    ->on(new CreatePage($this->taxonomy))
                    ->assertSee(__(':category Details', ['category' => str_singular($this->taxonomy->name)]))
                    ->createTaxon($data)
                    ->assertRouteIs(self::ROUTE_TO_CREATE_TAXONS, $this->taxonomy->id)
                    ->assertSeeIn('.error-name', __('validation.required', [
                        'attribute' => __('name')
                    ]));
        });
    }

    /** @test */
    public function edit_a_taxon_within_taxonomy()
    {
        $taxon = factory(Taxon::class, 3)->create(['taxonomy_id' => $this->taxonomy->id])->random();
        $nbWords = $this->faker->numberBetween(1, 5);
        $otherSlug = $this->faker->randomElement([ true , false ]);
        $data = [ 
            'name' => $name = $this->faker->sentence($nbWords, true),
            'image' => $this->faker->randomElement([ true , false ])
        ];
        if (!$this->faker->randomElement([ true , false ]))
            $data['slug'] = $otherSlug ? $name : $this->faker->sentence($nbWords, true);
        else
            $data['slug'] = '';

        $this->browse(function (Browser $browser) use ($taxon, $data) {
            $browser->visit(route(self::ROUTE_TO_SHOW_TAXONOMIES, $this->taxonomy->id)) 
                    ->with('.table .row-'.$taxon->id, function($tr) use ($taxon) {
                        $tr->clickLink($taxon->name);
                    })
                    ->on(new EditPage($this->taxonomy, $taxon))
                    ->assertSeeEdit()
                    ->editTaxon($data)
                    ->assertRouteIs(self::ROUTE_TO_SHOW_TAXONOMIES, $this->taxonomy->id)
                    ->assertSeeIn('.alert', __(':name has been updated', ['name' => $data['name']]))
                    ->with('.table tbody', function($table) use ($data) {
                        $table->assertSee($data['name']);
                    });
        });
    }

    /** @test */
    public function validate_form_entry_to_edit_taxon_within_taxonomy()
    {
        $taxon = factory(Taxon::class, 3)->create(['taxonomy_id' => $this->taxonomy->id])->random();
        $data = [ 'name' => '', 'slug' => '' ];

        $this->browse(function (Browser $browser) use ($taxon, $data){
            $browser->visit(route(self::ROUTE_TO_SHOW_TAXONOMIES, $this->taxonomy->id))
                    ->with('.table .row-'.$taxon->id, function($tr) use ($taxon) {
                        $tr->clickLink($taxon->name);
                    })
                    ->on(new EditPage($this->taxonomy, $taxon))
                    ->assertSee(__(':category Data', ['category' => str_singular($this->taxonomy->name)]))
                    ->editTaxon($data)
                    ->assertRouteIs(self::ROUTE_TO_EDIT_TAXONS, [$this->taxonomy, $taxon])
                    ->assertSeeIn('.error-name', __('validation.required', [
                        'attribute' => __('name')
                    ]));
        });
    }

    /** @test */
    public function edit_a_taxon_inside_another_taxon()
    {
        $taxon = factory(Taxon::class, 3)->create(['taxonomy_id' => $this->taxonomy->id])->random();
        $children = factory(Taxon::class, 3)->create([
            'parent_id' => $taxon->id,
            'taxonomy_id' => $this->taxonomy->id
        ])->random();
        $nbWords = $this->faker->numberBetween(1, 5);
        $otherSlug = $this->faker->randomElement([ true , false ]);
        $data = [ 
            'name' => $name = $this->faker->sentence($nbWords, true) 
        ];
        if (!$this->faker->randomElement([ true , false ]))
            $data['slug'] = $otherSlug ? $name : $this->faker->sentence($nbWords, true);
        else
            $data['slug'] = '';

        $this->browse(function (Browser $browser) use ($taxon, $children, $data) {
            $browser->visit(route(self::ROUTE_TO_SHOW_TAXONOMIES, $this->taxonomy->id)) 
                    ->click('.row-'.$taxon->id)
                    ->clickLink($children->name)
                    ->on(new EditPage($this->taxonomy, $children))
                    ->assertSeeEdit()
                    ->editTaxon($data)
                    ->assertRouteIs(self::ROUTE_TO_SHOW_TAXONOMIES, $this->taxonomy->id)
                    ->assertSeeIn('.alert', __(':name has been updated', ['name' => $data['name']]))
                    ->click('.row-'.$taxon->id)
                    ->assertSee($data['name']);
        });

    }

    /** @test */
    public function validate_form_entry_to_edit_taxon_within_taxon()
    {
        $taxon = factory(Taxon::class, 3)->create(['taxonomy_id' => $this->taxonomy->id])->random();
        $children = factory(Taxon::class, 3)->create([
            'parent_id' => $taxon->id,
            'taxonomy_id' => $this->taxonomy->id
        ])->random();
        $data = [ 'name' => '', 'slug' => '' ];

        $this->browse(function (Browser $browser) use ($taxon, $children, $data) {
            $browser->visit(route(self::ROUTE_TO_SHOW_TAXONOMIES, $this->taxonomy->id)) 
                    ->click('.row-'.$taxon->id)
                    ->clickLink($children->name)
                    ->on(new EditPage($this->taxonomy, $children))
                    ->assertSeeEdit()
                    ->editTaxon($data)
                    ->assertRouteIs(self::ROUTE_TO_EDIT_TAXONS, [$this->taxonomy, $children])
                    ->assertSeeIn('.error-name', __('validation.required', [
                        'attribute' => __('name')
                    ]));
        });
    }
}
