<?php

namespace Tests\Browser;

use App\User;
use App\Country;
use App\Address;
use App\Province;
use App\Customer;
use App\AddressType;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Support\Facades\Artisan;
use Tests\Browser\Pages\Customer\ShowPage;
use Tests\Browser\Pages\Customer\IndexPage;
use Tests\Browser\Pages\Address\CreatePage;
use Illuminate\Foundation\Testing\WithFaker;

class AddressTest extends DuskTestCase
{
    use WithFaker;

    const ROUTE_TO_SHOW_CUSTOMER    = 'customer.show';

    const ROUTE_TO_CREATE_ADRESSES  = 'address.create';
    const ROUTE_TO_STORE_ADRESSES   = 'address.store';
    const ROUTE_TO_DELETE_ADRESSES  = 'address.delete';

    protected $customer;

    public function setUp(): void
    {
        parent::setUp();

        Artisan::call('db:seed');
        
        $auth = User::first();
        $this->browse(function (Browser $browser) use ($auth) {
            $browser->loginAs($auth, 'admin');
        });
        $this->customer = factory(Customer::class)->create();
    }

    /** @test */
    public function create_new_customer_address()
    {
        $types = AddressType::choices();
        $country = Country::all()->random();
        $data = [
            'type'          => $this->faker->randomElement(AddressType::toArray()),
            'name'          => $this->faker->sentence(2, true),
            'address'       => $this->faker->address,
            'country_id'    => $country->id,
            'province'      => $this->faker->state,
            'city'          => $this->faker->city,
            'postalcode'    => $this->faker->postcode,
            'is_default'    => $this->faker->randomElement([true, false]),
        ];

        $this->browse(function (Browser $browser) use ($data, $types){
            $browser->visit(new IndexPage)
                    ->assertSeeLink(__('Create Customer'))
                    ->clickLink($this->customer->name)
                    ->assertSee(strtoupper($this->customer->name))
                    ->assertSee(__('Addresses'))
                    ->assertSeeLink(__('New Address'))
                    ->clickLink(__('New Address'))
                    ->on(new CreatePage($this->customer))
                    ->assertSeeCreate($types) 
                    ->createAddress($data)
                    ->assertRouteIs(self::ROUTE_TO_SHOW_CUSTOMER, ['customer' => $this->customer->id])
                    ->assertSeeIn('.alert', __('Address has been created for :name', [
                        'name' => $this->customer->name
                    ]));
        });
    }

    /** @test */
    public function validate_form_input_create_address()
    {
        $country = Country::all()->random();
        $types = AddressType::choices();
        $data = [
            'type'          => $this->faker->randomElement(AddressType::toArray()),
            'name'          => '',
            'address'       => '',
            'country_id'    => $country->id,
            'province'      => '',
            'city'          => '',
            'postalcode'    => '',
            'is_default'    => $this->faker->randomElement([true, false]),
        ];
        
        $this->browse(function (Browser $browser) use ($data){
            $browser->visit(new IndexPage)
                    ->assertSeeLink(__('Create Customer'))
                    ->clickLink($this->customer->name)
                    ->assertSee(strtoupper($this->customer->name))
                    ->assertSee(__('Addresses'))
                    ->assertSeeLink(__('New Address'))
                    ->clickLink(__('New Address'))
                    ->on(new CreatePage($this->customer))
                    ->createAddress($data)
                    ->assertRouteIs(self::ROUTE_TO_CREATE_ADRESSES)
                    ->assertSeeIn('.error-name', __('validation.required', [
                        'attribute' => __('name')
                    ]))
                    ->assertSeeIn('.error-address', __('validation.required', [
                        'attribute' => __('address')
                    ]))
                    ->assertSeeIn('.error-postalcode', __('validation.required', [
                        'attribute' => __('postalcode')
                    ]))
                    ->assertSeeIn('.error-city', __('validation.required', [
                        'attribute' => __('city')
                    ]))
                    ->assertSeeIn('.error-province', __('validation.required', [
                        'attribute' => __('province')
                    ]));

        });
    }

    /** @test */
    public function delete_address()
    {   
        $country = Country::all()->random();
        $province = factory(Province::class)->create([
            'country_id' => $country->id
        ]);
        $address = factory(Address::class)->create([
            'country_id' => $country->id,
            'province_id' => $province->id
        ]);
        $address->customers()->attach($this->customer->id);

        $this->browse(function (Browser $browser) use ($address){
            $browser->visit(new IndexPage)
                    ->assertSeeLink(__('Create Customer'))
                    ->clickLink($this->customer->name)
                    ->on(new ShowPage($this->customer))
                    ->assertSee(strtoupper($this->customer->name))
                    ->assertSee(__('Addresses'))
                    ->assertSeeLink(__('New Address'))
                    ->assertSeeTableAddresses($address)
                    ->assertSeeAlert($address)
                    ->deleteCustomer($address)
                    ->assertRouteIs(self::ROUTE_TO_SHOW_CUSTOMER, ['customer' => $this->customer->id]);
        });
    }

    /** @test */
    public function not_delete_address()
    {
        $country = Country::all()->random();
        $province = factory(Province::class)->create([
            'country_id' => $country->id
        ]);
        $address = factory(Address::class)->create([
            'country_id' => $country->id,
            'province_id' => $province->id
        ]);
        $address->customers()->attach($this->customer->id);
        
        $this->browse(function (Browser $browser) use ($address){
            $browser->visit(new IndexPage)
                    ->assertSeeLink(__('Create Customer'))
                    ->clickLink($this->customer->name)
                    ->on(new ShowPage($this->customer))
                    ->assertSee(strtoupper($this->customer->name))
                    ->assertSee(__('Addresses'))
                    ->assertSeeLink(__('New Address'))
                    ->assertSeeTableAddresses($address)
                    ->assertSeeAlert($address)
                    ->notDeleteData($address)
                    ->assertRouteIs(self::ROUTE_TO_SHOW_CUSTOMER, ['customer' => $this->customer->id]);
        });

    }
}
