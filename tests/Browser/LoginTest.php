<?php

namespace Tests\Browser;

use App\User;
use App\Customer;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Tests\Browser\Pages\LoginPage;
use Caffeinated\Shinobi\Models\Role;

class LoginTest extends DuskTestCase
{
    const USER_PASSWORD = 'secret';
    const ROUTE_LOGIN = '/login';
    const REDIRECT_TO = '/home';
    const REDIRECT_TO_ADMIN = '/admin/dashboard';

    /** @test */
    public function user_login_successfully()
    {
        $customer = factory(Customer::class)->create([
            'password' => bcrypt($password = self::USER_PASSWORD),
            'is_active' => true
        ]);
        $credentials = [
            'email' => $customer->email,
            'password' => $password,
        ];

        $this->browse(function (Browser $browser) use ($credentials) {
            $browser->visit(new LoginPage)
                    ->addCredentialsForm($credentials)
                    ->press('@submit')
                    ->assertPathIs(self::REDIRECT_TO);
        });
    }

    /** @test */
    public function redirect_user_with_role_other_than_customers()
    {
        $user = factory(User::class)->create([
            'password' => bcrypt($password = self::USER_PASSWORD),
            'is_active' => true
        ]);
        $role = factory(Role::class)->create([
            'name' => 'admin',
            'slug' => 'admin',
            'special' => 'all-access',
        ]);
        $user->assignRoles($role->name);
        $credentials = [
            'email' => $user->email,
            'password' => $password,
        ];

        $this->browse(function (Browser $browser) use ($credentials) {
            $browser->visit(new LoginPage)
                    ->addCredentialsForm($credentials)
                    ->press('@submit')
                    ->assertPathIs(self::REDIRECT_TO_ADMIN)
                    ->assertDontSee('Not Found');
        });
    }

    /** @test */
    public function deny_customer_access_to_admin_routes()
    {
        $customer = factory(Customer::class)->create([
            'password' => bcrypt(self::USER_PASSWORD),
        ]);

        $this->browse(function (Browser $browser) use ($customer) {
            $browser->loginAs($customer)
                    ->visit(self::REDIRECT_TO_ADMIN)
                    ->assertPathIs(self::REDIRECT_TO);
        });
    }

    /** @test */
    public function allow_access_to_roles_other_than_the_customer()
    {
        $user = factory(User::class)->create([
            'password' => bcrypt(self::USER_PASSWORD),
        ]);
        $role = factory(Role::class)->create([
            'name' => 'admin',
            'slug' => 'admin',
            'special' => 'all-access',
        ]);
        $user->assignRoles($role->name);

        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user, 'admin')
                    ->visit(self::REDIRECT_TO_ADMIN)
                    ->assertPathIs(self::REDIRECT_TO_ADMIN)
                    ->assertDontSee('Not Found');
        });
    }

}
