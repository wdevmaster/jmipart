<?php

namespace Tests\Browser;

use App\User;
use App\Taxonomy;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Support\Facades\Artisan;
use Tests\Browser\Pages\Taxonomy\EditPage;
use Tests\Browser\Pages\Taxonomy\ShowPage;
use Tests\Browser\Pages\Taxonomy\IndexPage;
use Tests\Browser\Pages\Taxonomy\CreatePage;
use Illuminate\Foundation\Testing\WithFaker;

class TaxonomyTest extends DuskTestCase
{
    use WithFaker;

    const ROUTE_TO_INDEX_TAXONOMIES   = 'taxonomy.index';
    const ROUTE_TO_CREATE_TAXONOMIES  = 'taxonomy.create';
    const ROUTE_TO_STORE_TAXONOMIES   = 'taxonomy.store';
    const ROUTE_TO_SHOW_TAXONOMIES    = 'taxonomy.show';
    const ROUTE_TO_EDIT_TAXONOMIES    = 'taxonomy.edit';
    const ROUTE_TO_UPDATE_TAXONOMIES  = 'taxonomy.update';
    const ROUTE_TO_DELETE_TAXONOMIES  = 'taxonomy.delete';

    public function setUp(): void
    {
        parent::setUp();

        Artisan::call('db:seed');
        
        $auth = User::first();
        $this->browse(function (Browser $browser) use ($auth) {
            $browser->loginAs($auth, 'admin');
        });
    }

    /** @test */
    public function index_view_of_admin_taxonomy()
    {
        $taxonomies = factory(Taxonomy::class, 3)->create();
        $this->browse(function (Browser $browser) use ($taxonomies) {
            $browser->visit(new IndexPage)
                    ->assertSeeIndex()
                    ->assertSeeTableData($taxonomies);
        });
    }
 
    /** @test */
    public function appearance_of_action_btn_taxonomy()
    {
        $taxonomy = factory(Taxonomy::class, 3)->create()->random();

        $this->browse(function (Browser $browser) use($taxonomy) {
            $browser->visit(new IndexPage)
                    ->assertSee(__('Category Trees'))
                    ->assertSeeActionBtn($taxonomy);
        });
    }

    /** @test */
    public function delete_taxonomy()
    {
        $taxonomy = factory(Taxonomy::class, 3)->create()->random();

        $this->browse(function (Browser $browser) use($taxonomy) {
            $browser->visit(new IndexPage)
                    ->assertSee(__('Category Trees'))
                    ->assertSeeActionBtn($taxonomy)
                    ->assertSeeAlert($taxonomy)
                    ->deleteTaxonomy($taxonomy);
        });
    }

    /** @test */
    public function not_delete_taxonomy()
    {
        $taxonomy = factory(Taxonomy::class, 3)->create()->random();

        $this->browse(function (Browser $browser) use($taxonomy) {
            $browser->visit(new IndexPage)
                    ->assertSee(__('Category Trees'))
                    ->assertSeeActionBtn($taxonomy)
                    ->assertSeeAlert($taxonomy)
                    ->notDeleteTaxonomy($taxonomy);
        });
    }

    /** @test */
    public function create_new_taxonomy()
    {
        $nbWords = $this->faker->numberBetween(1, 5);
        $otherSlug = $this->faker->randomElement([ true , false ]);
        $data = [ 'name' => $name = $this->faker->sentence($nbWords, true) ];

        if (!$this->faker->randomElement([ true , false ]))
            $data['slug'] = $otherSlug ? $name : $this->faker->sentence($nbWords, true);
        else
            $data['slug'] = '';

        $this->browse(function (Browser $browser) use ($data) {
            $browser->visit(new IndexPage)
                    ->assertSee(__('Category Trees'))
                    ->clickLink(__('New Category Tree'))
                    ->on(new CreatePage)
                    ->assertSeeCreate()
                    ->createTaxonomy($data)
                    ->assertRouteIs(self::ROUTE_TO_INDEX_TAXONOMIES)
                    ->assertSeeIn('.alert', __(':name has been created', ['name' => $data['name']]))
                    ->with('.table tbody', function($table) use ($data) {
                        $table->assertSee($data['name']);
                    });
        });
    }

    /** @test */
    public function validate_form_input_create_taxonomy()
    {
        $data = [ 
            'name' => '',
            'slug' => '' 
        ];

        $this->browse(function (Browser $browser) use ($data) {
            $browser->visit(new IndexPage)
                    ->assertSee(__('Category Trees'))
                    ->clickLink(__('New Category Tree'))
                    ->on(new CreatePage)
                    ->assertSee(__('Category Tree Details'))
                    ->createTaxonomy($data)
                    ->assertRouteIs(self::ROUTE_TO_CREATE_TAXONOMIES)
                    ->assertSeeIn('.error-name', __('validation.required', [
                        'attribute' => __('name')
                    ]));
        });
    }

    /** @test */
    public function edit_taxonomy()
    {
        $taxonomy = factory(Taxonomy::class, 3)->create()->random();
        $nbWords = $this->faker->numberBetween(1, 5);
        $otherSlug = $this->faker->randomElement([ true , false ]);
        $data = [ 'name' => $name = $this->faker->sentence($nbWords, true) ];

        if (!$this->faker->randomElement([ true , false ]))
            $data['slug'] = $otherSlug ? $name : $this->faker->sentence($nbWords, true);
        else
            $data['slug'] = '';

        $this->browse(function (Browser $browser) use($taxonomy, $data) {
            $browser->visit(new IndexPage)
                    ->assertSee(__('Category Trees'))
                    ->with('.table .row-'.$taxonomy->id, function($row) use ($taxonomy) {
                        $row->clickLink(__('Edit'));
                    })
                    ->on(new EditPage($taxonomy))
                    ->assertSeeEdit()
                    ->editTaxonomy($data)
                    ->assertRouteIs(self::ROUTE_TO_INDEX_TAXONOMIES)
                    ->assertSeeIn('.alert', __(':name has been updated', ['name' => $data['name']]))
                    ->with('.table tbody', function($table) use ($data) {
                        $table->assertSee($data['name']);
                    });
        });
    }

    /** @test */
    public function deny_access_to_non_existent_taxonomy()
    {
        $n = rand(1, 5);
        factory(Taxonomy::class, $n)->create();
        $n++;
        $this->browse(function (Browser $browser)  use ($n){
            $browser->visit(route(self::ROUTE_TO_EDIT_TAXONOMIES, rand($n, $n+3)))
                    ->assertRouteIs(self::ROUTE_TO_INDEX_TAXONOMIES)
                    ->assertSeeIn('.alert', __('The taxonomy was not found'));
        });
    }

    /** @test */
    public function show_taxonomy()
    {   
        $taxonomy = factory(Taxonomy::class, 3)->create()->random();

        $this->browse(function (Browser $browser) use ($taxonomy){
            $browser->visit(new IndexPage)
                    ->assertSee(__('Category Trees'))
                    ->clickLink($taxonomy->name)
                    ->on(new ShowPage($taxonomy))
                    ->assertSeeShow();
        });
    }
}
