<?php

namespace Tests;

use Caffeinated\Shinobi\Models\Permission;
use Laravel\Dusk\TestCase as BaseTestCase;
use Facebook\WebDriver\Chrome\ChromeOptions;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Illuminate\Foundation\Testing\DatabaseMigrations;

abstract class DuskTestCase extends BaseTestCase
{
    use CreatesApplication, DatabaseMigrations;

    /**
     * Prepare for Dusk test execution.
     *
     * @beforeClass
     * @return void
     */
    public static function prepare()
    {
        static::startChromeDriver();
    }

    /**
     * Create the RemoteWebDriver instance.
     *
     * @return \Facebook\WebDriver\Remote\RemoteWebDriver
     */
    protected function driver()
    {
        $options = (new ChromeOptions)->addArguments([
            '--disable-gpu',
            '--headless',
            '--window-size=1290,1024',
            '--no-sandbox',
        ]);

        return RemoteWebDriver::create(
            'http://localhost:9515', DesiredCapabilities::chrome()->setCapability(
                ChromeOptions::CAPABILITY, $options
            )
        );
    }

    protected function seedPermissions()
    {
        $permissions = Permission::where('slug', 'not like', '%store%')
                        ->where('slug', 'not like', '%update%')
                        ->get();
        
        $result = collect([]);
        $i=0;
        do {
            $select_array = $permissions->random();

            if (!$result->firstWhere('slug', $select_array->slug)) {
                $result->push(collect($select_array->toArray()));
                $i++;
            }
                
        } while ($i < $this->faker->numberBetween(0, $permissions->count()));

        return $result;
    }

}
