<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Caffeinated\Shinobi\Models\Permission;
use Illuminate\Http\Request;
use Illuminate\Http\Response;


abstract class TestCase extends BaseTestCase
{
    use CreatesApplication, RefreshDatabase;

    /**
     * Perform a middleware check against a mocked response.
     * 
     * @param  string  $middleware
     * @param  array|string  $parameter
     * @return int
     */
    protected function middleware($middleware, $request, $parameter)
    {
        return $middleware->handle($request, function () {
            return (new Response())->setContent('<html></html>');
        }, $parameter);
    }

    protected function seedPermissions()
    {
        $permissions = Permission::where('slug', 'not like', '%store%')
                        ->where('slug', 'not like', '%update%')
                        ->get();
        
        $result = collect([]);
        $i=0;
        do {
            $select_array = $permissions->random();

            if (!$result->firstWhere('slug', $select_array->slug)) {
                $result->push(collect($select_array->toArray()));
                $i++;
            }
                
        } while ($i < $this->faker->numberBetween(0, $permissions->count()));

        return $result->keyBy('slug');
    }
}
