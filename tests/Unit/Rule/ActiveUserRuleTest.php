<?php

namespace Tests\Unit\Rule;

use App\User;
use App\Customer;
use Tests\TestCase;
use App\Rules\UserActiveRule;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Testing\WithFaker;

class ActiveUserRuleTest extends TestCase
{
    use WithFaker;

    const USER_PASSWORD = 'secret';

    protected $rule;

    public function setUp(): void
    {
        parent::setUp(); 

        $this->rule = new UserActiveRule();
    }

    /** @test */
    public function validate_if_the_user_is_active_admin()
    {
        $user = factory(User::class)->create([
            'password' => bcrypt(self::USER_PASSWORD),
            'is_active' => true
        ]);

        $this->assertTrue($this->rule->passes('email', $user->email));
    }

    /** @test */
    public function validate_if_the_user_is_active_customer()
    {
        $customer = factory(Customer::class)->create([
            'password' => bcrypt(self::USER_PASSWORD),
            'is_active' => true
        ]);

        $this->assertTrue($this->rule->passes('email', $customer->email));
    }

    /** @test */
    public function validate_if_the_user_is_not_active_customer()
    {
        $customer = factory(Customer::class)->create([
            'password' => bcrypt(self::USER_PASSWORD),
            'is_active' => false
        ]);

        $this->assertFalse($this->rule->passes('email', $customer->email));
    }

    /** @test */
    public function validate_if_the_user_is_not_active_admin()
    {
        $user = factory(User::class)->create([
            'password' => bcrypt(self::USER_PASSWORD),
            'is_active' => false
        ]);

        $this->assertFalse($this->rule->passes('email', $user->email));
    }

    /** @test */
    public function validate_if_the_user_is_active_con_email_not_found()
    {
        $user = factory(User::class)->create([
            'password' => bcrypt(self::USER_PASSWORD),
        ]);
        $email = $this->faker->unique()->safeEmail;

        $this->assertTrue($this->rule->passes('email', $email));
    }

}
