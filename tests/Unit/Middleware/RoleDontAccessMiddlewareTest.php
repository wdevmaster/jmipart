<?php

namespace Tests\Unit\Middleware;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use App\Http\Middleware\RoleDontAccessMiddleware;
use Caffeinated\Shinobi\Models\Role;
use Illuminate\Http\Request;
use App\User;

class RoleDontAccessMiddlewareTest extends TestCase
{
    use WithFaker;

    /** @test */
    public function a_user_with_the_customer_role_cannot_access_the_route()
    {
        $user   = factory(User::class)->create();
        $role   = factory(Role::class)->create([
            'name' => 'Customer',
            'slug' => 'customer',
        ]);
        $user->assignRoles($role);
        $this->actingAs($user);
        $request = Request::create('/admin', 'GET');

        $response = $this->middleware(new RoleDontAccessMiddleware, $request, 'customer');
        
        $this->assertEquals($response->status(), 302);
    }

    /** @test */
    public function a_user_with_the_role_other_than_customer_can_access_the_route()
    {
        $user   = factory(User::class)->create();
        $role   = factory(Role::class)->create([
            'name' => 'Admin',
            'slug' => 'admin',
        ]);
        $user->assignRoles($role);
        $this->actingAs($user);
        $request = Request::create('/admin', 'GET');

        $response = $this->middleware(new RoleDontAccessMiddleware, $request, 'customer');

        $this->assertEquals($response->status(), 200);
    }

}
