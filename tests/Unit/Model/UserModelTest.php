<?php

namespace Tests\Unit\Model;

use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;

class UserModelTest extends TestCase
{
    use WithFaker;

    /** @test */
    public function find_user_by_email()
    {
        factory(User::class, 3)
            ->create()
            ->each(function ($user) {
                $found_user = User::findByEmail($user->email);
                $this->assertTrue($found_user->email === $user->email);
            });
    }
}
