<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'WebController@welcome');

// Registration Routes
Route::get('/register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('/register', 'Auth\RegisterController@register')->name('auth.register');

// Authentication Routes
Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('/login', 'Auth\LoginController@login')->name('auth.login');
Route::post('/logout', 'Auth\LoginController@logout')->name('logout');

// Password Reset Routes
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('password.update');

Route::group([
    'as' => 'web.'
], function () {
    
    Route::group(['as' => 'product.'], function() {
        Route::get('/shop/products', 'WebController@indexProduct')->name('index');
        Route::get('{taxon}/c', 'WebController@indexProduct')->name('category');
        Route::get('{product}/p', 'WebController@showProduct')->name('show');
    });

    Route::group(['prefix' => 'cart', 'as' => 'cart.'], function() {
        Route::get('show', 'CartController@show')->name('show');
        Route::post('add/{product}', 'CartController@add')->name('add');
        Route::post('update/{cart_item}', 'CartController@update')->name('update');
        Route::post('remove/{cart_item}', 'CartController@remove')->name('remove');
    });
});

Route::group([
    'prefix' => 'admin',
    'middleware' => 'auth:admin'
], function () {

    //Route::redirect('/', '/dashboard', 301);
    Route::view('/dashboard', 'admin.dashboard');
    
    /* Route Roles  */
    Route::resource('role', 'RoleController')->except(['show', 'destroy']);
    Route::delete('role/delete', 'RoleController@delete')->name('role.delete');

    /* Route Users  */
    Route::resource('user', 'UserController')->except(['show', 'destroy']);
    Route::delete('user/delete', 'UserController@delete')->name('user.delete');

    /* Route Customers  */
    Route::resource('customer', 'CustomerController')->except(['destroy']);
    Route::delete('customer/delete', 'CustomerController@delete')->name('customer.delete');
    
    /* Route Address  */
    Route::resource('address', 'AddressController')->only(['create', 'store']);
    Route::delete('address/delete', 'AddressController@delete')->name('address.delete');

    /* Route Taxonomies  */
    Route::resource('taxonomy', 'TaxonomyController')->except(['destroy']);
    Route::delete('taxonomy/delete', 'TaxonomyController@delete')->name('taxonomy.delete');

    /* Route Taxons  */
    Route::group(['prefix' => 'taxonomy/{taxonomy}'], function () {
        Route::resource('taxon', 'TaxonController')->except(['index', 'show', 'destroy']);
        Route::delete('taxon/delete', 'TaxonController@delete')->name('taxon.delete');
    });

    /* Route Products  */
    Route::resource('product', 'ProductController')->except(['destroy']);
    Route::delete('product/delete', 'ProductController@delete')->name('product.delete');
    
});
