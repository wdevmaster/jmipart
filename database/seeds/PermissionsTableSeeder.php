<?php

use Illuminate\Database\Seeder;
use Caffeinated\Shinobi\Models\Permission;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rows = [
            /*------------------------------------------------------------------
            //                    ROLES
            -------------------------------------------------------------------*/

            [
                'name'  => 'Role Index',
                'slug'  => 'role.index',
            ],

            [
                'name'	=> 'Role Create',
                'slug'	=> 'role.create',
            ],

            [
                'name'	=> 'Role Store',
                'slug'	=> 'role.store',
            ],

            [
                'name'  => 'Role Edit',
                'slug'  => 'role.edit',
            ],

            [
                'name'	=> 'Role Update',
                'slug'	=> 'role.update',
            ],

            [
                'name'  => 'Role Delete',
                'slug'  => 'role.delete',
            ],

            /*------------------------------------------------------------------
            //                    USER
            -------------------------------------------------------------------*/

            [
                'name'  => 'User Index',
                'slug'  => 'user.index',
            ],

            [
                'name'	=> 'User Create',
                'slug'	=> 'user.create',
            ],

            [
                'name'	=> 'User Store',
                'slug'	=> 'user.store',
            ],

            [
                'name'  => 'User Edit',
                'slug'  => 'user.edit',
            ],

            [
                'name'	=> 'User Update',
                'slug'	=> 'user.update',
            ],

            [
                'name'  => 'User Delete',
                'slug'  => 'user.delete',
            ],

            /*------------------------------------------------------------------
            //                    CUSTOMER
            -------------------------------------------------------------------*/

            [
                'name'  => 'Customer Index',
                'slug'  => 'customer.index',
            ],

            [
                'name'	=> 'Customer Create',
                'slug'	=> 'customer.create',
            ],

            [
                'name'	=> 'Customer Store',
                'slug'	=> 'customer.store',
            ],

            [
                'name'  => 'Customer Show',
                'slug'  => 'customer.show',
            ],

            [
                'name'  => 'Customer Edit',
                'slug'  => 'customer.edit',
            ],

            [
                'name'	=> 'Customer Update',
                'slug'	=> 'customer.update',
            ],

            [
                'name'  => 'Customer Delete',
                'slug'  => 'customer.delete',
            ],

        ];

        foreach ($rows as $row) {
            Permission::create($row);
        }
    }
}