<?php

use App\User;
use Illuminate\Database\Seeder;
use Caffeinated\Shinobi\Models\Role;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $password = 'secret';
        $roles = Role::all();

        $rows = [
            [
                'name'  => 'Admin',
                'email' => 'admin@email.com',
                'role'  => 'admin',
                'is_active' => true
            ],
        ];

        foreach ($rows as $row) {
            if (isset($row['role']) && $roles->where('slug', $row['role'])->first()) {
                $user = User::create([
                    'name' => $row['name'],
                    'email' => $row['email'],
                    'password' => bcrypt($password),
                    'is_active' => $row['is_active']
                ]);

                $user->assignRoles($row['role']);
            }
        }
    }
}