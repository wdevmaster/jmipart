<?php

use Illuminate\Database\Seeder;
use Caffeinated\Shinobi\Models\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            [
                'name' => 'Admin',
                'slug' => 'admin',
                'description' => 'System administrator',
                'special' => 'all-access',
            ],
        ];

        foreach ($roles as $row) {
            $role = Role::create([
                'name' => $row['name'],
                'slug' => $row['slug'],
                'description' => $row['description'],
                'special' => $row['special'],
            ]);   
        }
    }
}