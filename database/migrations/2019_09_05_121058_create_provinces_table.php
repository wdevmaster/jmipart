<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\ProvinceType;

class CreateProvincesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('provinces', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->char('country_id', 2);
            $table->integer('parent_id')->unsigned()->nullable();
            $table->enum('type', ProvinceType::values())->default(ProvinceType::defaultValue());
            $table->string('code', 16)->nullable()->comment('National identification code');
            $table->string('name');

            $table->index('code', 'country_id');

            $table->foreign('country_id')->references('id')->on('countries');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('provinces');
    }
}
