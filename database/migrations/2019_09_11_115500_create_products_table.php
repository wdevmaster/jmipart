<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\ProductAvailability;
use App\ProductCondition;
use App\ProductState;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('slug')->nullable();
            $table->string('sku');
            $table->decimal('price', 15, 4)->nullable();
            $table->text('excerpt')->nullable();
            $table->text('description')->nullable();
            $table->enum('state', ProductState::values())->default(ProductState::defaultValue());
            $table->enum('availability', ProductAvailability::values())->default(ProductAvailability::defaultValue());
            $table->enum('condition', ProductCondition::values())->default(ProductCondition::defaultValue());
            $table->integer('stock')->default(0);
            $table->timestamp('last_sale_at')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
