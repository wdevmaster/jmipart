<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Country;
use Faker\Generator as Faker;

$factory->define(Country::class, function (Faker $faker) {
    return [
        'id'           => $faker->lexify('??'),
        'name'         => $faker->country,
        'phonecode'    => $faker->numberBetween(1, 100),
        'is_eu_member' => 0
    ];
});
