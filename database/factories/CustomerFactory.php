<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Customer;
use Illuminate\Support\Str;
use Faker\Generator as Faker;
use App\CustomerType;

$factory->define(Customer::class, function (Faker $faker) {
    $model = [
        'type'      => $type = $faker->randomElement(CustomerType::toArray()),
        'firstname' => $faker->firstName('male'|'female'), 
        'lastname' => $faker->lastName, 
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        'remember_token' => Str::random(10),
    ];

    if ($type == 'organization')
        $model = array_merge($model, [
            'company_name'      => $faker->company,
            'tax_nr'            => $faker->ein,
            'registration_nr'   => $faker->ein,
        ]);

    return $model;
});
