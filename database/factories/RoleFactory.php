<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use Caffeinated\Shinobi\Models\Role;

$factory->define(Role::class, function (Faker $faker) {
    return [
        'name' => $name = $faker->jobTitle,
        'slug' => str_slug($name),
    ];
});

