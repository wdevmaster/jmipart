<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Taxon;
use App\Taxonomy;
use Faker\Generator as Faker;

$factory->define(Taxon::class, function (Faker $faker) {
    return [
        'name'        => $faker->unique()->word,
        'taxonomy_id' => function () {
            return factory(Taxonomy::class)->create()->id;
        },
    ];
});
