<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Address;
use App\AddressType;
use Faker\Generator as Faker;

$factory->define(Address::class, function (Faker $faker) {
    return [
        'type' => $faker->randomElement(AddressType::toArray()),
        'name' => $faker->sentence(2, true),
        'country_id' => $countryId = function () {
            return factory(App\Country::class)->create()->id;
        },
        'province_id' => function () use ($countryId){
            return factory(App\Province::class)->create(['country_id' => $countryId])->id;
        },
        'postalcode' => $faker->postcode,
        'city' => $faker->city,
        'address' => $faker->streetAddress,
        'is_default' => $this->faker->randomElement([true, false]),
    ];
});
