<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Country;
use App\Province;
use App\ProvinceType;
use Faker\Generator as Faker;

$factory->define(Province::class, function (Faker $faker) {
    return [
        'country_id' => function ()
        {
            return factory(App\Country::class)->create()->id;
        },
        'type' => $faker->randomElement(ProvinceType::toArray()),
        'code' => $faker->stateAbbr, 
        'name' => $faker->state,
    ];
});
