<?php

return [

    'default_fit' => 'crop',


    'variants' => [
        'thumbnail' => [
            'width'  => 150,
            'height' => 150,
        ], 
    
        'medium' => [
            'width'  => 300,
            'height' => 300,
        ],

        'large' => [
            'width'  => 1024,
            'height' => 1024,
        ],

    ], 

];