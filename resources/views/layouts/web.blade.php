<!doctype html>
<html class="no-js" lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }} - @yield('title')</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('assets/img/favicon.ico')}}">
    
    @section('styles')
    <!-- CSS 
    ========================= -->
    <!-- Plugins CSS -->
    <link rel="stylesheet" href="{{asset('assets/css/plugins.css')}}">
    <!-- Main Style CSS -->
    <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
    @show

</head>

<body>

    <!-- Main Wrapper Start -->
    <!--header area start-->
    <header class="header_area">
        <!--header top start-->
        <div class="header_top">
            <div class="container">  
                <div class="top_inner">
                    <div class="row align-items-center">
                    <div class="col-lg-6 col-md-6">
                        <div class="follow_us">
                            <label>Follow Us:</label>
                            <ul class="follow_link">
                                <li><a href="https://www.facebook.com/JMInterpartCo/?ref=br_rs"><i class="ion-social-facebook"></i></a></li>
                                <li><a href="https://twitter.com/jminterpartco?lang=es"><i class="ion-social-twitter"></i></a></li>
                                <li><a href="https://www.instagram.com/jminterpart/"><i class="ion-social-instagram"></i></a></li>
                               <!-- <li><a href="#"><i class="ion-social-youtube"></i></a></li>-->
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <div class="top_right text-right">
                            <ul>
                                @guest
                                <li class="top_links">
                                    <a href="{{ route('login') }}">{{ __('Login') }}</a>
                                </li> 
                                <li class="top_links">
                                    <a href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li> 
                                @else
                                    <li class="top_links">
                                        <a href="#"><i class="ion-android-person"></i> My Account<i class="ion-ios-arrow-down"></i></a>
                                        <ul class="dropdown_links">
                                            <li><a href="checkout.html">Checkout </a></li>
                                            <li><a href="my-account.html">My Account </a></li>
                                            <li><a href="cart.html">Shopping Cart</a></li>
                                            <!--<li><a href="wishlist.html">Wishlist</a></li>-->
                                        </ul>
                                    </li> 
                                @endif
                                <!--
                               <li class="language"><a href="#"><img src="assets/img/logo/language.png" alt="">en-gb<i class="ion-ios-arrow-down"></i></a>
                                    <ul class="dropdown_language">
                                        <li><a href="#"><img src="assets/img/logo/language.png" alt=""> English</a></li>
                                        <li><a href="#"><img src="assets/img/logo/language2.png" alt=""> Germany</a></li>
                                    </ul>
                                </li>
                                -->
                                <li class="currency">
                                    <a href="#">+1(305)593.1007<i class="ion-ios-phone-portrait"></i></a>
                                </li> 
                            </ul>
                        </div>  
                    </div>
                </div>
                </div>
            </div>
        </div>
        <!--header top start-->
        <!--header middel start-->
        <div class="header_middle">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-3 col-md-6">
                        <div class="logo">
                            <a href="{{url('/')}}"><img src="assets/logos/logo1.png" alt=""></a>
                        </div>
                    </div>
                    <div class="col-lg-9 col-md-6">
                        <div class="middel_right">
                            <div class="search-container">
                               <form action="{{route('web.product.index')}}">
                                    <div class="search_box">
                                        <input name="search" placeholder="Enter your part o search key" type="text">
                                        <button type="submit"><i class="ion-ios-search-strong"></i></button> 
                                    </div>
                                </form>
                            </div>
                            <div class="middel_right_info">
                                
                                <div class="header_wishlist">
                                    <!--
                                    <a href="wishlist.html"><span class="lnr lnr-heart"></span> Wish list </a>
                                    <span class="wishlist_quantity">0</span>
                                    -->
                                </div>
                                @include('elements._cart_wrapper')
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--header middel end-->
        <!--header bottom satrt-->
        <div class="header_bottom sticky-header">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-12">
                        <div class="main_menu header_position"> 
                            <nav>  
                                <ul>

                                    <li>
                                        <a href="{{url('/')}}">home</a>
                                    </li>
                                    <li class="mega_items">
                                        <a href="{{route('web.product.index')}}">shop</a> 
                                    </li>
                                    <li>
                                        <a href="blog.html">blog</a>
                                    </li>
                                    <li>
                                        <a href="portfolio.html">pages</a>
                                    </li>

                                    <li><a href="about.html">about Us</a></li>
                                    <li><a href="contact.html"> Contact Us</a></li>
                                </ul>  
                            </nav> 
                        </div>
                    </div>
                   
                </div>
            </div>
        </div>
        <!--header bottom end-->
     
    </header>
    <!--header area end-->

    @yield('content')

    <!--footer area start-->
    <footer class="footer_widgets">
        <div class="container">  
            <div class="footer_top">
                <div class="row">
                    <div class="col-lg-4 col-md-6">
                        <div class="widgets_container contact_us">
                            <div class="footer_logo">
                                <a href="#"><img src="assets/logos/logo2.jpg" alt=""></a>
                            </div>
                            <div class="footer_contact">
                                <p>The team at JM INTERPART CORPORATION has over 40 years of experience with replacement auto parts and accessories. JM Interpart Corp. is an established wholesaler for electronic and engine parts for the Latin American Market and have now expanded our reach to the USA.</p>
                                <p><span>Address</span> 8242 NW 70th St. Miami FL 33166 USA</p>
                                <p><span>Need Help?</span>Call: +1(305)593.10.07</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-6">
                        <div class="widgets_container widget_menu">
                            <h3>Information</h3>
                            <div class="footer_menu">
                                <ul>
                                    <li><a href="shop.html">Shop</a></li>
                                    <li><a href="blog.html">Blog</a></li>
                                    <li><a href="about.html">About Us</a></li>
                                    <li><a href="portfolio.html">Terms & Conditions</a></li>
                                    <li><a href="contact.html">Contact Us</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-6">
                        <div class="widgets_container widget_menu">
                            <h3>Categories</h3>
                            <div class="footer_menu">
                                <ul>
                                    <li><a href="#">Heating & Cooling</a></li>
                                    <li><a href="#">Fuel System</a></li>
                                    <li><a href="wishlist.html">Ignition System</a></li>
                                    <li><a href="#">Wheel Hubs & Bearings</a></li>
                                    <li><a href="#">Suspension</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                       <div class="widgets_container">
                            <h3>Join us to get our newsletter</h3>
                            <!--<p>We’ll never share your email address with a third-party.</p>-->
                            <div class="subscribe_form">
                                <form>
                                    <input placeholder="Enter you email address here..." type="text">
                                    <button type="submit">Subscribe</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer_bottom">
               <div class="row">
                    <div class="col-lg-6 col-md-6">
                        <div class="copyright_area">
                            <p>Copyright &copy; 2019 <a href="#">JM Interpart Corporation</a>  All Right Reserved.</p>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <div class="footer_payment text-right">
                            <a href="#"><img src="assets/img/icon/payment.png" alt=""></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>     
    </footer>
    <!--footer area end-->

    @section('scripts')
    <!-- JS
    ============================================ -->
    <!-- Plugins JS -->
    <script src="{{asset('assets/js/plugins.js')}}"></script>
    <!-- Main JS -->
    <script src="{{asset('assets/js/main.js')}}"></script>
    @show
</body>
</html>