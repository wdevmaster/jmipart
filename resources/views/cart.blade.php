@extends('layouts.web')

@section('title')
    {{ __('Show cart') }}
@stop

@section('content')
    
    <!--shopping cart area start -->
    <div class="shopping_cart_area mt-32">
        <div class="container">  
            <div class="row">
                <div class="col-12">
                    <div class="table_desc">
                        <div class="cart_page table-responsive">
                            <table>
                                <thead>
                                    <tr>
                                        <th class="product_remove">Delete</th>
                                        <th class="product_thumb">Image</th>
                                        <th class="product_name">Product</th>
                                        <th class="product-price">Price</th>
                                        <th class="product_quantity">Quantity</th>
                                        <th class="product_total">Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse (Cart::getItems() as $item)
                                        <tr>
                                            <td class="product_remove">
                                                <form action="{{ route('web.cart.remove', $item) }}"
                                                    style="display: inline-block" method="POST">
                                                    @csrf
                                                    <button dusk="cart-delete-{{ $item->getProduct()->id }}" class="btn btn-link btn-sm"><i class="fa fa-trash-o"></i></button>
                                                </form>
                                            </td>
                                            <td class="product_thumb"><a href="#"><img src="{{$item->getThumbnailUrl()}}" alt=""></a></td>
                                            <td class="product_name"><a href="#">{{$item->name}}</a></td>
                                            <td class="product-price"> ${{$item->price}}</td>
                                            <td class="product_quantity">
                                                <label>Quantity</label> 
                                                <form id="update-item-{{ $item->getProduct()->id }}" action="{{ route('web.cart.update', $item) }}" 
                                                    style="display: inline-block" method="POST">
                                                    @csrf
                                                    <input data-id="{{ $item->getProduct()->id }}" name="qty" value="{{ $item->quantity }}" type="number" min="0" max="100" />
                                                </form>
                                            </td>
                                            <td class="product_total"> ${{$item->getTotalFormat()}}</td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="6">
                                                <div class="cart_empty">
                                                    {{__('Cart Empty')}}
                                                </div>
                                            </td>
                                        </tr>
                                    @endforelse
                                </tbody>
                            </table>   
                        </div>    
                    </div>
                 </div>
             </div>
             <!--coupon code area start-->
            <div class="coupon_area">
                <div class="row">
                    <div class="col-lg-6 col-md-6">
                        <!--
                        <div class="coupon_code left">
                                <h3>Coupon</h3>
                                <div class="coupon_inner">   
                                    <p>Enter your coupon code if you have one.</p>                                
                                    <input placeholder="Coupon code" type="text">
                                    <button type="submit">Apply coupon</button>
                                </div>    
                        </div>
                        -->
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <div class="coupon_code right">
                            <h3>Cart Totals</h3>
                            <div class="coupon_inner">
                                <div class="cart_subtotal">
                                    <p>Subtotal</p>
                                    <p class="cart_amount"> ${{Cart::getSubtotal()}}</p>
                                </div>
                                <!--
                                <div class="cart_subtotal ">
                                    <p>Shipping</p>
                                    <p class="cart_amount"><span>Flat Rate:</span>  $255.00</p>
                                </div>
                                <a href="#">Calculate shipping</a>
                                -->
                                <hr>
                                <div class="cart_subtotal">
                                    <p>Total</p>
                                    <p class="cart_amount"> ${{Cart::getTotal()}}</p>
                                </div>
                                <div class="checkout_btn">
                                    <a href="#">Proceed to Checkout</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--coupon code area end-->
        </div>     
    </div>
    <!--shopping cart area end -->

    @include('elements._call_action')
@endsection

@section('scripts')
    @parent

    <script>
        $(document).ready(function(){
            $('input[name=qty]').on('input', function() {
                let id = $(this).data('id')
                document.getElementById('update-item-'+id).submit();
            })
        })
    </script>
@stop