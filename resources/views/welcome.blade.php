@extends('layouts.web')

@section('content')
    <!--Offcanvas menu area start-->
    <div class="Offcanvas_menu">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="canvas_open">
                            <span>MENU</span>
                            <a href="#"><i class="ion-navicon"></i></a>
                        </div>
                        <div class="Offcanvas_menu_wrapper">

                            <div class="canvas_close">
                                  <a href="#"><i class="ion-android-close"></i></a>  
                            </div>
                            <div class="top_right text-right">
                                <ul>
                                   <li class="top_links"><a href="#"><i class="ion-android-person"></i> My Account<i class="ion-ios-arrow-down"></i></a>
                                        <ul class="dropdown_links">
                                            <li><a href="checkout.html">Checkout </a></li>
                                            <li><a href="my-account.html">My Account </a></li>
                                            <li><a href="cart.html">Shopping Cart</a></li>
                                            <li><a href="wishlist.html">Wishlist</a></li>
                                        </ul>
                                    </li> 
                                   <li class="language"><a href="#"><img src="assets/img/logo/language.png" alt="">en-gb<i class="ion-ios-arrow-down"></i></a>
                                        <ul class="dropdown_language">
                                            <li><a href="#"><img src="assets/img/logo/language.png" alt=""> English</a></li>
                                            <li><a href="#"><img src="assets/img/logo/language2.png" alt=""> Germany</a></li>
                                        </ul>
                                    </li>
                                    <li class="currency"><a href="#"></a>
                                       <!-- <ul class="dropdown_currency">
                                            <li><a href="#">EUR – Euro</a></li>
                                            <li><a href="#">GBP – British Pound</a></li>
                                            <li><a href="#">INR – India Rupee</a></li>
                                        </ul>-->
                                    </li>
                                </ul>
                                <center><br><a href="">+1(305)593.1007</a></center>
                            </div>
                            <div class="Offcanvas_follow">
                                <label>Follow Us:</label>
                                <ul class="follow_link">
                                    <li><a href="https://www.facebook.com/JMInterpartCo/?ref=br_rs"><i class="ion-social-facebook"></i></a></li>
                                    <li><a href="https://twitter.com/jminterpartco?lang=es"><i class="ion-social-twitter"></i></a></li>
                                    <li><a href="https://www.instagram.com/jminterpart/"><i class="ion-social-instagram"></i></a></li>
                                    <!--<li><a href="#"><i class="ion-social-youtube"></i></a></li>-->
                                </ul>
                            </div>
                            
                            <div class="search-container">
                               <form action="#">
                                    <div class="search_box">
                                        <input placeholder="Search entire store here ..." type="text">
                                        <button type="submit"><i class="ion-ios-search-strong"></i></button> 
                                    </div>
                                </form>
                            </div>
                            <div id="menu" class="text-left ">
                                 <ul>

                                    <li><a href="index.html">home</a>
                                        <!--<ul class="sub_menu">
                                            <li><a href="index.html">Home 1</a></li>
                                            <li><a href="index-2.html">Home 2</a></li>
                                            <li><a href="index-3.html">Home 3</a></li>
                                            <li><a href="index-4.html">Home 4</a></li>
                                        </ul>-->
                                    </li>
                                    <li><a href="shop.html">shop</a> 
                                       <!-- <ul>
                                            <li><a href="#">Shop Layouts</a>
                                                <ul>
                                                    <li><a href="shop-fullwidth.html">Full Width</a></li>
                                                    <li><a href="shop-fullwidth-list.html">Full Width list</a></li>
                                                    <li><a href="shop-right-sidebar.html">Right Sidebar </a></li>
                                                    <li><a href="shop-right-sidebar-list.html"> Right Sidebar list</a></li>
                                                    <li><a href="shop-list.html">List View</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="#">other Pages</a>
                                                <ul>
                                                    <li><a href="cart.html">cart</a></li>
                                                    <li><a href="checkout.html">Checkout</a></li>
                                                    <li><a href="my-account.html">my account</a></li>


                                                </ul>
                                            </li>
                                            <li><a href="#">Product Types</a>
                                                <ul>
                                                    <li><a href="product-sidebar.html">product details</a></li>
                                                    <li><a href="product-sidebar.html">product sidebar</a></li>
                                                    <li><a href="product-grouped.html">product grouped</a></li>
                                                    <li><a href="variable-product.html">product variable</a></li>

                                                </ul>
                                            </li>
                                        </ul>-->
                                    </li>
                                    <li><a href="blog.html">blog</a>
                                       <!-- <ul>
                                            <li><a href="blog-details.html">blog details</a></li>
                                            <li><a href="blog-fullwidth.html">blog fullwidth</a></li>
                                            <li><a href="blog-sidebar.html">blog sidebar</a></li>
                                        </ul>-->
                                    </li>
                                    <li><a href="portfolio.html">pages</a>
                                        <!--<ul>
                                            <li><a href="about.html">About Us</a></li>
                                            <li><a href="services.html">services</a></li>
                                            <li><a href="faq.html">Frequently Questions</a></li>
                                            <li><a href="contact.html">contact</a></li>
                                            <li><a href="login.html">login</a></li>
                                            <li><a href="wishlist.html">Wishlist</a></li>
                                            <li><a href="404.html">Error 404</a></li>
                                        </ul>-->
                                    </li>

                                    <li><a href="about.html">about Us</a></li>
                                    <li><a href="contact.html"> Contact Us</a></li>
                                </ul> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    <!--Offcanvas menu area end-->  <!--Fin header-->


<!--shipping area start-->
    <section class="shipping_area mb-50">
        <div class="container">
            <div class="checkout_form">
            <div class=" row">
               <div class="col-12">
                   <div class="shipping_inner">
                    <div class="section_title">
                        <h2><span>Select Your</span>Vehicle</h2>
                    </div>
                        <div class="single_shipping" style="padding-left:10px;padding-right: 10px;">
                            <select class="niceselect_option" name="cuntry" id="country"> 
                                        <option value="2">Year</option> 
                                        <option value="2">bangladesh</option>      
                                        <option value="3">Algeria</option> 
                                        <option value="4">Afghanistan</option>    
                                        <option value="5">Ghana</option>    
                                        <option value="6">Albania</option>    
                                        <option value="7">Bahrain</option>    
                                        <option value="8">Colombia</option>    
                                        <option value="9">Dominican Republic</option>   

                                    </select> 
                        </div>
                        <div class="single_shipping" style="padding-left:10px;padding-right: 10px;">
                            <select class="niceselect_option" name="cuntry" id="country"> 
                                        <option value="2">Make</option> 
                                        <option value="2">bangladesh</option>      
                                        <option value="3">Algeria</option> 
                                        <option value="4">Afghanistan</option>    
                                        <option value="5">Ghana</option>    
                                        <option value="6">Albania</option>    
                                        <option value="7">Bahrain</option>    
                                        <option value="8">Colombia</option>    
                                        <option value="9">Dominican Republic</option>   

                                    </select> 
                        </div>
                       <div class="single_shipping" style="padding-left:10px;padding-right: 10px;">
                            <select class="niceselect_option" name="cuntry" id="country"> 
                                        <option value="2">Model</option> 
                                        <option value="2">bangladesh</option>      
                                        <option value="3">Algeria</option> 
                                        <option value="4">Afghanistan</option>    
                                        <option value="5">Ghana</option>    
                                        <option value="6">Albania</option>    
                                        <option value="7">Bahrain</option>    
                                        <option value="8">Colombia</option>    
                                        <option value="9">Dominican Republic</option>   

                                    </select> 
                        </div>
                        <div class="single_shipping">
                                <div class="order_button">
                                    <button class="button"  type="submit">Search</button> 
                                </div> 
                        </div>
                    </div>
               </div>
            </div></div>
        </div>
    </section>
    <!--shipping area end-->
    <!--slider area start-->
    <section class="slider_section mb-50">
        <div class="container">
            <div class="row">
               
                <div class="col-lg-3 col-md-12">
                    <div class="categories_menu">
                        <div class="categories_title">
                            <h2 class="categori_toggle">Browse categories</h2>
                        </div>
                        <div class="categories_menu_toggle">
                            <ul>
                                @foreach ($categories as $taxon)
                                    <li>
                                        <a href="{{route('web.product.category', $taxon->slug)}}"> 
                                            {{$taxon->name}}
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                            
                            <!--
                            <ul>
                                <li class="menu_item_children categorie_list"><a href="#">Brake Parts <i class="fa fa-angle-right"></i></a>
                                    <ul class="categories_mega_menu">
                                        <li class="menu_item_children"><a href="#">Wheel Bearings</a>
                                            <ul class="categorie_sub_menu">
                                                <li><a href="">Bower</a></li>
                                                <li><a href="">Flipbac</a></li>
                                                <li><a href="">Gary Fong</a></li>
                                                <li><a href="">GigaPan</a></li>
                                            </ul>
                                        </li>
                                        <li class="menu_item_children"><a href="#">Wheel Rim Screws</a>
                                            <ul class="categorie_sub_menu">
                                                <li><a href="">Accessories</a></li>
                                                <li><a href="">2-Stroke</a></li>
                                                <li><a href="">Handbag</a></li>
                                                <li><a href="">Clothing</a></li>
                                            </ul>
                                        </li>
                                        <li class="menu_item_children last_child"><a href="#">Wheel Simulators</a>
                                                <ul class="categorie_sub_menu">
                                                    <li><a href="">Bags & Cases</a></li>
                                                    <li><a href="">Binoculars & Scopes</a></li>
                                                    <li><a href="">Film Photography</a></li>
                                                    <li><a href="">Lighting & Studio</a></li>
                                                </ul>
                                                <div class="categorie_banner">
                                                    <a href="#"><img src="assets/img/bg/banner2.jpg" alt=""></a>
                                                </div>
                                        </li>

                                    </ul>
                                </li>
                                <li class="menu_item_children"><a href="#"> Wheels & Tires  <i class="fa fa-angle-right"></i></a>
                                    <ul class="categories_mega_menu">
                                        <li class="menu_item_children"><a href="#">Dresses</a>
                                            <div class="categorie_sub_menu">
                                                <ul>
                                                    <li><a href="">Sweater</a></li>
                                                    <li><a href="">Evening</a></li>
                                                    <li><a href="">Day</a></li>
                                                    <li><a href="">Sports</a></li>
                                                </ul>
                                            </div>
                                        </li>
                                        <li class="menu_item_children"><a href="#">Handbags</a>
                                            <div class="categorie_sub_menu">
                                                <ul>
                                                    <li><a href="">Shoulder</a></li>
                                                    <li><a href="">Satchels</a></li>
                                                    <li><a href="">kids</a></li>
                                                    <li><a href="">coats</a></li>
                                                </ul>
                                            </div>
                                        </li>
                                        <li class="menu_item_children"><a href="#">shoes</a>
                                            <div class="categorie_sub_menu">
                                                <ul>
                                                    <li><a href="">Ankle Boots</a></li>
                                                    <li><a href="">Clog sandals </a></li>
                                                    <li><a href="">run</a></li>
                                                    <li><a href="">Books</a></li>
                                                </ul>
                                            </div>
                                        </li>
                                        <li class="menu_item_children"><a href="#">Clothing</a>
                                            <div class="categorie_sub_menu">
                                                <ul>
                                                    <li><a href="">Coats  Jackets </a></li>
                                                    <li><a href="">Raincoats</a></li>
                                                    <li><a href="">Jackets</a></li>
                                                    <li><a href="">T-shirts</a></li>
                                                </ul>
                                            </div>
                                        </li>

                                    </ul>
                                </li>
                                <li class="menu_item_children"><a href="#"> Furnitured & Decor <i class="fa fa-angle-right"></i></a>
                                    <ul class="categories_mega_menu column_3">
                                        <li class="menu_item_children"><a href="#">Chair</a>
                                            <div class="categorie_sub_menu">
                                                <ul>
                                                    <li><a href="">Dining room</a></li>
                                                    <li><a href="">bedroom</a></li>
                                                    <li><a href=""> Home & Office</a></li>
                                                    <li><a href="">living room</a></li>
                                                </ul>
                                            </div>
                                        </li>
                                        <li class="menu_item_children"><a href="#">Lighting</a>
                                            <div class="categorie_sub_menu">
                                                <ul>
                                                    <li><a href="">Ceiling Lighting</a></li>
                                                    <li><a href="">Wall Lighting</a></li>
                                                    <li><a href="">Outdoor Lighting</a></li>
                                                    <li><a href="">Smart Lighting</a></li>
                                                </ul>
                                            </div>
                                        </li>
                                        <li class="menu_item_children"><a href="#">Sofa</a>
                                            <div class="categorie_sub_menu">
                                                <ul>
                                                    <li><a href="">Fabric Sofas</a></li>
                                                    <li><a href="">Leather Sofas</a></li>
                                                    <li><a href="">Corner Sofas</a></li>
                                                    <li><a href="">Sofa Beds</a></li>
                                                </ul>
                                            </div>
                                        </li>
                                    </ul>
                                </li>
                                <li class="menu_item_children"><a href="#"> Turbo System <i class="fa fa-angle-right"></i></a>
                                    <ul class="categories_mega_menu column_2">
                                        <li class="menu_item_children"><a href="#">Brake Tools</a>
                                            <div class="categorie_sub_menu">
                                                <ul>
                                                    <li><a href="">Driveshafts</a></li>
                                                    <li><a href="">Spools</a></li>
                                                    <li><a href="">Diesel </a></li>
                                                    <li><a href="">Gasoline</a></li>
                                                </ul>
                                            </div>
                                        </li>
                                        <li class="menu_item_children"><a href="#">Emergency Brake</a>
                                            <div class="categorie_sub_menu">
                                                <ul>
                                                    <li><a href="">Dolls for Girls</a></li>
                                                    <li><a href="">Girls' Learning Toys</a></li>
                                                    <li><a href="">Arts and Crafts for Girls</a></li>
                                                    <li><a href="">Video Games for Girls</a></li>
                                                </ul>
                                            </div>
                                        </li>

                                    </ul>
                                </li>
                                <li class="menu_item_children"><a href="#"> Lighting <i class="fa fa-angle-right"></i></a>
                                    <ul class="categories_mega_menu column_2">
                                        <li class="menu_item_children"><a href="#">Check Trousers</a>
                                            <div class="categorie_sub_menu">
                                                <ul>
                                                    <li><a href="">Building</a></li>
                                                    <li><a href="">Electronics</a></li>
                                                    <li><a href="">action figures </a></li>
                                                    <li><a href="">specialty & boutique toy</a></li>
                                                </ul>
                                            </div>
                                        </li>
                                        <li class="menu_item_children"><a href="#">Calculators</a>
                                            <div class="categorie_sub_menu">
                                                <ul>
                                                    <li><a href="">Dolls for Girls</a></li>
                                                    <li><a href="">Girls' Learning Toys</a></li>
                                                    <li><a href="">Arts and Crafts for Girls</a></li>
                                                    <li><a href="">Video Games for Girls</a></li>
                                                </ul>
                                            </div>
                                        </li>

                                    </ul>
                                </li>
                                <li><a href="#"> Accessories</a></li>
                                <li><a href="#">Body Parts</a></li>
                                <li><a href="#">Perfomance Filters</a></li>
                                <li><a href="#"> Engine Parts</a></li>
                                <li id="cat_toggle" class="has-sub"><a href="#"> More Categories</a>
                                    <ul class="categorie_sub">
                                        <li><a href="#">Hide Categories</a></li>
                                    </ul>   

                                </li>
                            </ul>
                        -->
                        </div>
                    </div>
                </div>
                <div class="col-lg-9 col-md-12">
                    <div class="slider_area owl-carousel">
                        <div class="single_slider d-flex align-items-center" data-bgimg="assets/img/slider/slider1.png">
                            <div class="slider_content">
                                <h2>Top Quality</h2>
                                <h1>Aftermarket Turbocharger Specialist</h1>
                                <a class="button" href="#">shopping now</a>
                            </div>

                        </div>
                        <div class="single_slider d-flex align-items-center" data-bgimg="assets/img/slider/slider2.png">
                            <div class="slider_content">
                                <h2>Height - Quality</h2>
                                <h1>The Parts Of shock Absorbers & Brake Kit</h1>
                                <a class="button" href="#">shopping now</a>
                            </div>
                        </div>
                        <div class="single_slider d-flex align-items-center" data-bgimg="assets/img/slider/slider3.png">
                            <div class="slider_content">
                                <h2>Engine Oils</h2>
                                <h1>Top Quality Oil For Every Vehicle</h1>
                                <a class="button" href="#">shopping now</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
         
    </section>

    <!--slider area end-->

    <!--shipping area start-->
    <section class="shipping_area mb-50">
        <div class="container">
            <div class=" row">
               <div class="col-12">
                   <div class="shipping_inner">
                        <div class="single_shipping">
                            <div class="shipping_icone">
                                <img src="assets/images/icon4.png" alt="">
                            </div>
                            <div class="shipping_content">
                                <h2>Free Shipping</h2>
                                <p>Free shipping on all US order</p>
                            </div>
                        </div>
                        <div class="single_shipping">
                            <div class="shipping_icone">
                                <img src="assets/images/icon3.png" alt="">
                            </div>
                            <div class="shipping_content">
                                <h2>Support 24/7</h2>
                                <p>Contact us 24 hours a day</p>
                            </div>
                        </div>
                        <div class="single_shipping">
                            <div class="shipping_icone">
                                <img src="assets/images/icon2.png" alt="">
                            </div>
                            <div class="shipping_content">
                                <h2>100% Money Back</h2>
                                <p>You have 30 days to Return</p>
                            </div>
                        </div>
                        <div class="single_shipping">
                            <div class="shipping_icone">
                                <img src="assets/images/icon1.png" alt="">
                            </div>
                            <div class="shipping_content">
                                <h2>Payment Secure</h2>
                                <p>We ensure secure payment</p>
                            </div>
                        </div>
                    </div>
               </div>
            </div>
        </div>
    </section>
    <!--shipping area end-->
    <!--product area start-->
    <section class="product_area mb-50">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section_title">
                        <h2><span>New</span>Arrivals</h2>
                        <ul class="product_tab_button nav" role="tablist">
                            @foreach ($newProducts as $item)
                            <li>
                                <a data-toggle="tab" href="#{{$item->slug}}" role="tab" aria-controls="brake" aria-selected="true">{{$item->name}}</a>
                            </li>
                            @endforeach
                        </ul>
                    </div>

                </div>
            </div> 
            
            <div class="tab-content">
                @foreach ($newProducts as $i => $item)
                    <div class="tab-pane fade {{ $i < 1 ? 'show active': ''}}" id="{{$item->slug}}" role="tabpanel">
                        <div class="product_carousel product_column5 owl-carousel">
                            @foreach ($item->products as $key =>  $product)
                                <div class="single_product">
                                    <div class="product_name">
                                        <h3><a href="{{route('web.product.show', $product->slug)}}">{{$product->name}}</a></h3>
                                        <p class="manufacture_product"><a href="{{route('web.product.category', $item->slug)}}">IGNITION SYSTEM</a></p>
                                    </div>
                                    <div class="product_thumb">
                                        <a class="primary_img" href="{{route('web.product.show', $product->slug)}}"><img src="{{$product->getThumbnailUrl()}}" alt=""></a>
                                        
                                        <!--
                                        <div class="label_product">
                                            <span class="label_sale">-57%</span>
                                        </div>

                                        <div class="action_links">
                                            <ul>
                                                <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box"  title="quick view"> <span class="lnr lnr-magnifier"></span></a></li>
                                                <!--<li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><span class="lnr lnr-heart"></span></a></li>
                                                <li class="compare"><a href="#" title="compare"><span class="lnr lnr-sync"></span></a></li>-
                                            </ul>
                                        </div>
                                        -->
                                    </div>
                                    <div class="product_content">
                                        <div class="product_ratings">
                                            <ul>
                                                <li><a href="#"><i class="ion-star"></i></a></li>
                                                <li><a href="#"><i class="ion-star"></i></a></li>
                                                <li><a href="#"><i class="ion-star"></i></a></li>
                                                <li><a href="#"><i class="ion-star"></i></a></li>
                                                <li><a href="#"><i class="ion-star"></i></a></li>
                                            </ul>
                                        </div>
                                        <div class="product_footer d-flex align-items-center">
                                            <div class="price_box">
                                                <span class="regular_price">${{$product->price}}</span>  
                                            </div>
                                            <div class="add_to_cart">
                                                <a href="cart.html" title="add to cart"><span class="lnr lnr-cart"></span></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>   
                    </div>
                @endforeach
            </div> 
        </div>
    </section>
    <!--product area end-->
    
    
    <!--featured categories area start-->
    <section class="featured_categories mb-50">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section_title">
                        <h2><span>Top</span>Categories</h2>
                    </div>
                    <div class="featured_container">
                        <div class="featured_carousel featured_column3 owl-carousel">
                            @foreach ($categories->chunk(2) as $rows)
                                <div class="single_items">
                                    @foreach ($rows as $item)
                                        <div class="single_featured">
                                            <div class="featured_thumb">
                                                <a href="#"><img src="{{$item->getThumbnailUrl()}}" alt=""></a>
                                            </div>
                                            <div class="featured_content">
                                                <h3 class="product_name"><a href="{{route('web.product.category', $item->slug)}}">{{$item->name}}</a></h3>
                                                <div class="sub_featured">
                                                    <ul>
                                                        <li><a href="#">Handbag</a></li>
                                                        <li><a href="#">Accessories</a></li>
                                                        <li><a href="#">Clothing</a></li>
                                                        <li><a href="#">Shoes</a></li>
                                                    </ul>
                                                </div>
                                                <a class="view_more" href="{{route('web.product.category', $item->slug)}}">shop now</a>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            @endforeach
                            
                        </div>
                    </div>
                </div>
            </div> 
        </div>
    </section>
    <!--featured categories area end-->
    
    <!--small product area start-->
    <section class="small_product_area mb-50">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <!--<div class="section_title">
                        <h2><span>Body</span>Parts</h2>
                    </div>-->
                    <div class="product_carousel small_product product_column3 owl-carousel">
                        <div class="single_product">
                            <div class="product_content">
                                <h3><a href="{{route('web.product.show', $product->slug)}}">FUEL PUMP E8023 ACURA CHEVROLET MAZDA TOYOTA LAND CRUISER FE0062 69140</a></h3>
                                <p class="manufacture_product"><a href="#">IGNITION SYSTEM</a></p>
                                <div class="product_ratings">
                                    <ul>
                                        <li><a href="#"><i class="ion-star"></i></a></li>
                                        <li><a href="#"><i class="ion-star"></i></a></li>
                                        <li><a href="#"><i class="ion-star"></i></a></li>
                                        <li><a href="#"><i class="ion-star"></i></a></li>
                                        <li><a href="#"><i class="ion-star"></i></a></li>
                                    </ul>
                                </div>
                                <div class="price_box">
                                    <span class="current_price">$140.00</span>
                                    <span class="old_price">$320.00</span>   
                                </div>
                            </div>
                            <div class="product_thumb">
                                <a class="primary_img" href="{{route('web.product.show', $product->slug)}}"><img src="assets/images/113-E82132.jpg" alt=""></a>
                            </div>
                        </div>
                        <div class="single_product">
                            <div class="product_content">
                                <h3><a href="{{route('web.product.show', $product->slug)}}">NEW FUEL PUMP E2545M FORD F150 3.7L 5.0L 6.2L 2011-2014 4.6L 5.4L 2009-2010</a></h3>
                                <p class="manufacture_product"><a href="#">FUEL SYSTEM</a></p>
                                <div class="product_ratings">
                                    <ul>
                                        <li><a href="#"><i class="ion-star"></i></a></li>
                                        <li><a href="#"><i class="ion-star"></i></a></li>
                                        <li><a href="#"><i class="ion-star"></i></a></li>
                                        <li><a href="#"><i class="ion-star"></i></a></li>
                                        <li><a href="#"><i class="ion-star"></i></a></li>
                                    </ul>
                                </div>
                                <div class="price_box">
                                    <span class="regular_price">$180.00</span>  
                                </div>
                            </div>
                            <div class="product_thumb">
                                <a class="primary_img" href="{{route('web.product.show', $product->slug)}}"><img src="assets/images/102-E3954M.jpg" alt=""></a>
                            </div>
                        </div>
                        <div class="single_product">
                            <div class="product_content">
                                <h3><a href="{{route('web.product.show', $product->slug)}}">IGNITION COIL C1175 UF267 LEXUS ES300 TOYOTA AVALON HIGHLANDER SIENNA 00-06</a></h3>
                                <p class="manufacture_product"><a href="#">IGNITION SYSTEM</a></p>
                                <div class="product_ratings">
                                    <ul>
                                        <li><a href="#"><i class="ion-star"></i></a></li>
                                        <li><a href="#"><i class="ion-star"></i></a></li>
                                        <li><a href="#"><i class="ion-star"></i></a></li>
                                        <li><a href="#"><i class="ion-star"></i></a></li>
                                        <li><a href="#"><i class="ion-star"></i></a></li>
                                    </ul>
                                </div>
                                <div class="price_box">
                                    <span class="current_price">$140.00</span>
                                    <span class="old_price">$320.00</span>   
                                </div>
                            </div>
                            <div class="product_thumb">
                                <a class="primary_img" href="{{route('web.product.show', $product->slug)}}"><img src="assets/images/088-E3240R.jpg" alt=""></a>
                            </div>
                        </div>
                        <div class="single_product">
                            <div class="product_content">
                                <h3><a href="{{route('web.product.show', $product->slug)}}">IGNITION COIL C1393 VOLKSWAGEN BETTLE GOLF JETTA, JETTA CITY L4-2.0L 8223</a></h3>
                                <p class="manufacture_product"><a href="#">HEATING & COOLING</a></p>
                                <div class="product_ratings">
                                    <ul>
                                        <li><a href="#"><i class="ion-star"></i></a></li>
                                        <li><a href="#"><i class="ion-star"></i></a></li>
                                        <li><a href="#"><i class="ion-star"></i></a></li>
                                        <li><a href="#"><i class="ion-star"></i></a></li>
                                        <li><a href="#"><i class="ion-star"></i></a></li>
                                    </ul>
                                </div>
                                <div class="price_box">
                                    <span class="current_price">$140.00</span>
                                    <span class="old_price">$320.00</span>   
                                </div>
                            </div>
                            <div class="product_thumb">
                                <a class="primary_img" href="{{route('web.product.show', $product->slug)}}"><img src="assets/images/086-E21571.jpg" alt=""></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--small product area end-->
    
     <!--small product area start-->
    <section class="small_product_area mb-50">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <!--<div class="section_title">
                        <h2><span>Engine</span>Parts</h2>
                    </div>-->
                    <div class="product_carousel small_product product_column3 owl-carousel">
                        <div class="single_product">
                            <div class="product_content">
                                <h3><a href="{{route('web.product.show', $product->slug)}}">FUEL PUMP REPAIR KIT MU1199 E3978M CADILLAC DEVILLE SEVILLE</a></h3>
                                <p class="manufacture_product"><a href="#">SUSPENSION</a></p>
                                <div class="product_ratings">
                                    <ul>
                                        <li><a href="#"><i class="ion-star"></i></a></li>
                                        <li><a href="#"><i class="ion-star"></i></a></li>
                                        <li><a href="#"><i class="ion-star"></i></a></li>
                                        <li><a href="#"><i class="ion-star"></i></a></li>
                                        <li><a href="#"><i class="ion-star"></i></a></li>
                                    </ul>
                                </div>
                                <div class="price_box">
                                    <span class="regular_price">$160.00</span>  
                                </div>
                            </div>
                            <div class="product_thumb">
                                <a class="primary_img" href="{{route('web.product.show', $product->slug)}}"><img src="assets/images/086-E2157.jpg" alt=""></a>
                            </div>
                        </div>
                        <div class="single_product">
                            <div class="product_content">
                                <h3><a href="{{route('web.product.show', $product->slug)}}">REAR WHEEL HUB BEARING 512195 ELANTRA SPECTRA SPECTRA5 2001-2009</a></h3>
                                <p class="manufacture_product"><a href="#">WHEEL HUBS & BEARINGS</a></p>
                                <div class="product_ratings">
                                    <ul>
                                        <li><a href="#"><i class="ion-star"></i></a></li>
                                        <li><a href="#"><i class="ion-star"></i></a></li>
                                        <li><a href="#"><i class="ion-star"></i></a></li>
                                        <li><a href="#"><i class="ion-star"></i></a></li>
                                        <li><a href="#"><i class="ion-star"></i></a></li>
                                    </ul>
                                </div>
                                <div class="price_box">
                                    <span class="current_price">$140.00</span>
                                    <span class="old_price">$320.00</span>   
                                </div>
                            </div>
                            <div class="product_thumb">
                                <a class="primary_img" href="{{route('web.product.show', $product->slug)}}"><img src="assets/images/513061.jpg" alt=""></a>
                            </div>
                        </div>
                        <div class="single_product">
                            <div class="product_content">
                                <h3><a href="{{route('web.product.show', $product->slug)}}">FUEL PUMP FORD MERCURY GRAND MARQUIS 90-91 TOPAZ 88-91</a></h3>
                                <p class="manufacture_product"><a href="#">ELECTRIC & ELECTRONICS</a></p>
                                <div class="product_ratings">
                                    <ul>
                                        <li><a href="#"><i class="ion-star"></i></a></li>
                                        <li><a href="#"><i class="ion-star"></i></a></li>
                                        <li><a href="#"><i class="ion-star"></i></a></li>
                                        <li><a href="#"><i class="ion-star"></i></a></li>
                                        <li><a href="#"><i class="ion-star"></i></a></li>
                                    </ul>
                                </div>
                                <div class="price_box">
                                    <span class="regular_price">$280.00</span>  
                                </div>
                            </div>
                            <div class="product_thumb">
                                <a class="primary_img" href="{{route('web.product.show', $product->slug)}}"><img src="assets/images/051-E2059M.jpg" alt=""></a>
                            </div>
                        </div>
                        <div class="single_product">
                            <div class="product_content">
                                <h3><a href="{{route('web.product.show', $product->slug)}}">IGNITION DISTRIBUTOR TD22U HONDA CIVIC 1ST GENERATION ENGINE JDM B16A1</a></h3>
                                 <p class="manufacture_product"><a href="#">IGNITION SYSTEM</a></p>
                                <div class="product_ratings">
                                    <ul>
                                        <li><a href="#"><i class="ion-star"></i></a></li>
                                        <li><a href="#"><i class="ion-star"></i></a></li>
                                        <li><a href="#"><i class="ion-star"></i></a></li>
                                        <li><a href="#"><i class="ion-star"></i></a></li>
                                        <li><a href="#"><i class="ion-star"></i></a></li>
                                    </ul>
                                </div>
                                <div class="price_box">
                                    <span class="current_price">$140.00</span>
                                    <span class="old_price">$320.00</span>   
                                </div>
                            </div>
                            <div class="product_thumb">
                                <a class="primary_img" href="{{route('web.product.show', $product->slug)}}"><img src="assets/images/500-E3563M.jpg" alt=""></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--small product area end-->
    
    <!--banner area start-->
    <section class="banner_area mb-50">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="banner_container">
                        <div class="single_banner">
                            <div class="banner_thumb">
                                <a href="#"><img src="assets/img/slider/slider2.png" alt=""></a>
                                <div class="banner_text">
                                    <h3>Why</h3>
                                    <h2>JMI Interpart?</h2>
                                    <a href="shop.html">About Us</a>
                                </div>
                            </div>
                        </div>
                        <div class="single_banner">
                            <div class="banner_thumb">
                                <a href="#"><img src="assets/img/slider/slider3.png" alt=""></a>
                                <div class="banner_text">
                                    <h3>Download</h3>
                                    <h2>Our Catalogue</h2>
                                    <a href="shop.html">Download</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--banner area end-->
    
     <!--product area start-->
    <section class="product_area mb-50">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section_title">
                        <h2><span>Special</span>Offers</h2>
                    </div>
                    <div class="product_carousel product_column5 owl-carousel">
                        <div class="single_product">
                            <div class="product_name">
                                <h3><a href="{{route('web.product.show', $product->slug)}}">FILTER STRAINER FUEL PUMP JRN001 E150 E350 CAMARO CAPRICE ASTRO DEVILLE</a></h3>
                                <p class="manufacture_product"><a href="#">FUEL SYSTEM</a></p>
                            </div>
                            <div class="product_thumb">
                                <a class="primary_img" href="{{route('web.product.show', $product->slug)}}"><img src="assets/images/312-E2068.jpg" alt=""></a>
                                <a class="secondary_img" href="{{route('web.product.show', $product->slug)}}"><img src="assets/images/312-E2068.jpg" alt=""></a>
                                <div class="label_product">
                                    <span class="label_sale">-57%</span>
                                </div>

                                <div class="action_links">
                                    <ul>
                                        <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box"  title="quick view"> <span class="lnr lnr-magnifier"></span></a></li>
                                        <!--<li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><span class="lnr lnr-heart"></span></a></li>
                                        <li class="compare"><a href="#" title="compare"><span class="lnr lnr-sync"></span></a></li>-->
                                    </ul>
                                </div>
                            </div>
                            <div class="product_content">
                                <div class="product_ratings">
                                    <ul>
                                        <li><a href="#"><i class="ion-star"></i></a></li>
                                        <li><a href="#"><i class="ion-star"></i></a></li>
                                        <li><a href="#"><i class="ion-star"></i></a></li>
                                        <li><a href="#"><i class="ion-star"></i></a></li>
                                        <li><a href="#"><i class="ion-star"></i></a></li>
                                    </ul>
                                </div>
                                <div class="product_footer d-flex align-items-center">
                                    <div class="price_box">
                                        <span class="regular_price">$180.00</span>  
                                    </div>
                                    <div class="add_to_cart">
                                        <a href="cart.html" title="add to cart"><span class="lnr lnr-cart"></span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="single_product">
                            <div class="product_name">
                                <h3><a href="{{route('web.product.show', $product->slug)}}">NEW BARRICADE FUEL INJECTION HOSES 1/4 15 FT ROLLS MADE IN USA</a></h3>
                                <p class="manufacture_product"><a href="#">FUEL SYSTEM</a></p>
                            </div>
                            <div class="product_thumb">
                                <a class="primary_img" href="{{route('web.product.show', $product->slug)}}"><img src="assets/images/1201-dist-pick-up-coil.jpg" alt=""></a>
                                <a class="secondary_img" href="{{route('web.product.show', $product->slug)}}"><img src="assets/images/1201-dist-pick-up-coil.jpg" alt=""></a>
                                <div class="label_product">
                                    <span class="label_sale">-47%</span>
                                </div>

                                <div class="action_links">
                                    <ul>
                                        <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box"  title="quick view"> <span class="lnr lnr-magnifier"></span></a></li>
                                        <!--<li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><span class="lnr lnr-heart"></span></a></li>
                                        <li class="compare"><a href="#" title="compare"><span class="lnr lnr-sync"></span></a></li>-->
                                    </ul>
                                </div>
                            </div>
                            <div class="product_content">
                                <div class="product_ratings">
                                    <ul>
                                        <li><a href="#"><i class="ion-star"></i></a></li>
                                        <li><a href="#"><i class="ion-star"></i></a></li>
                                        <li><a href="#"><i class="ion-star"></i></a></li>
                                        <li><a href="#"><i class="ion-star"></i></a></li>
                                        <li><a href="#"><i class="ion-star"></i></a></li>
                                    </ul>
                                </div>
                                <div class="product_footer d-flex align-items-center">
                                    <div class="price_box">
                                        <span class="current_price">$160.00</span>
                                        <span class="old_price">$3200.00</span>   
                                    </div>
                                    <div class="add_to_cart">
                                        <a href="cart.html" title="add to cart"><span class="lnr lnr-cart"></span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="single_product">
                            <div class="product_name">
                                <h3><a href="{{route('web.product.show', $product->slug)}}">IGNITION COIL GMC ISUZU JEEP WAGONEER OLDSMOBILE PONTIAC FIERO C846 GC-69</a></h3>
                                <p class="manufacture_product"><a href="#">IGNITION SYSTEM</a></p>
                            </div>
                            <div class="product_thumb">
                                <a class="primary_img" href="{{route('web.product.show', $product->slug)}}"><img src="assets/images/306-E7094M.jpg" alt=""></a>
                                <a class="secondary_img" href="{{route('web.product.show', $product->slug)}}"><img src="assets/images/306-E7094M.jpg" alt=""></a>
                                <div class="label_product">
                                    <span class="label_sale">-57%</span>
                                </div>

                                <div class="action_links">
                                    <ul>
                                        <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box"  title="quick view"> <span class="lnr lnr-magnifier"></span></a></li>
                                        <!--<li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><span class="lnr lnr-heart"></span></a></li>
                                        <li class="compare"><a href="#" title="compare"><span class="lnr lnr-sync"></span></a></li>-->
                                    </ul>
                                </div>
                            </div>
                            <div class="product_content">
                                <div class="product_ratings">
                                    <ul>
                                        <li><a href="#"><i class="ion-star"></i></a></li>
                                        <li><a href="#"><i class="ion-star"></i></a></li>
                                        <li><a href="#"><i class="ion-star"></i></a></li>
                                        <li><a href="#"><i class="ion-star"></i></a></li>
                                        <li><a href="#"><i class="ion-star"></i></a></li>
                                    </ul>
                                </div>
                                <div class="product_footer d-flex align-items-center">
                                    <div class="price_box">
                                        <span class="regular_price">$150.00</span>  
                                    </div>
                                    <div class="add_to_cart">
                                        <a href="cart.html" title="add to cart"><span class="lnr lnr-cart"></span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="single_product">
                            <div class="product_name">
                                <h3><a href="{{route('web.product.show', $product->slug)}}">PAIR IGNITION COIL C1339 9315 MAZ DA MIATA L4 1.8L 2001-2005</a></h3>
                                <p class="manufacture_product"><a href="#">IGNITION SYSTEM</a></p>
                            </div>
                            <div class="product_thumb">
                                <a class="primary_img" href="{{route('web.product.show', $product->slug)}}"><img src="assets/images/500-E3563M3.jpg" alt=""></a>
                                <a class="secondary_img" href="{{route('web.product.show', $product->slug)}}"><img src="assets/images/500-E3563M3.jpg" alt=""></a>
                                <div class="label_product">
                                    <span class="label_sale">-57%</span>
                                </div>

                                <div class="action_links">
                                    <ul>
                                        <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box"  title="quick view"> <span class="lnr lnr-magnifier"></span></a></li>
                                        <!--<li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><span class="lnr lnr-heart"></span></a></li>
                                        <li class="compare"><a href="#" title="compare"><span class="lnr lnr-sync"></span></a></li>-->
                                    </ul>
                                </div>
                            </div>
                            <div class="product_content">
                                <div class="product_ratings">
                                    <ul>
                                        <li><a href="#"><i class="ion-star"></i></a></li>
                                        <li><a href="#"><i class="ion-star"></i></a></li>
                                        <li><a href="#"><i class="ion-star"></i></a></li>
                                        <li><a href="#"><i class="ion-star"></i></a></li>
                                        <li><a href="#"><i class="ion-star"></i></a></li>
                                    </ul>
                                </div>
                                <div class="product_footer d-flex align-items-center">
                                    <div class="price_box">
                                        <span class="regular_price">$175.00</span>  
                                    </div>
                                    <div class="add_to_cart">
                                        <a href="cart.html" title="add to cart"><span class="lnr lnr-cart"></span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="single_product">
                            <div class="product_name">
                                <h3><a href="{{route('web.product.show', $product->slug)}}">IGNITION DISTRIBUTOR TD44U D8014 ACURA INTEGRA HON DA CIVIC DEL SOL</a></h3>
                                <p class="manufacture_product"><a href="#">IGNITION SYSTEM</a></p>
                            </div>
                            <div class="product_thumb">
                                <a class="primary_img" href="{{route('web.product.show', $product->slug)}}"><img src="assets/images/500-E3563M.jpg" alt=""></a>
                                <a class="secondary_img" href="{{route('web.product.show', $product->slug)}}"><img src="assets/images/500-E3563M.jpg" alt=""></a>
                                <div class="label_product">
                                    <span class="label_sale">-07%</span>
                                </div>

                                <div class="action_links">
                                    <ul>
                                        <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box"  title="quick view"> <span class="lnr lnr-magnifier"></span></a></li>
                                        <!--<li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><span class="lnr lnr-heart"></span></a></li>
                                        <li class="compare"><a href="#" title="compare"><span class="lnr lnr-sync"></span></a></li>-->
                                    </ul>
                                </div>
                            </div>
                            <div class="product_content">
                                <div class="product_ratings">
                                    <ul>
                                        <li><a href="#"><i class="ion-star"></i></a></li>
                                        <li><a href="#"><i class="ion-star"></i></a></li>
                                        <li><a href="#"><i class="ion-star"></i></a></li>
                                        <li><a href="#"><i class="ion-star"></i></a></li>
                                        <li><a href="#"><i class="ion-star"></i></a></li>
                                    </ul>
                                </div>
                                <div class="product_footer d-flex align-items-center">
                                    <div class="price_box">
                                        <span class="current_price">$180.00</span>
                                        <span class="old_price">$420.00</span>   
                                    </div>
                                    <div class="add_to_cart">
                                        <a href="cart.html" title="add to cart"><span class="lnr lnr-cart"></span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="single_product">
                            <div class="product_name">
                                <h3><a href="{{route('web.product.show', $product->slug)}}">FRONT WHEEL HUB BEARING 513044 CHEVY MONTE CARLO PONTIAC GRAND PRIX 88-01</a></h3>
                                <p class="manufacture_product"><a href="#">WHEEL HUBS & BEARINGS</a></p>
                            </div>
                            <div class="product_thumb">
                                <a class="primary_img" href="{{route('web.product.show', $product->slug)}}"><img src="assets/images/1366-512257t.jpg" alt=""></a>
                                <a class="secondary_img" href="{{route('web.product.show', $product->slug)}}"><img src="assets/images/1366-512257t.jpg" alt=""></a>
                                <div class="label_product">
                                    <span class="label_sale">-57%</span>
                                </div>

                                <div class="action_links">
                                    <ul>
                                        <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box"  title="quick view"> <span class="lnr lnr-magnifier"></span></a></li>
                                        <!--<li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><span class="lnr lnr-heart"></span></a></li>
                                        <li class="compare"><a href="#" title="compare"><span class="lnr lnr-sync"></span></a></li>-->
                                    </ul>
                                </div>
                            </div>
                            <div class="product_content">
                                <div class="product_ratings">
                                    <ul>
                                        <li><a href="#"><i class="ion-star"></i></a></li>
                                        <li><a href="#"><i class="ion-star"></i></a></li>
                                        <li><a href="#"><i class="ion-star"></i></a></li>
                                        <li><a href="#"><i class="ion-star"></i></a></li>
                                        <li><a href="#"><i class="ion-star"></i></a></li>
                                    </ul>
                                </div>
                                <div class="product_footer d-flex align-items-center">
                                    <div class="price_box">
                                        <span class="current_price">$140.00</span>
                                        <span class="old_price">$320.00</span>   
                                    </div>
                                    <div class="add_to_cart">
                                        <a href="cart.html" title="add to cart"><span class="lnr lnr-cart"></span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="single_product">
                            <div class="product_name">
                                <h3><a href="{{route('web.product.show', $product->slug)}}">FRONT LEFT WHEEL HUB BEARING 513176 JEEP LIBERTY 2002 03 04 05 06 2007 4WD </a></h3>
                                <p class="manufacture_product"><a href="#">WHEEL HUBS & BEARINGS</a></p>
                            </div>
                            <div class="product_thumb">
                                <a class="primary_img" href="{{route('web.product.show', $product->slug)}}"><img src="assets/images/1366-512257.jpg" alt=""></a>
                                <a class="secondary_img" href="{{route('web.product.show', $product->slug)}}"><img src="assets/images/1366-512257.jpg" alt=""></a>
                                <div class="label_product">
                                    <span class="label_sale">-57%</span>
                                </div>

                                <div class="action_links">
                                    <ul>
                                        <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box"  title="quick view"> <span class="lnr lnr-magnifier"></span></a></li>
                                        <!--<li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><span class="lnr lnr-heart"></span></a></li>
                                        <li class="compare"><a href="#" title="compare"><span class="lnr lnr-sync"></span></a></li>-->
                                    </ul>
                                </div>
                            </div>
                            <div class="product_content">
                                <div class="product_ratings">
                                    <ul>
                                        <li><a href="#"><i class="ion-star"></i></a></li>
                                        <li><a href="#"><i class="ion-star"></i></a></li>
                                        <li><a href="#"><i class="ion-star"></i></a></li>
                                        <li><a href="#"><i class="ion-star"></i></a></li>
                                        <li><a href="#"><i class="ion-star"></i></a></li>
                                    </ul>
                                </div>
                                <div class="product_footer d-flex align-items-center">
                                    <div class="price_box">
                                        <span class="regular_price">$160.00</span>  
                                    </div>
                                    <div class="add_to_cart">
                                        <a href="cart.html" title="add to cart"><span class="lnr lnr-cart"></span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
                   
        </div>
    </section>
    <!--product area end-->
    
    
    <!--brand area start-->
    <div class="brand_area mb-42">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="brand_container owl-carousel">
                        @foreach ($brands as $brand)
                            <div class="single_brand">
                                <a href="{{route('web.product.category', $brand->slug)}}">
                                    <img src="{{$brand->getThumbnailUrl()}}" alt="" style="width: 100px; height: 100px">
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--brand area end-->
    
    
    <!--custom product area-->
    <section class="custom_product_area">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-12">
                    <!--featured product area-->
                    <div class="custom_product">
                         <div class="section_title">
                            <h2> Featured Products </h2>
                        </div>
                        <div class="small_product_items small_product_active">
                            <div class="single_product_items">
                                <div class="product_thumb">
                                    <a href="{{route('web.product.show', $product->slug)}}"><img src="assets/images/1366-5122578.jpg" alt=""></a>
                                </div>
                                <div class="product_content">
                                    <div class="product_name">
                                        <h3><a href="{{route('web.product.show', $product->slug)}}">FRONT WHEEL HUB BEARING 513087 RIVIERA LESABRE BONNEVILLE 98 ELDORADO 91-99</a></h3>
                                    </div>
                                    <div class="product_ratings">
                                        <ul>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                        </ul>
                                    </div>
                                    <div class="price_box">
                                        <span class="current_price">$180.00</span>
                                        <span class="old_price">$420.00</span>  
                                    </div>
                                </div>
                            </div>
                            <div class="single_product_items">
                                <div class="product_thumb">
                                    <a href="{{route('web.product.show', $product->slug)}}"><img src="assets/images/1201-dist-pick-up-coil.jpg" alt=""></a>
                                </div>
                                <div class="product_content">
                                    <div class="product_name">
                                        <h3><a href="{{route('web.product.show', $product->slug)}}">IGNITION DISTRIBUTOR TD31U D8031 HOND A ACCORD 1990 1991 2.2L L4</a></h3>
                                    </div>
                                    <div class="product_ratings">
                                        <ul>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                        </ul>
                                    </div>
                                    <div class="price_box">
                                        <span class="regular_price">$170.00</span> 
                                    </div>
                                </div>
                            </div>
                            <div class="single_product_items">
                                <div class="product_thumb">
                                    <a href="{{route('web.product.show', $product->slug)}}"><img src="assets/images/306-E7094M.jpg" alt=""></a>
                                </div>
                                <div class="product_content">
                                    <div class="product_name">
                                        <h3><a href="{{route('web.product.show', $product->slug)}}">FUEL PUMP W/ FILTER MU1314 CHEV SILVERADO SIERRA 1500 4.8L 5.3L 6.0L 2004-07</a></h3>
                                    </div>
                                    <div class="product_ratings">
                                        <ul>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                        </ul>
                                    </div>
                                    <div class="price_box">
                                        <span class="regular_price">$160.00</span> 
                                    </div>
                                </div>
                            </div>
                            <div class="single_product_items">
                                <div class="product_thumb">
                                    <a href="{{route('web.product.show', $product->slug)}}"><img src="assets/images/306-E7094M2.jpg" alt=""></a>
                                </div>
                                <div class="product_content">
                                    <div class="product_name">
                                        <h3><a href="{{route('web.product.show', $product->slug)}}">NEW FUEL PUMP STRAINER FOR FUEL PUMPS N126 JRN126 </a></h3>
                                    </div>
                                    <div class="product_ratings">
                                        <ul>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                        </ul>
                                    </div>
                                    <div class="price_box">
                                        <span class="current_price">$180.00</span>
                                        <span class="old_price">$420.00</span>  
                                    </div>
                                </div>
                            </div>
                            <div class="single_product_items">
                                <div class="product_thumb">
                                    <a href="{{route('web.product.show', $product->slug)}}"><img src="assets/images/312-E20683.jpg" alt=""></a>
                                </div>
                                <div class="product_content">
                                    <div class="product_name">
                                        <h3><a href="{{route('web.product.show', $product->slug)}}">ELECTRIC FUEL PUMP E7629 SUZUKI SWIFT ESTEEM 1.3L 1.6L 1993-2007</a></h3>
                                    </div>
                                    <div class="product_ratings">
                                        <ul>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                        </ul>
                                    </div>
                                    <div class="price_box">
                                        <span class="current_price">$180.00</span>
                                        <span class="old_price">$420.00</span>  
                                    </div>
                                </div>
                            </div>
                            <div class="single_product_items">
                                <div class="product_thumb">
                                    <a href="{{route('web.product.show', $product->slug)}}"><img src="assets/img/product/product6.jpg" alt=""></a>
                                </div>
                                <div class="product_content">
                                    <div class="product_name">
                                        <h3><a href="{{route('web.product.show', $product->slug)}}">1. New and sale new  badge product </a></h3>
                                    </div>
                                    <div class="product_ratings">
                                        <ul>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                        </ul>
                                    </div>
                                    <div class="price_box">
                                        <span class="current_price">$180.00</span>
                                        <span class="old_price">$420.00</span>  
                                    </div>
                                </div>
                            </div>
                            <div class="single_product_items">
                                <div class="product_thumb">
                                    <a href="{{route('web.product.show', $product->slug)}}"><img src="assets/images/312-E20680.jpg" alt=""></a>
                                </div>
                                <div class="product_content">
                                    <div class="product_name">
                                        <h3><a href="{{route('web.product.show', $product->slug)}}">BRAND NEW FUEL PUMP SUZUKI SUPER CARRY 1.0L 33kW 7622 1985-1999</a></h3>
                                    </div>
                                    <div class="product_ratings">
                                        <ul>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                        </ul>
                                    </div>
                                    <div class="price_box">
                                        <span class="current_price">$180.00</span>
                                        <span class="old_price">$420.00</span>  
                                    </div>
                                </div>
                            </div>
                            <div class="single_product_items">
                                <div class="product_thumb">
                                    <a href="{{route('web.product.show', $product->slug)}}"><img src="assets/img/product/product2.jpg" alt=""></a>
                                </div>
                                <div class="product_content">
                                    <div class="product_name">
                                        <h3><a href="{{route('web.product.show', $product->slug)}}">JBL Flip 3 Splashproof  Portable Bluetooth</a></h3>
                                    </div>
                                    <div class="product_ratings">
                                        <ul>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                        </ul>
                                    </div>
                                    <div class="price_box">
                                        <span class="regular_price">$170.00</span> 
                                    </div>
                                </div>
                            </div>
                            <div class="single_product_items">
                                <div class="product_thumb">
                                    <a href="{{route('web.product.show', $product->slug)}}"><img src="assets/img/product/product3.jpg" alt=""></a>
                                </div>
                                <div class="product_content">
                                    <div class="product_name">
                                        <h3><a href="{{route('web.product.show', $product->slug)}}">Kodak PIXPRO Astro Zoom AZ421 16 MP</a></h3>
                                    </div>
                                    <div class="product_ratings">
                                        <ul>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                        </ul>
                                    </div>
                                    <div class="price_box">
                                        <span class="regular_price">$160.00</span> 
                                    </div>
                                </div>
                            </div>
                            <div class="single_product_items">
                                <div class="product_thumb">
                                    <a href="{{route('web.product.show', $product->slug)}}"><img src="assets/img/product/product4.jpg" alt=""></a>
                                </div>
                                <div class="product_content">
                                    <div class="product_name">
                                        <h3><a href="{{route('web.product.show', $product->slug)}}">1. New and sale new  badge product </a></h3>
                                    </div>
                                    <div class="product_ratings">
                                        <ul>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                        </ul>
                                    </div>
                                    <div class="price_box">
                                        <span class="current_price">$180.00</span>
                                        <span class="old_price">$420.00</span>  
                                    </div>
                                </div>
                            </div>
                            <div class="single_product_items">
                                <div class="product_thumb">
                                    <a href="{{route('web.product.show', $product->slug)}}"><img src="assets/img/product/product5.jpg" alt=""></a>
                                </div>
                                <div class="product_content">
                                    <div class="product_name">
                                        <h3><a href="{{route('web.product.show', $product->slug)}}">Accusantium dolorem  Security Camera</a></h3>
                                    </div>
                                    <div class="product_ratings">
                                        <ul>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                        </ul>
                                    </div>
                                    <div class="price_box">
                                        <span class="current_price">$180.00</span>
                                        <span class="old_price">$420.00</span>  
                                    </div>
                                </div>
                            </div>
                            <div class="single_product_items">
                                <div class="product_thumb">
                                    <a href="{{route('web.product.show', $product->slug)}}"><img src="assets/img/product/product6.jpg" alt=""></a>
                                </div>
                                <div class="product_content">
                                    <div class="product_name">
                                        <h3><a href="{{route('web.product.show', $product->slug)}}">1. New and sale new  badge product </a></h3>
                                    </div>
                                    <div class="product_ratings">
                                        <ul>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                        </ul>
                                    </div>
                                    <div class="price_box">
                                        <span class="current_price">$180.00</span>
                                        <span class="old_price">$420.00</span>  
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                   <!--featured product end-->
                </div>
                <div class="col-lg-4 col-md-12">
                    <!--mostview product area-->
                    <div class="custom_product">
                        <div class="section_title">
                            <h2> Most view products </h2>
                        </div>
                        <div class="small_product_items small_product_active">
                            <div class="single_product_items">
                                <div class="product_thumb">
                                    <a href="{{route('web.product.show', $product->slug)}}"><img src="assets/images/306-E7094M2.jpg" alt=""></a>
                                </div>
                                <div class="product_content">
                                    <div class="product_name">
                                        <h3><a href="{{route('web.product.show', $product->slug)}}">NEW FUEL PUMP STRAINER FOR FUEL PUMPS N126 JRN126 </a></h3>
                                    </div>
                                    <div class="product_ratings">
                                        <ul>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                        </ul>
                                    </div>
                                    <div class="price_box">
                                        <span class="current_price">$180.00</span>
                                        <span class="old_price">$420.00</span>  
                                    </div>
                                </div>
                            </div>
                            <div class="single_product_items">
                                <div class="product_thumb">
                                    <a href="{{route('web.product.show', $product->slug)}}"><img src="assets/images/312-E20683.jpg" alt=""></a>
                                </div>
                                <div class="product_content">
                                    <div class="product_name">
                                        <h3><a href="{{route('web.product.show', $product->slug)}}">ELECTRIC FUEL PUMP E7629 SUZUKI SWIFT ESTEEM 1.3L 1.6L 1993-2007</a></h3>
                                    </div>
                                    <div class="product_ratings">
                                        <ul>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                        </ul>
                                    </div>
                                    <div class="price_box">
                                        <span class="current_price">$180.00</span>
                                        <span class="old_price">$420.00</span>  
                                    </div>
                                </div>
                            </div>
                            <div class="single_product_items">
                                <div class="product_thumb">
                                    <a href="{{route('web.product.show', $product->slug)}}"><img src="assets/images/357-5150244.jpg" alt=""></a>
                                </div>
                                <div class="product_content">
                                    <div class="product_name">
                                        <h3><a href="{{route('web.product.show', $product->slug)}}">FRONT WHEEL HUB BEARING 513188 BUICK CHEVY GMC OLDSMOBILE SAAB WITH ABS</a></h3>
                                    </div>
                                    <div class="product_ratings">
                                        <ul>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                        </ul>
                                    </div>
                                    <div class="price_box">
                                        <span class="regular_price">$160.00</span> 
                                    </div>
                                </div>
                            </div>
                            <div class="single_product_items">
                                <div class="product_thumb">
                                    <a href="{{route('web.product.show', $product->slug)}}"><img src="assets/img/product/product10.jpg" alt=""></a>
                                </div>
                                <div class="product_content">
                                    <div class="product_name">
                                        <h3><a href="{{route('web.product.show', $product->slug)}}">1. New and sale new  badge product </a></h3>
                                    </div>
                                    <div class="product_ratings">
                                        <ul>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                        </ul>
                                    </div>
                                    <div class="price_box">
                                        <span class="current_price">$160.00</span>
                                        <span class="old_price">$420.00</span>  
                                    </div>
                                </div>
                            </div>
                            <div class="single_product_items">
                                <div class="product_thumb">
                                    <a href="{{route('web.product.show', $product->slug)}}"><img src="assets/img/product/product11.jpg" alt=""></a>
                                </div>
                                <div class="product_content">
                                    <div class="product_name">
                                        <h3><a href="{{route('web.product.show', $product->slug)}}">Accusantium dolorem  Security Camera</a></h3>
                                    </div>
                                    <div class="product_ratings">
                                        <ul>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                        </ul>
                                    </div>
                                    <div class="price_box">
                                        <span class="current_price">$180.00</span>
                                        <span class="old_price">$420.00</span>  
                                    </div>
                                </div>
                            </div>
                            <div class="single_product_items">
                                <div class="product_thumb">
                                    <a href="{{route('web.product.show', $product->slug)}}"><img src="assets/img/product/product12.jpg" alt=""></a>
                                </div>
                                <div class="product_content">
                                    <div class="product_name">
                                        <h3><a href="{{route('web.product.show', $product->slug)}}">1. New and sale new  badge product </a></h3>
                                    </div>
                                    <div class="product_ratings">
                                        <ul>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                        </ul>
                                    </div>
                                    <div class="price_box">
                                        <span class="current_price">$180.00</span>
                                        <span class="old_price">$420.00</span>  
                                    </div>
                                </div>
                            </div>
                            <div class="single_product_items">
                                <div class="product_thumb">
                                    <a href="{{route('web.product.show', $product->slug)}}"><img src="assets/img/product/product8.jpg" alt=""></a>
                                </div>
                                <div class="product_content">
                                    <div class="product_name">
                                        <h3><a href="{{route('web.product.show', $product->slug)}}">1. New and sale new  badge product </a></h3>
                                    </div>
                                    <div class="product_ratings">
                                        <ul>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                        </ul>
                                    </div>
                                    <div class="price_box">
                                        <span class="current_price">$170.00</span>
                                        <span class="old_price">$430.00</span>  
                                    </div>
                                </div>
                            </div>
                            <div class="single_product_items">
                                <div class="product_thumb">
                                    <a href="{{route('web.product.show', $product->slug)}}"><img src="assets/img/product/product9.jpg" alt=""></a>
                                </div>
                                <div class="product_content">
                                    <div class="product_name">
                                        <h3><a href="{{route('web.product.show', $product->slug)}}">Accusantium dolorem  Security Camera</a></h3>
                                    </div>
                                    <div class="product_ratings">
                                        <ul>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                        </ul>
                                    </div>
                                    <div class="price_box">
                                        <span class="regular_price">$170.00</span> 
                                    </div>
                                </div>
                            </div>
                            <div class="single_product_items">
                                <div class="product_thumb">
                                    <a href="{{route('web.product.show', $product->slug)}}"><img src="assets/img/product/product4.jpg" alt=""></a>
                                </div>
                                <div class="product_content">
                                    <div class="product_name">
                                        <h3><a href="{{route('web.product.show', $product->slug)}}">Kodak PIXPRO Astro Zoom AZ421 16 MP</a></h3>
                                    </div>
                                    <div class="product_ratings">
                                        <ul>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                        </ul>
                                    </div>
                                    <div class="price_box">
                                        <span class="regular_price">$160.00</span> 
                                    </div>
                                </div>
                            </div>
                            <div class="single_product_items">
                                <div class="product_thumb">
                                    <a href="{{route('web.product.show', $product->slug)}}"><img src="assets/img/product/product10.jpg" alt=""></a>
                                </div>
                                <div class="product_content">
                                    <div class="product_name">
                                        <h3><a href="{{route('web.product.show', $product->slug)}}">1. New and sale new  badge product </a></h3>
                                    </div>
                                    <div class="product_ratings">
                                        <ul>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                        </ul>
                                    </div>
                                    <div class="price_box">
                                        <span class="current_price">$160.00</span>
                                        <span class="old_price">$420.00</span>  
                                    </div>
                                </div>
                            </div>
                            <div class="single_product_items">
                                <div class="product_thumb">
                                    <a href="{{route('web.product.show', $product->slug)}}"><img src="assets/img/product/product11.jpg" alt=""></a>
                                </div>
                                <div class="product_content">
                                    <div class="product_name">
                                        <h3><a href="{{route('web.product.show', $product->slug)}}">Accusantium dolorem  Security Camera</a></h3>
                                    </div>
                                    <div class="product_ratings">
                                        <ul>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                        </ul>
                                    </div>
                                    <div class="price_box">
                                        <span class="current_price">$180.00</span>
                                        <span class="old_price">$420.00</span>  
                                    </div>
                                </div>
                            </div>
                            <div class="single_product_items">
                                <div class="product_thumb">
                                    <a href="{{route('web.product.show', $product->slug)}}"><img src="assets/img/product/product12.jpg" alt=""></a>
                                </div>
                                <div class="product_content">
                                    <div class="product_name">
                                        <h3><a href="{{route('web.product.show', $product->slug)}}">1. New and sale new  badge product </a></h3>
                                    </div>
                                    <div class="product_ratings">
                                        <ul>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                        </ul>
                                    </div>
                                    <div class="price_box">
                                        <span class="current_price">$180.00</span>
                                        <span class="old_price">$420.00</span>  
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                   <!--mostview product end-->
                </div>
                <div class="col-lg-4 col-md-12">
                    <!--bestSeller product area-->
                    <div class="custom_product">
                        <div class="section_title">
                            <h2> bestseller products </h2>
                        </div>
                        <div class="small_product_items small_product_active">
                            <div class="single_product_items">
                                <div class="product_thumb">
                                    <a href="{{route('web.product.show', $product->slug)}}"><img src="assets/images/1366-5122578.jpg" alt=""></a>
                                </div>
                                <div class="product_content">
                                    <div class="product_name">
                                        <h3><a href="{{route('web.product.show', $product->slug)}}">FRONT WHEEL HUB BEARING 515015 CHEVY GMC K2500 K3500 WITH ABS 4WD 1995</a></h3>
                                    </div>
                                    <div class="product_ratings">
                                        <ul>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                        </ul>
                                    </div>
                                    <div class="price_box">
                                        <span class="current_price">$180.00</span>
                                        <span class="old_price">$420.00</span>  
                                    </div>
                                </div>
                            </div>
                            <div class="single_product_items">
                                <div class="product_thumb">
                                    <a href="{{route('web.product.show', $product->slug)}}"><img src="assets/images/312-E20684.jpg" alt=""></a>
                                </div>
                                <div class="product_content">
                                    <div class="product_name">
                                        <h3><a href="{{route('web.product.show', $product->slug)}}">IGNITION DISTRIBUTOR ROTOR DR161 VORTEC V6 V8 GM ESCALADE BLAZER TAHOE DR975</a></h3>
                                    </div>
                                    <div class="product_ratings">
                                        <ul>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                        </ul>
                                    </div>
                                    <div class="price_box">
                                        <span class="regular_price">$170.00</span> 
                                    </div>
                                </div>
                            </div>
                            <div class="single_product_items">
                                <div class="product_thumb">
                                    <a href="{{route('web.product.show', $product->slug)}}"><img src="assets/images/312-E20680.jpg" alt=""></a>
                                </div>
                                <div class="product_content">
                                    <div class="product_name">
                                        <h3><a href="{{route('web.product.show', $product->slug)}}">PAIR IGNITION COILS FD487 F150 EXPLORER MARK B2300 TOWN 2.5 4.6 5.0L 91-03</a></h3>
                                    </div>
                                    <div class="product_ratings">
                                        <ul>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                        </ul>
                                    </div>
                                    <div class="price_box">
                                        <span class="regular_price">$160.00</span> 
                                    </div>
                                </div>
                            </div>
                            <div class="single_product_items">
                                <div class="product_thumb">
                                    <a href="{{route('web.product.show', $product->slug)}}"><img src="assets/img/product/product13.jpg" alt=""></a>
                                </div>
                                <div class="product_content">
                                    <div class="product_name">
                                        <h3><a href="{{route('web.product.show', $product->slug)}}">1. New and sale new  badge product </a></h3>
                                    </div>
                                    <div class="product_ratings">
                                        <ul>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                        </ul>
                                    </div>
                                    <div class="price_box">
                                        <span class="current_price">$180.00</span>
                                        <span class="old_price">$420.00</span>  
                                    </div>
                                </div>
                            </div>
                            <div class="single_product_items">
                                <div class="product_thumb">
                                    <a href="{{route('web.product.show', $product->slug)}}"><img src="assets/img/product/product14.jpg" alt=""></a>
                                </div>
                                <div class="product_content">
                                    <div class="product_name">
                                        <h3><a href="{{route('web.product.show', $product->slug)}}">Accusantium dolorem  Security Camera</a></h3>
                                    </div>
                                    <div class="product_ratings">
                                        <ul>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                        </ul>
                                    </div>
                                    <div class="price_box">
                                        <span class="current_price">$180.00</span>
                                        <span class="old_price">$420.00</span>  
                                    </div>
                                </div>
                            </div>
                            <div class="single_product_items">
                                <div class="product_thumb">
                                    <a href="{{route('web.product.show', $product->slug)}}"><img src="assets/img/product/product15.jpg" alt=""></a>
                                </div>
                                <div class="product_content">
                                    <div class="product_name">
                                        <h3><a href="{{route('web.product.show', $product->slug)}}">1. New and sale new  badge product </a></h3>
                                    </div>
                                    <div class="product_ratings">
                                        <ul>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                        </ul>
                                    </div>
                                    <div class="price_box">
                                        <span class="current_price">$180.00</span>
                                        <span class="old_price">$420.00</span>  
                                    </div>
                                </div>
                            </div>
                            <div class="single_product_items">
                                <div class="product_thumb">
                                    <a href="{{route('web.product.show', $product->slug)}}"><img src="assets/img/product/product10.jpg" alt=""></a>
                                </div>
                                <div class="product_content">
                                    <div class="product_name">
                                        <h3><a href="{{route('web.product.show', $product->slug)}}">Variable with soldout product for title</a></h3>
                                    </div>
                                    <div class="product_ratings">
                                        <ul>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                        </ul>
                                    </div>
                                    <div class="price_box">
                                        <span class="current_price">$180.00</span>
                                        <span class="old_price">$420.00</span>  
                                    </div>
                                </div>
                            </div>
                            <div class="single_product_items">
                                <div class="product_thumb">
                                    <a href="{{route('web.product.show', $product->slug)}}"><img src="assets/img/product/product11.jpg" alt=""></a>
                                </div>
                                <div class="product_content">
                                    <div class="product_name">
                                        <h3><a href="{{route('web.product.show', $product->slug)}}">JBL Flip 3 Splashproof  Portable Bluetooth</a></h3>
                                    </div>
                                    <div class="product_ratings">
                                        <ul>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                        </ul>
                                    </div>
                                    <div class="price_box">
                                        <span class="regular_price">$170.00</span> 
                                    </div>
                                </div>
                            </div>
                            <div class="single_product_items">
                                <div class="product_thumb">
                                    <a href="{{route('web.product.show', $product->slug)}}"><img src="assets/img/product/product12.jpg" alt=""></a>
                                </div>
                                <div class="product_content">
                                    <div class="product_name">
                                        <h3><a href="{{route('web.product.show', $product->slug)}}">Kodak PIXPRO Astro Zoom AZ421 16 MP</a></h3>
                                    </div>
                                    <div class="product_ratings">
                                        <ul>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                        </ul>
                                    </div>
                                    <div class="price_box">
                                        <span class="regular_price">$160.00</span> 
                                    </div>
                                </div>
                            </div>
                            <div class="single_product_items">
                                <div class="product_thumb">
                                    <a href="{{route('web.product.show', $product->slug)}}"><img src="assets/img/product/product13.jpg" alt=""></a>
                                </div>
                                <div class="product_content">
                                    <div class="product_name">
                                        <h3><a href="{{route('web.product.show', $product->slug)}}">1. New and sale new  badge product </a></h3>
                                    </div>
                                    <div class="product_ratings">
                                        <ul>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                        </ul>
                                    </div>
                                    <div class="price_box">
                                        <span class="current_price">$180.00</span>
                                        <span class="old_price">$420.00</span>  
                                    </div>
                                </div>
                            </div>
                            <div class="single_product_items">
                                <div class="product_thumb">
                                    <a href="{{route('web.product.show', $product->slug)}}"><img src="assets/img/product/product14.jpg" alt=""></a>
                                </div>
                                <div class="product_content">
                                    <div class="product_name">
                                        <h3><a href="{{route('web.product.show', $product->slug)}}">Accusantium dolorem  Security Camera</a></h3>
                                    </div>
                                    <div class="product_ratings">
                                        <ul>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                        </ul>
                                    </div>
                                    <div class="price_box">
                                        <span class="current_price">$180.00</span>
                                        <span class="old_price">$420.00</span>  
                                    </div>
                                </div>
                            </div>
                            <div class="single_product_items">
                                <div class="product_thumb">
                                    <a href="{{route('web.product.show', $product->slug)}}"><img src="assets/img/product/product15.jpg" alt=""></a>
                                </div>
                                <div class="product_content">
                                    <div class="product_name">
                                        <h3><a href="{{route('web.product.show', $product->slug)}}">1. New and sale new  badge product </a></h3>
                                    </div>
                                    <div class="product_ratings">
                                        <ul>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                        </ul>
                                    </div>
                                    <div class="price_box">
                                        <span class="current_price">$180.00</span>
                                        <span class="old_price">$420.00</span>  
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                   <!--bestSeller product end-->
                </div>
            </div>
        </div>
    </section>
    <!--custom product end-->
    
    <!--call to action start-->
    <section class="call_to_action">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="call_action_inner">
                        <div class="call_text">
                            <h3>We Have <span>Recommendations</span>  for You</h3>
                            <p>Take 30% off when you spend $150 or more with code Autima11</p>
                        </div>
                        <div class="discover_now">
                            <a href="#">discover now</a>
                        </div>
                        <div class="link_follow">
                            <ul>
                                <li><a href="https://www.facebook.com/JMInterpartCo/?ref=br_rs"><i class="ion-social-facebook"></i></a></li>
                                <li><a href="https://twitter.com/jminterpartco?lang=es"><i class="ion-social-twitter"></i></a></li>
                                <li><a href="https://www.instagram.com/jminterpart/"><i class="ion-social-instagram"></i></a></li>
                                <!--<li><a href="#"><i class="ion-social-youtube"></i></a></li>-->
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--call to action end-->
    
@endsection