@extends('layouts.app')

@section('content')
<main role="main" class="col">
    <div class="row pt-5 justify-content-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-body py-5">
                    <h3>{{ __('Verify Your Email Address') }}</h3>

                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            {{ __('A fresh verification link has been sent to your email address.') }}
                        </div>
                    @endif
                    
                    {{ __('Before proceeding, please check your email for a verification link.') }}
                    {{ __('If you did not receive the email') }}, <a href="{{ route('verification.resend') }}">{{ __('click here to request another') }}</a>.
                </div>
            </div>
        </div>
    </div>
</main>
@endsection

