@extends('layouts.web')

@section('content')
    <!--shop  area start-->
    <div class="shop_area shop_reverse">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-12">
                   <!--sidebar widget start-->
                    <aside class="sidebar_widget">
                        <div class="widget_inner">
                            <!--
                            <div class="widget_list widget_filter">
                                <h2>Filter by price</h2>
                                <form action="{{url()->full()}}"> 
                                    <div id="slider-range"></div>   
                                    <button type="submit">Filter</button>
                                    <input type="text" name="amount" id="amount" />   

                                </form> 
                            </div>
                            -->
                            @foreach ($taxonomies as $taxonomy)
                                <div class="widget_list widget_categories">
                                    <h2>{{$taxonomy->name}}</h2>
                                    <ul>
                                        @foreach ($taxonomy->rootLevelTaxons() as $taxon)
                                            <li>
                                                <input type="checkbox">
                                                <a href="{{route('web.product.category', $taxon->slug)}}">
                                                    {{$taxon->name}} ({{$taxon->products->count()}})
                                                </a> 
                                                <span class="checkmark"></span>
                                            </li>    
                                        @endforeach
                                        
                                    </ul>
                                </div>  
                            @endforeach
                            
                        </div>
                        <div class="shop_sidebar_banner">
                            <a href="#"><img src="assets/img/bg/banner9.jpg" alt=""></a>
                        </div>
                    </aside>
                    <!--sidebar widget end-->
                </div>
                <div class="col-lg-9 col-md-12">
                    <!--shop wrapper start-->
                    <!--shop toolbar start
                    <div class="shop_banner">
                        <img src="assets/img/bg/banner8.jpg" alt="">
                    </div>
                    <div class="shop_title">
                        <h1>shop</h1>
                    </div>
                    <div class="shop_toolbar_wrapper">
                        <div class="shop_toolbar_btn">

                            <button data-role="grid_3" type="button" class="active btn-grid-3" data-toggle="tooltip" title="3"></button>

                            <button data-role="grid_4" type="button"  class=" btn-grid-4" data-toggle="tooltip" title="4"></button>

                            <button data-role="grid_list" type="button"  class="btn-list" data-toggle="tooltip" title="List"></button>
                        </div>
                        <div class=" niceselect_option">

                            <form class="select_option" action="#">
                                <select name="orderby" id="short">

                                    <option selected value="1">Sort by average rating</option>
                                    <option  value="2">Sort by popularity</option>
                                    <option value="3">Sort by newness</option>
                                    <option value="4">Sort by price: low to high</option>
                                    <option value="5">Sort by price: high to low</option>
                                    <option value="6">Product Name: Z</option>
                                </select>
                            </form>


                        </div>
                        <div class="page_amount">
                            <p>Showing 1–9 of 21 results</p>
                        </div>
                    </div>
                    shop toolbar end-->

                     <div class="row shop_wrapper">
                        @foreach ($products as $product)
                            <div class="col-lg-4 col-md-4 col-12 ">
                                <div class="single_product">
                                    <div class="product_name grid_name">
                                        <h3><a href="{{route('web.product.show', $product->slug)}}">{{$product->name}}</a></h3>
                                        <p class="manufacture_product"><a href="#">{{$product->taxons->first()->name}}</a></p>
                                    </div>
                                    <div class="product_thumb">
                                        <a class="primary_img" href="{{route('web.product.show', $product->slug)}}"><img src="{{$product->getThumbnailUrl()}}" alt=""></a>
                                        <!--
                                        <div class="label_product">
                                            <span class="label_sale">-47%</span>
                                        </div>
                                        <div class="action_links">
                                            <ul>
                                                <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box"  title="quick view"> <span class="lnr lnr-magnifier"></span></a></li>
                                                <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><span class="lnr lnr-heart"></span></a></li>
                                                <li class="compare"><a href="#" title="compare"><span class="lnr lnr-sync"></span></a></li>
                                            </ul>
                                        </div>
                                    -->
                                    </div>
                                    <div class="product_content grid_content">
                                        <div class="content_inner">
                                            <div class="product_ratings">
                                                <ul>
                                                    <li><a href="#"><i class="ion-star"></i></a></li>
                                                    <li><a href="#"><i class="ion-star"></i></a></li>
                                                    <li><a href="#"><i class="ion-star"></i></a></li>
                                                    <li><a href="#"><i class="ion-star"></i></a></li>
                                                    <li><a href="#"><i class="ion-star"></i></a></li>
                                                </ul>
                                            </div>
                                            <div class="product_footer d-flex align-items-center">
                                                <div class="price_box">
                                                    <span class="current_price">${{$product->getPrice()}}</span>
                                                    <!--
                                                    <span class="old_price">$3200.00</span>   
                                                    -->
                                                </div>
                                                <div class="add_to_cart">
                                                    <a href="cart.html" title="add to cart"><span class="lnr lnr-cart"></span></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="product_content list_content">
                                        <div class="left_caption">
                                        <div class="product_name">
                                                <h3><a href="{{route('web.product.show', $product->slug)}}">Cas Meque Metus Shoes Core i7 3.4GHz, 16GB DDR3</a></h3>
                                            </div>
                                            <div class="product_ratings">
                                                <ul>
                                                    <li><a href="#"><i class="ion-star"></i></a></li>
                                                    <li><a href="#"><i class="ion-star"></i></a></li>
                                                    <li><a href="#"><i class="ion-star"></i></a></li>
                                                    <li><a href="#"><i class="ion-star"></i></a></li>
                                                    <li><a href="#"><i class="ion-star"></i></a></li>
                                                </ul>
                                            </div>
                                            
                                            <div class="product_desc">
                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nobis ad, iure incidunt. Ab consequatur temporibus non eveniet inventore doloremque necessitatibus sed, ducimus quisquam, ad asperiores  </p>
                                            </div>
                                        </div>
                                        <div class="right_caption">
                                        <div class="text_available">
                                            <p>availabe: <span>99 in stock</span></p>
                                        </div>
                                        <div class="price_box">
                                                <span class="current_price">$160.00</span>
                                                <span class="old_price">$420.00</span>
                                            </div>
                                            <div class="cart_links_btn">
                                                <a href="#" title="add to cart">add to cart</a>
                                            </div>
                                            <div class="action_links_btn">
                                                <ul>
                                                    <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box"  title="quick view"> <span class="lnr lnr-magnifier"></span></a></li>
                                                    <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><span class="lnr lnr-heart"></span></a></li>
                                                    <li class="compare"><a href="#" title="compare"><span class="lnr lnr-sync"></span></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>


                    {{ $products->links() }}
                    <!--shop toolbar end-->
                    <!--shop wrapper end-->
                </div>
            </div>
        </div>
    </div>
    <!--shop  area end-->
    
     <!--call to action start-->
    <section class="call_to_action">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="call_action_inner">
                        <div class="call_text">
                            <h3>We Have <span>Recommendations</span>  for You</h3>
                            <p>Take 30% off when you spend $150 or more with code Autima11</p>
                        </div>
                        <div class="discover_now">
                            <a href="#">discover now</a>
                        </div>
                        <div class="link_follow">
                            <ul>
                                <li><a href="#"><i class="ion-social-facebook"></i></a></li>
                                <li><a href="#"><i class="ion-social-twitter"></i></a></li>
                                <li><a href="#"><i class="ion-social-googleplus"></i></a></li>
                                <li><a href="#"><i class="ion-social-youtube"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--call to action end-->
@endsection