<div class="mini_cart_wrapper">
    <a href="#"><span class="lnr lnr-cart"></span>My Cart </a>
    <span class="cart_quantity">{{Cart::itemCount()}}</span>
    <!--mini cart-->
     <div class="mini_cart">
        @forelse (Cart::getItems() as $item)
            <div class="cart_item">
                <div class="cart_img">
                    <a href="#"><img src="{{$item->getThumbnailUrl()}}" alt=""></a>
                </div>
                <div class="cart_info">
                    <a href="#">{{$item->name}}</a>

                    <span class="quantity">Qty: {{$item->getQuantity()}}</span>
                    <span class="price_cart">${{$item->getTotalFormat()}}</span>
                </div>
                <div class="cart_remove">
                    <a href="#"><i class="ion-android-close"></i></a>
                </div>
            </div>
        @empty
            <div class="cart_empty">
                {{__('Cart Empty')}}
            </div>
        @endforelse
        <div class="mini_cart_table">
            <div class="cart_total">
                <span>{{__('Subtotal')}}:</span>
                <span class="price">${{Cart::getSubtotal()}}</span>
            </div>
            <div class="cart_total mt-10">
                <span>{{__('Total')}}:</span>
                <span class="price">${{Cart::getTotal()}}</span>
            </div>
        </div>
        @if (Cart::isNotEmpty())
            <div class="mini_cart_footer">
            <div class="cart_button">
                    <a href="{{route('web.cart.show')}}">{{__('View cart')}}</a>
                </div>
                <div class="cart_button">
                    <a href="checkout.html">{{__('Checkout')}}</a>
                </div>

            </div>
        @endif

    </div>
    <!--mini cart end-->
</div>