<nav class="col-md-2 d-none d-md-block bg-light sidebar">
    <div class="sidebar-sticky">
        <ul class="nav flex-column">
            <li class="nav-item">
                <a class="nav-link active" href="#">
                    <span data-feather="home"></span>
                    {{__('Dashboard')}} <span class="sr-only">(current)</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">
                    <span data-feather="file"></span>
                    {{__('Orders')}}
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{route('product.index')}}">
                    <span data-feather="shopping-cart"></span>
                    {{__('Products')}}
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{route('customer.index')}}">
                    <span data-feather="users"></span>
                    {{__('Customers')}}
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{route('taxonomy.index')}}">
                    <span data-feather="bookmark"></span>
                    {{__('Categorization')}}
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{route('user.index')}}">
                    <span data-feather="gitlab"></span>
                    {{__('Users')}}
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{route('role.index')}}">
                    <span data-feather="shield"></span>
                    {{__('Permissions')}}
                </a>
            </li>
        </ul>
    </div>
</nav>