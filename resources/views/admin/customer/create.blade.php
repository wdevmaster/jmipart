@extends('layouts.app')

@section('title')
    {{ __('Create new customer') }}
@stop

@section('content')
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">{{__('Enter Customer  Details')}}</h1>
    </div>

    <div class="card">
        <div class="card-body">
            {!! Form::model($customer, ['route' => 'customer.store', 'autocomplete' => 'off']) !!}

                @include('admin.customer._form')
            
                <hr>
                <div class="form-group text-right">
                    <button class="btn btn-success">{{ __('Create customer') }}</button>
                    <a href="#" onclick="history.back();" class="btn btn-link text-muted">{{ __('Cancel') }}</a>
                </div>
            {!! Form::close() !!}
        </div>
      </div>
</main>
@endsection

@section('sidebar')
    @parent
    @include('elements._sidebar')
@endsection