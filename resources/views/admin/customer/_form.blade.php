<div id="app">
<div class="form-group row">
    <label class="form-control-label col-md-2">{{ __('Customer type') }}</label>
    <div class="col-md-10">
        @foreach($types as $key => $value)
            <label class="radio-inline" for="type_{{ $key }}">
                {{  Form::radio(
                        'type', 
                        $key, 
                        $customer->type->value() == $key, 
                        [
                            'id' => "type_$key", 
                            'class' => "".($errors->has('type') ? ' is-invalid': ''),
                            'v-model' =>  'customerType'
                        ]
                    ) 
                }}
                {{ $value }}
                &nbsp;
            </label>
        @endforeach

        @if ($errors->has('type'))
            <div class="invalid-feedback d-block error-type">
                {{ $errors->first('type') }}
            </div>
        @endif
    </div>
</div>
    
<div class="row">
    
        <div class="col-md-6">
            <div class="form-group">
                {{ Form::text('firstname', null, [
                        'class' => 'form-control form-control-lg'.($errors->has('firstname') ? ' is-invalid': ''),
                        'placeholder' => __('First name')
                    ])
                }}
    
                @if ($errors->has('firstname'))
                    <div class="invalid-feedback d-block error-firstname">
                        {{ $errors->first('firstname') }}
                    </div>
                @endif
            </div>
        </div>
    
        <div class="col-md-6">
            <div class="form-group">
                {{ Form::text('lastname', null, [
                        'class' => 'form-control form-control-lg'.($errors->has('lastname') ? ' is-invalid': ''),
                        'placeholder' => __('Last name')
                    ])
                }}
    
                @if ($errors->has('lastname'))
                    <div class="invalid-feedback d-block error-lastname">
                        {{ $errors->first('lastname') }}
                    </div>
                @endif
            </div>
        </div>
            
</div>
<div class="row">

    <div class="col-md-6">
       <div class="form-group">
           {{ Form::text('email', old('email'), [
                   'class' => 'form-control'.($errors->has('email') ? ' is-invalid': ''),
                   'placeholder' => __('E-mail address')
               ])
           }}
    
           @if ($errors->has('email'))
               <div class="invalid-feedback d-block error-email">
                   {{ $errors->first('email') }}
               </div>
           @endif
       </div>
    </div>
    
    <div class="col-md-6">
       <div class="form-group">
           {{ Form::password('password', [
                   'class' => 'form-control'.($errors->has('password') ? ' is-invalid': ''),
                   'placeholder' => !isset($customer) ? 
                   __('Type new password if you want to change it') :
                   __('Enter password')
               ])
           }}
    
           @if ($errors->has('password'))
               <div class="invalid-feedback d-block error-password">
                   {{ $errors->first('password') }}
               </div>
           @endif
       </div>
    </div>    
</div>
<div id="customer-organization" v-show="customerType == 'organization'">
    @include('admin.customer._organization')
</div>
</div>

@section('scripts')
    <script src="https://cdn.jsdelivr.net/npm/vue@2.6.10/dist/vue.js"></script>
    <script>
        new Vue({
            el: '#app',
            data: {
                customerType: '{{ old('type') ?: $customer->type->value() }}'
            }
        });
    </script>
@stop