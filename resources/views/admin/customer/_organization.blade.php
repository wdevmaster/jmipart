<hr>

<div class="form-group">
    <div class="input-group">
        {{ Form::text(
                'company_name', 
                old('company_name'), 
                [
                    'class' => 'form-control form-control-lg'.($errors->has('company_name') ? ' is-invalid': ''), 
                    'placeholder' => __('Company name')
                ]
            ) 
        }}
    </div>
    @if ($errors->has('company_name'))
        <div class="invalid-feedback d-block error-company_name">
            {{ $errors->first('company_name') }}
        </div>
    @endif
</div>

<div class="form-group">
    <div class="input-group">
        {{ Form::text(
                'tax_nr', 
                old('tax_nr'), 
                [
                    'class' => 'form-control'.($errors->has('tax_nr') ? ' is-invalid': ''), 
                    'placeholder' => __('Tax no.')
                ]
            ) 
        }}
    </div>
    @if ($errors->has('tax_nr'))
        <div class="invalid-feedback d-block error-tax_nr">
            {{ $errors->first('tax_nr') }}
        </div>
    @endif
</div>

<div class="form-group">
    <div class="input-group">
        {{ Form::text(
                'registration_nr', 
                old('registration_nr'), 
                [
                    'class' => 'form-control'.($errors->has('registration') ? ' is-invalid': ''), 
                    'placeholder' => __('Reg. no.')
                ]
            ) 
        }}
    </div>
    @if ($errors->has('registration_nr'))
        <div class="invalid-feedback d-block error-registration_nr">
            {{ $errors->first('registration_nr') }}
        </div>
    @endif
</div>