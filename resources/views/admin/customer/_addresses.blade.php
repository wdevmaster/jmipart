

<div class="card mt-4 card-address">
    <div class="card-header">
        {{ __('Addresses') }}
        <div class="card-actionbar">
            @can('addesses.create')
                <a href="{{ route('address.create') }}?for=customer&forId={{ $customer->id }}" class="btn btn-sm btn-outline-success float-right">
                    {{ __('New Address') }}
                </a>
            @endcan
        </div>
    </div>
    <div class="card-block">
        <table class="table">
            <thead>
                <tr>
                    <th>{{ __('Name') }}</th>
                    <th>{{ __('Type') }}</th>
                    <th>{{ __('Address') }}</th>
                    <th>{{ __('Country') }}</th>
                    <th style="width: 15%">&nbsp;</th>
                </tr>
            </thead>
            <tbody>
            @forelse($customer->addresses as $address)
                <tr class="row-{{$address->id}}">
                    <td>{{ $address->name }}</td>
                    <td>{{ $address->type->label() }}</td>
                    <td>
                        @component('widgets.address.short_table_row', ['address' => $address])
                        @endcomponent
                    </td>
                    <td>{{ $address->country->name }}</td>
                    <td>
                        @can('address.delete')
                            <button 
                                type="button" 
                                data-id="{{ $address->id }}"
                                data-type="address"
                                class="btn btn-danger btn-delete">
                                    {{__('Remove')}}
                            </button>
                        @endcan
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="5" class="text-center">{{ __('Customer has no addresses yet') }}</td>
                </tr>
            @endforelse
            </tbody>
        </table>
    </div>
</div>