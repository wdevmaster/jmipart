@extends('layouts.app')

@section('title')
    {{ __('Viewing') }} {{ $customer->name }}
@stop

@section('content')
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">{{ __('Viewing') }}</h1>
        <div class="btn-toolbar mb-2 mb-md-0">
            <div class="btn-group mr-2">
                <a href="{{route('customer.index')}}" class="btn btn-sm btn-outline-secondary">
                    {{ __('Go back') }}
                </a>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            @component('widgets.card_with_icon', [
                'iconSlot'  => '<i class="fas fa-user"></i>'
            ])
                {{ $customer->name }}
                @if (!$customer->is_active)
                    <small>
                        <span class="badge badge-{{$customer->is_active ? 'success' : 'warning'}}">
                            {{ __('Inactive') }}
                        </span>
                    </small>
                @endif
                @slot('subtitle')
                    {{ $customer->type->label() }}
                @endslot
            @endcomponent
        </div>
        <div class="col-md-6">
            @component('widgets.card_with_icon', [
                'iconSlot'  => '<i class="fas fa-history"></i>'
            ])
                {{ __('Last purchase') }}
                <span title="{{__('This is fake right now')}}">
                    {{ $customer->updated_at->diffForHumans() }}
                </span>

                @slot('subtitle')
                    {{ __('Customer since') }}
                    {{ $customer->created_at->format(__('Y-m-d H:i')) }}
                @endslot
            @endcomponent
        </div>
    </div>

    @include('admin.customer._addresses')
    
    <div class="card mt-4">
        <div class="card-body">
            <div class="d-flex justify-content-between">
                @can('customer.edit')
                    <a 
                        href="{{ route('customer.edit', $customer->id) }}"
                        class="btn btn-primary btn-edit">
                            {{__('Edit customer')}}
                    </a>
                @endcan

                @can('customer.delete')
                    <button 
                        type="button" 
                        data-id="{{ $customer->id }}"
                        data-type="customer"
                        class="btn btn-danger btn-delete">
                            {{__('Delete customer')}}
                    </button>
                @endcan
            </div>
        </div>
    </div>
</main>
@endsection

@section('sidebar')
    @parent
    @include('elements._sidebar')
@endsection

@section('scripts')
    @parent
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
    <script>
        $('.btn-delete').click(function(e) {

            if ($(this).data('type') == 'customer'){
                var redirectUrl = "{{route('customer.index')}}"
                var deleteUrl = "{{ route('customer.delete') }}"
            } else {
                var redirectUrl = "{{route('customer.show', $customer->id)}}"
                var deleteUrl = "{{ route('address.delete') }}"
            }

            let id = $(this).data('id')

            $('.row-'+id).toggleClass('table-danger')

            Swal.fire({
                title: "{{__('Are you sure?')}}",
                html: "{{__('You won\'t be able to revert this!')}}",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#d33',
                confirmButtonText: "{{__('Yes, delete it!')}}",
                cancelButtonText: "{{__('No, cancel!')}}",
                showLoaderOnConfirm: true,
                preConfirm: () => {
                    return fetch(deleteUrl, {
                        headers: {
                            'Accept': 'application/json',
                            'Content-Type': 'application/json'
                        },
                        method: "DELETE",
                        body: JSON.stringify({ 
                            '_token': '{{csrf_token()}}',
                            'id': id
                        })
                    })
                },
                allowOutsideClick: () => !Swal.isLoading()
            }).then((result) => {
                if (result.value) {
                    Swal.fire({
                        title: "{{__('Deleted!')}}",
                        text: "{{__('Your file has been deleted.')}}",
                        type:'success',
                        confirmButtonText: "{{__('Ok')}}",
                    }).then(() => {
                        window.location.href = redirectUrl
                    })
                }
                $('.row-'+id).removeClass('table-danger')
            })
        })
    </script>
@endsection