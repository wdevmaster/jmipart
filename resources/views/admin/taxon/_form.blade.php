<div class="form-group">
    <div class="input-group">
        {{ Form::text(
                'name', 
                old('name'), 
                [
                    'class' => 'form-control form-control-lg'.($errors->has('name') ? ' is-invalid': ''), 
                    'placeholder' => __('Name of the Category Tree')
                ]
            ) 
        }}
    </div>
    @if ($errors->has('name'))
        <div class="invalid-feedback d-block error-name">
            {{ $errors->first('name') }}
        </div>
    @endif
</div>

<div class="form-group">
   <div class="input-group">
       {{ Form::text(
               'slug', 
               old('slug'), 
               [
                   'class' => 'form-control form-control-sm'.($errors->has('slug') ? ' is-invalid': ''), 
                   'placeholder' => __('URL (leave empty to auto-generate from name)')
               ]
           ) 
       }}
   </div>
   @if ($errors->has('slug'))
       <div class="invalid-feedback d-block error-slug">
           {{ $errors->first('slug') }}
       </div>
   @endif
</div>

<hr>

<label class="form-control-label">{{ __('Parent') }}</label>
<div class="form-group">
    {{  Form::select('parent_id', $taxons, null, [
            'class' => 'form-control form-control-sm' . ($errors->has('parent_id') ? ' is-invalid': ''),
            'placeholder' => __('No parent')
        ])
    }}
    
    @if ($errors->has('parent_id'))
        <div class="invalid-feedback d-block error-parent_id">
            {{ $errors->first('parent_id') }}
        </div>
    @endif
</div>

<label class="form-control-label">{{ __('Priority') }}</label>
<div class="form-group">
    {{  Form::text('priority', null, [
            'class' => 'form-control form-control-sm' . ($errors->has('priority') ? ' is-invalid': '')
        ])
    }}
    
    @if ($errors->has('priority'))
        <div class="invalid-feedback d-block error-priority">
            {{ $errors->first('priority') }}
        </div>
    @endif
</div>

{{Form::file('image')}}

