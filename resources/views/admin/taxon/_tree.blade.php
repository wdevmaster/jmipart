

<div class="card mt-4 card-taxon">
    <div class="card-header">
        <div class="card-actionbar">
            @can('taxon.create')
                <a href="{{ route('taxon.create', $taxonomy) }}" class="btn btn-sm btn-outline-success float-right">
                    {{ __('Add :category', ['category' => str_singular($taxonomy->name)]) }}
                </a>
            @endcan
        </div>
    </div>
    <div class="card-block p-2">

        @component('widgets.table_tree_taxon', [
            'taxonomy' => $taxonomy,
            'taxons' => $taxons
        ])
        @endcomponent
        
    </div>
</div>