<div class="form-group">
    <div class="input-group">
        {{ Form::text(
                'name', 
                old('name'), 
                [
                    'class' => 'form-control'.($errors->has('name') ? ' is-invalid': ''), 
                    'placeholder' => __('Name of the role')
                ]
            ) 
        }}
    </div>
    @if ($errors->has('name'))
        <div class="invalid-feedback d-block error-name">
            {{ $errors->first('name') }}
        </div>
    @endif
</div>

<h3>{{ __('Role permissions') }}</h3>

<div class="form-group row">

    @foreach($permissions as $permission)
        <div class="col-6 col-sm-2 @unless(($loop->index + 5) % 5)offset-sm-1 @endunless">
            {{  Form::checkbox(
                    "permissions[{$permission->slug}][slug]", 
                    $permission->slug, 
                    isset($role) ? $role->hasPermissionTo($permission) : false, 
                    [
                        'class' => 'form-check-input',
                        'id' => $permission->slug
                    ]
                ) 
            }}
            <label class="form-check-label" for="{{ $permission->slug }}">
                {{ $permission->name }}
            </label>
        </div>
    @endforeach
</div>