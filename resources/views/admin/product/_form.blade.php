<div class="row mb-2">
    <div class="col-md-8">
        @include('admin.product.card._identification')

        @include('admin.product.card._information')

        @include('admin.product.card._create_images')
    </div>
    <div class="col-md-4">
        @include('admin.product.card._status')

        @include('admin.product.card._price')

        @include('admin.product.card._categorization')
    </div>
</div>