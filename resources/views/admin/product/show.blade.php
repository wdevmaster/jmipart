@extends('layouts.app')

@section('title')
    {{ __('Viewing') }} {{ $taxonomy->name }}
@stop

@section('content')
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">@yield('title')</h1>
        <div class="btn-toolbar mb-2 mb-md-0">
            <div class="btn-group mr-2">
                <a href="{{route('taxonomy.index')}}" class="btn btn-sm btn-outline-secondary">
                    {{ __('Go back') }}
                </a>
            </div>
        </div>
    </div> 

    @include('admin.taxon._tree', ['taxons' => $taxonomy->rootLevelTaxons()])
    
    <div class="card mt-4">
        <div class="card-body">
            <div class="d-flex justify-content-between">
                @can('taxonomy.edit')
                    <a 
                        href="{{ route('taxonomy.edit', $taxonomy->id) }}"
                        class="btn btn-primary btn-edit">
                            {{__('Edit Category Tree')}}
                    </a>
                @endcan

                @can('taxonomy.delete')
                    <button 
                        type="button" 
                        data-id="{{ $taxonomy->id }}"
                        data-type="taxonomy"
                        class="btn btn-danger btn-delete">
                            {{__('Delete Category Tree')}}
                    </button>
                @endcan
            </div>
        </div>
    </div>
</main>
@endsection

@section('sidebar')
    @parent
    @include('elements._sidebar')
@endsection

@section('styles')
    @parent
    <style>
        .table tr {
            cursor: pointer;
        }

        .table tr img{
            width: 64px;
            height: 64px;
        }
    </style>
@endsection

@section('scripts')
    @parent
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
    <script>
        $('.treeTaxon').collapse()

        $('.btn-delete').click(function(e) {

            if ($(this).data('type') == 'taxonomy'){
                var redirectUrl = "{{route('taxonomy.index')}}"
                var deleteUrl = "{{ route('taxonomy.delete') }}"
            } else {
                var redirectUrl = "{{route('taxonomy.show', $taxonomy->id)}}"
                var deleteUrl = "{{ route('taxon.delete', $taxonomy) }}"
            }

            let id = $(this).data('id')

            $('.row-'+id).toggleClass('table-danger')

            Swal.fire({
                title: "{{__('Are you sure?')}}",
                html: "{{__('You won\'t be able to revert this!')}}",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#d33',
                confirmButtonText: "{{__('Yes, delete it!')}}",
                cancelButtonText: "{{__('No, cancel!')}}",
                showLoaderOnConfirm: true,
                preConfirm: () => {
                    return fetch(deleteUrl, {
                        headers: {
                            'Accept': 'application/json',
                            'Content-Type': 'application/json'
                        },
                        method: "DELETE",
                        body: JSON.stringify({ 
                            '_token': '{{csrf_token()}}',
                            'id': id
                        })
                    })
                },
                allowOutsideClick: () => !Swal.isLoading()
            }).then((result) => {
                if (result.value) {
                    Swal.fire({
                        title: "{{__('Deleted!')}}",
                        text: "{{__('Your file has been deleted.')}}",
                        type:'success',
                        confirmButtonText: "{{__('Ok')}}",
                    }).then(() => {
                        window.location.href = redirectUrl
                    })
                }
                $('.row-'+id).removeClass('table-danger')
            })
        })
    </script>
@endsection