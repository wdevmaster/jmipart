<div class="card mt-3">
    <div class="card-header">
        {{ __('Images') }}
    </div>
    <div class="card-body px-3">

        <span class="btn btn-primary btn-file">
            Añadir fotos{{ Form::file('images[]', ['multiple', 'class' => 'form-control-file', 'id' => 'images']) }}
        </span>

        @if($product->hasImage())
            <input type="hidden" name="delete-img" value="{{old('delete-img')}}">
        @endif

        @php
            $oldDeleteImg = old('delete-img') ? explode(',', old('delete-img')) : [];
        @endphp

        <div id="result" >
            @if($product->hasImage())
                @foreach($product->getMedia() as $media)

                    @php
                        $boo = false;
                        if (old('delete-img'))
                            $boo = in_array($media->id, $oldDeleteImg);
                    @endphp

                    @if(!$boo)
                        <div class="thumbnail">
                            <img data-name="{{$media->name}}" src="{{$media->getUrl('thumbnail')}}" alt="{{$media->name}}">
                            <button data-media="{{$media->id}}" type="button" class="btn btn-sm btn-danger delete-img">
                                x
                            </button>
                        </div>
                    @endif
                @endforeach
            @endif
        </div>
    </div>
</div>

@section('styles')
    @parent
    <style>
        #result {
            display: flex;
            flex-wrap: wrap;
            min-height: 100px;
            justify-content: space-around;
            background-color: rgba(0,0,0,.03);
            border: 2px dashed rgba(0,0,0,.125);
            position: relative;
        }

        #result::after {
            content: "{{__('No images')}}";
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            color: rgba(0,0,0,.225);
            font-size: 18px;
            font-weight: 900;
        }
        
        .thumbnail {
            position: relative;
            width: 200px;
            margin: 10px;
        }

        .thumbnail img {
            width: 100%;
        }

        .thumbnail button,
        .thumbnail button:hover,
        .thumbnail button:active,
        .thumbnail button:focus,
        .thumbnail button:visited{
            position: absolute;
            top: 4px;
            right: 4px;
        }

        .btn-file {
            position: relative;
            overflow: hidden;
        }

        .btn-file input[type=file] {
            position: absolute;
            top: 0;
            right: 0;
            min-width: 100%;
            min-height: 100%;
            font-size: 100px;
            text-align: right;
            filter: alpha(opacity=0);
            opacity: 0;
            outline: none;
            background: white;
            cursor: inherit;
            display: block;
        }

        img.blur {
            -webkit-filter: blur(5px); /* Safari 6.0 - 9.0 */
            filter: blur(5px);
        }
    </style>
@endsection

@section('scripts')
@parent
<script>
$(function () {
    $('#images').change(function () {
        var files = document.getElementById("images").files,
            formData = null;

        if (window.FormData)
            formData = new FormData();
        
        for (var i = 0; i < files.length; i++) {
            var file = files[i];

            if (!file.type.match('image')) continue;
            
            if (window.FileReader) {
                var reader = new FileReader();
                reader.onloadend = function(e) {
                    fileRenderDom(e.target.result, file.fileName);
                }
                reader.readAsDataURL(file);
            }
        }
    })

    function fileRenderDom(result) {
        var div = document.createElement("div"),
            output = document.getElementById("result")

        div.className  = "thumbnail fileRender"
        div.innerHTML = '<img src="'+ result +'" class="" alt="">'
        output.insertBefore(div, null)
        
    }

    $('#result').off()
    $('#result').on('click', '.delete-img', function() {
        var inputDelete = $('input[name=delete-img]')

        if (inputDelete.val())
            arry = inputDelete.val().split(',')
        else 
            arry = []

        $(this).parent().remove()
        arry.push($(this).data('media'))
        inputDelete.val(arry.toString())
    })
})
</script>
@endsection
        