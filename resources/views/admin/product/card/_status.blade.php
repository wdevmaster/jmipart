
<div class="card">
        <div class="card-header">
            {{ __('Product status') }}

            @component('widgets.badge_status_product', [
                'type' => $product->state->value()
            ])
                {{ $product->state->label() }}
            @endcomponent
        </div>
        <div class="card-body px-3 py-1">

            <label class="form-control-label">{{ __('Condition') }}</label>
            <div class="form-group">
                {{  Form::select('condition', $conditions, old('condition'), [
                        'class' => 'form-control'.($errors->has('condition') ? ' is-invalid': '')
                    ])
                }} 

                @if ($errors->has('condition'))
                    <div class="invalid-feedback d-block error-condition">
                        {{ $errors->first('condition') }}
                    </div>
                @endif
            </div>

            <label class="form-control-label">{{ __('Availability') }}</label>
            <div class="form-group">
                {{  Form::select('availability', $availability, old('availability'), [
                        'class' => 'form-control'.($errors->has('availability') ? ' is-invalid': '')
                    ])
                }} 

                @if ($errors->has('availability'))
                    <div class="invalid-feedback d-block error-availability">
                        {{ $errors->first('availability') }}
                    </div>
                @endif
            </div>

            <label class="form-control-label">{{ __('Stock') }}</label>
            <div class="form-group">
                {{  Form::number('stock', null, [
                        'class' => 'form-control' . ($errors->has('stock') ? ' is-invalid' : ''),
                        'placeholder' => __('Product Stock Count')
                    ])
                }}

                @if ($errors->has('stock'))
                    <div class="invalid-feedback d-block error-stock">
                        {{ $errors->first('stock') }}
                    </div>
                @endif
            </div>


            
        </div>
    </div>
        