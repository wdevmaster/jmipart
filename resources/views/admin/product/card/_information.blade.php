
<div class="card mt-3">
    <div class="card-header">
        {{ __('Product information') }}
    </div>
    <div class="card-body px-3">

        <div class="form-group mb-0">
            <div class="input-group">
                {{  Form::textarea('description', null,
                        [
                            'class' => 'form-control' . ($errors->has('description') ? ' is-invalid' : ''),
                            'placeholder' => __('Type or copy/paste product description here')
                        ]
                    ) 
                }}
            </div>
            @if ($errors->has('description'))
                <div class="invalid-feedback d-block error-description">
                    {{ $errors->first('description') }}
                </div>
            @endif
        </div>
    </div>
</div>

<hr>
        