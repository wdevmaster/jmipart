
<div class="card mt-3">
    <div class="card-header">
        {{ __('Categorization') }}
    </div>
    <div class="card-body">
        
        <input type="hidden" name="taxons" id="taxons" value="{{old('taxons')}}">

        @foreach ($taxonomies as $taxonomy)
            <h5 class="title-tree"><b>{{$taxonomy->name}}</b></h5>
            <div id="{{$taxonomy->slug}}" class="tree">
                @component('widgets.checkbox_tree', [
                    'taxons' => $taxonomy->rootLevelTaxons()
                ])
                @endcomponent
            </div>
        @endforeach
    </div>
</div>

@section('styles')
    @parent
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jstree/3.2.1/themes/default/style.min.css" />
    <style>
        .title-tree {
            margin: 0;
        }
        .jstree-default .jstree-themeicon{
            display: none!important;
        }
    </style>
@endsection

@section('scripts')
    @parent
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jstree/3.2.1/jstree.min.js"></script>
    <script>
        $(function () {

            $('.tree').jstree({
                core: {
                    animation: 60,
                    themes: {
                        icons: false,
                        responsive: false
                    }
                },
                checkbox: {
                    keep_selected_style: false
                },
                plugins: ['checkbox']
            })

            setTree()

            $('.tree').on("changed.jstree", function (e, data) {
                if (inpTaxon = $('#taxons').val()) {
                    var arry = inpTaxon.split(',')
                    arry = arry.concat(data.selected).unique()
                    $('#taxons').val(treeIsSelected(arry).toString())
                } else
                    $('#taxons').val(data.selected.toString())
            });
        });

        function setTree() {
            var strNode = "{{old('taxons') ?: $product->getIdStr() }}"
            if (strNode.length > 0){
                $.each($('.tree'), function (k, tree) {
                    $('#'+tree.id+'.tree').jstree(true).select_node(strNode.split(','))
                })
            }
        }

        function treeIsSelected(array) {
            var boo = true;
            var arry = []
            $.each(array, function(key, value) {
                
                $.each($('.tree'), function (k, tree) {
                    if (boo = $('#'+tree.id+'.tree').jstree(true).is_selected(value))
                        return false                
                })

                if (boo) 
                    arry.push(value)
            })
            
            return arry
        }

        Array.prototype.unique=function(a){
            return function(){return this.filter(a)}}(function(a,b,c){return c.indexOf(a,b+1)<0
        })

    </script>
@endsection