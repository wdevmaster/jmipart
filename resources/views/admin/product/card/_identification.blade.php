<div class="card">
    <div class="card-header">
        {{ __('Identification') }}
    </div>
    <div class="card-body px-3">

        <div class="form-group">
            <div class="input-group">
                {{ Form::text(
                        'name', 
                        old('name'), 
                        [
                            'class' => 'form-control form-control-lg'.($errors->has('name') ? ' is-invalid': ''), 
                            'placeholder' => __('Product name')
                        ]
                    ) 
                }}
            </div>
            @if ($errors->has('name'))
                <div class="invalid-feedback d-block error-name">
                    {{ $errors->first('name') }}
                </div>
            @endif
        </div>
        
        <div class="form-group">
           <div class="input-group">
               {{ Form::text(
                       'slug', 
                       old('slug'), 
                       [
                           'class' => 'form-control form-control-sm'.($errors->has('slug') ? ' is-invalid': ''), 
                           'placeholder' => __('URL (leave empty to auto-generate from name)')
                       ]
                   ) 
               }}
           </div>
           @if ($errors->has('slug'))
               <div class="invalid-feedback d-block error-slug">
                   {{ $errors->first('slug') }}
               </div>
           @endif
        </div>

        <hr>

        <div class="form-group mb-0">
            <div class="input-group">
                {{ Form::text(
                        'sku', 
                        old('name'), 
                        [
                            'class' => 'form-control '.($errors->has('sku') ? ' is-invalid': ''), 
                            'placeholder' => __('SKU (product code)')
                        ]
                    ) 
                }}
            </div>
            @if ($errors->has('sku'))
                <div class="invalid-feedback d-block error-sku">
                    {{ $errors->first('sku') }}
                </div>
            @endif
        </div>

    </div>
</div>
    