<div class="card mt-3">
    <div class="card-header">
        {{ __('Pricing') }}
    </div>
    <div class="card-body px-3">

        <div class="form-group mb-0">
            <div class="input-group">
                {{ Form::text(
                        'price', 
                        old('price'), 
                        [
                            'class' => 'currency form-control'.($errors->has('price') ? ' is-invalid': ''), 
                            'placeholder' => __('Price'),
                        ]
                    ) 
                }}

            </div>
            @if ($errors->has('price'))
                <div class="invalid-feedback d-block error-price">
                    {{ $errors->first('price') }}
                </div>
            @endif
        </div>

    </div>
</div>

@section('scripts')
    @parent
    <script src="{{ asset('js/jquery.inputmask.min.js') }}"></script>
    <script>
        $(".currency").inputmask("currency", {
            groupSeparator: ',',
            placeholder: '0.00',
            numericInput: true,
            autoGroup: false
        })
    </script>    
@endsection
    