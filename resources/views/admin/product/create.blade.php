@extends('layouts.app')

@section('title')
    {{ __('Create new product') }}
@stop

@section('content')
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">{{ __('Product Details') }}</h1>
    </div>

    {!! Form::model($product, ['route' => 'product.store', 'autocomplete' => 'off', 'files' => true]) !!}
        @include('admin.product._form')

        <div class="card">
            <div class="card-body">
                <div class="form-group text-right">
                    <button class="btn btn-success">{{ __('Create product') }}</button>
                    <a href="#" onclick="history.back();" class="btn btn-link text-muted">{{ __('Cancel') }}</a>
                </div>
            </div>
        </div>
    {!! Form::close() !!}

</main>
@endsection

@section('sidebar')
    @parent
    @include('elements._sidebar')
@endsection