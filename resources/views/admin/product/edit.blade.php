@extends('layouts.app')

@section('title')
    {{ __('Editing') }} {{ $product->name }}
@stop

@section('content')
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">{{__('Category Tree Data')}}</h1>
    </div>

    {!! Form::model($product, ['route' => ['product.update', $product->id], 'autocomplete' => 'off', 'files' => true, 'method' => 'PUT']) !!}
        @include('admin.product._form')

        <div class="card">
            <div class="card-body">
                <div class="form-group text-right">
                    <button class="btn btn-success">{{ __('Save') }}</button>
                    <a href="#" onclick="history.back();" class="btn btn-link text-muted">{{ __('Cancel') }}</a>
                </div>
            </div>
        </div>
    {!! Form::close() !!}

</main>
@endsection

@section('sidebar')
    @parent
    @include('elements._sidebar')
@endsection