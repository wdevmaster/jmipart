@if($for)
    <div class="form-group row">
        <label class="form-control-label col-md-2">{{ __('Address for') }}:</label>

        <div class="col-md-10">
            <strong>{{ $for->name }}</strong>
        </div>
        {{ Form::hidden('for', Helper::shorten(get_class($for))) }}
        {{ Form::hidden('forId', $for->id) }}
    </div>
@endif


<div class="form-group row">
    <label class="form-control-label col-md-2">{{ __('Address type') }}</label>
    <div class="col-md-10">
        @foreach($types as $key => $value)
            @php 
                 
                $old = $address->type == null ? 
                       'billing' :
                       $address->type->value()
            @endphp
            <div class="custom-control custom-radio custom-control-inline">
                <input 
                    type="radio" 
                    id="{{$key}}" 
                    name="type" 
                    value="{{$key}}"
                    class="custom-control-input{{($errors->has('type') ? ' is-invalid': '')}}"
                    {{$old === $key ? 'checked' : ''}}
                >
                <label class="custom-control-label" for="{{$key}}">{{ $value }}</label>
            </div> 
        @endforeach
        @if ($errors->has('type'))
            <div class="invalid-feedback d-block error-type">
                {{ $errors->first('type') }}
            </div>
        @endif
    </div>
</div>

<div class="form-group">
    <div class="input-group">
        {{ Form::text(
                'name', 
                old('name'), 
                [
                    'class' => 'form-control form-control-lg'.($errors->has('name') ? ' is-invalid': ''), 
                    'placeholder' => __('Name')
                ]
            ) 
        }}
    </div>
    @if ($errors->has('name'))
        <div class="invalid-feedback d-block error-name">
            {{ $errors->first('name') }}
        </div>
    @endif
</div>

<div id='geocoder' class='geocoder'></div>

<hr>

<div class="row">
    <div class="col-md-6">
        <div class="jumbotron py-4 mb-1">
            <label class="form-control-label">{{ __('Address') }}</label>
            <div class="form-group">
                {{ Form::text('address', null, [
                    'id'    => 'data-address',
                    'class' => 'form-control'.($errors->has('address') ? ' is-invalid': '')
                ]) }}
                
                @if ($errors->has('address'))
                    <div class="invalid-feedback d-block error-address">
                        {{ $errors->first('address') }}
                    </div>
                @endif
            </div>

            <label class="form-control-label">{{ __('Postal/Zip Code') }}</label>
            <div class="form-group">
                {{ Form::text('postalcode', null, [
                    'id'    => 'data-postcode',
                    'class' => 'form-control'.($errors->has('postalcode') ? ' is-invalid': '')
                ]) }}

                @if ($errors->has('postalcode'))
                    <div class="invalid-feedback d-block error-postalcode">
                        {{ $errors->first('postalcode') }}
                    </div>
                @endif
            </div>

            <label class="form-control-label">{{ __('City') }}</label>
            <div class="form-group">
                {{ Form::text('city', null, [
                    'id'    => 'data-place',
                    'class' => 'form-control'.($errors->has('city') ? ' is-invalid': '')
                ]) }}

                @if ($errors->has('city'))
                    <div class="invalid-feedback d-block error-city">
                        {{ $errors->first('city') }}
                    </div>
                @endif
            </div>

            <label class="form-control-label">{{ __('State/Province') }}</label>
            <div class="form-group">
                {{ Form::hidden('province_code', null, [
                    'id'    => 'data-region-code',
                ]) }}

                {{ Form::text('province', null, [
                    'id'    => 'data-region',
                    'class' => 'form-control'.($errors->has('province') ? ' is-invalid': '')
                ]) }}

                @if ($errors->has('province'))
                    <div class="invalid-feedback d-block error-province">
                        {{ $errors->first('province') }}
                    </div>
                @endif
            </div>

            <label class="form-control-label">{{ __('Country') }}</label>
            <div class="form-group">
                {{  Form::select('country_id', $countries->pluck('name', 'id'), old('country_id'), [
                        'id'    => 'data-country',
                        'class' => 'form-control'.($errors->has('country_id') ? ' is-invalid': '')
                    ])
                }} 

                @if ($errors->has('country_id'))
                    <div class="invalid-feedback d-block error-country_id">
                        {{ $errors->first('country_id') }}
                    </div>
                @endif
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div id='map'></div>
    </div>
</div>
