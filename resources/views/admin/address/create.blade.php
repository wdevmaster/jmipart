@extends('layouts.app')

@section('title')
    {{ __('Create new address') }}
@stop

@section('content')
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">{{ __('Address Details') }}</h1>
    </div>

    <div class="card">
        <div class="card-body">
            {!! Form::model($address, ['route' => 'address.store', 'autocomplete' => 'off']) !!}

                @include('admin.address._form')
            
                <hr>
                <div class="form-group text-right">
                    <div class="custom-control custom-checkbox d-flex">
                        <input
                            type="checkbox" 
                            class="custom-control-input{{($errors->has('parent_id') ? ' is-invalid': '')}}" 
                            id="checkIsDefault" 
                            name="is_default" 
                            value="1" 
                            {{ $address->is_default ? 'checked' : ''}}>
                        <label class="custom-control-label" for="checkIsDefault"><b>{{__('Default address')}}</b></label>
                        @if ($errors->has('is_default'))
                            <div class="invalid-feedback d-block error-is_default">
                                {{ $errors->first('is_default') }}
                            </div>
                        @endif
                    </div>
                    <button class="btn btn-success">{{ __('Create address') }}</button>
                    <a href="#" onclick="history.back();" class="btn btn-link text-muted">{{ __('Cancel') }}</a>
                </div>
            {!! Form::close() !!}
        </div>
      </div>
</main>
@endsection

@section('sidebar')
    @parent
    @include('elements._sidebar')
@endsection

@section('styles')
    @parent
    <script src='https://api.tiles.mapbox.com/mapbox-gl-js/v1.3.1/mapbox-gl.js'></script>
    <link href='https://api.tiles.mapbox.com/mapbox-gl-js/v1.3.1/mapbox-gl.css' rel='stylesheet' />
    <style>
        #map {
            width: 100%;
            height: 460px;
        }
        .mapboxgl-ctrl-geocoder { 
            box-shadow: none!important;
            min-width:100%!important;
            border: 1px solid #ced4da;
        }
    </style>
@endsection

@section('scripts')
    @parent
    <script src='https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v4.4.1/mapbox-gl-geocoder.min.js'></script>
    <link rel='stylesheet' href='https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v4.4.1/mapbox-gl-geocoder.css' type='text/css' />
    <script>
        mapboxgl.accessToken = 'pk.eyJ1IjoibTdndWVsIiwiYSI6ImNqbm9wNGtiYjAweDkza3BjMnV1bTNqOGoifQ.mKudjUF_BtXRIGZ3PtIc2g';
        var map = new mapboxgl.Map({
        container: 'map',
        style: 'mapbox://styles/mapbox/streets-v11',
        center: [-79.4512, 43.6568],
        zoom: 13
        });
        
        var geocoder = new MapboxGeocoder({
            accessToken: mapboxgl.accessToken,
            mapboxgl: mapboxgl,
            placeholder: "{{__('Search address')}}"
        });
    
        document.getElementById('geocoder').appendChild(geocoder.onAdd(map));

        geocoder.on('result', function(ev){
            $('#data-region-code').val('')
            $('#data-region').val('')
            $('#data-place').val()
            $('#data-postcode').val('')
            $('#data-address').val('')

            let placeName = ev.result.place_name

            $.each(ev.result.context, function(k, row) {
                let id = row.id.split('.', 1)[0]

                if (id == 'country') {
                    $('#data-'+id+' > option[value='+row.short_code.toUpperCase()+']').attr("selected",true)
                    placeName = placeName.split(row.text).join('') 
                }else if (id == 'region') {
                    $('#data-'+id+'-code').val(row.short_code)
                    $('#data-'+id).val(row.text)
                    placeName = placeName.split(row.text).join('') 
                }else if (id == 'place') {
                    $('#data-'+id).val(row.text)
                    placeName = placeName.split(row.text).join('') 
                }else if (id == 'postcode') {
                    $('#data-'+id).val(row.text)
                    placeName = placeName.split(row.text).join('') 
                }
            })

            $('#data-address').val(placeName.split(' ,').join(''))
        })
    </script>
@endsection