@extends('layouts.app')

@section('title')
    {{ __('Users') }}
@stop

@section('content')
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">@yield('title')</h1>
        @can('user.create')
            <div class="btn-toolbar mb-2 mb-md-0">
                <div class="btn-group mr-2">
                    <a href="{{route('user.create')}}" class="btn btn-sm btn-outline-success">
                        {{ __('New User') }}
                    </a>
                </div>
            </div>
        @endcan
    </div>

    

    <div class="card pb-2">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-striped table-sm table-crud">
                    <thead> 
                        <tr>
                            <th>{{__('E-mail')}}</th>
                            <th>{{__('Name')}}</th>
                            <th>{{__('Registered')}}</th>
                            <th>{{__('Last login')}}</th>
                            <th style="width: 15%">&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($users as $user)
                            <tr class="row-{{ $user->id }}">
                                <td>{{ $user->email }}</td>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->created_at->diffForHumans() }}</td>
                                <td>{{ $user->last_login_at ? $user->last_login_at->diffForHumans() : __('never') }}</td>
                                <td class="text-left">
                                    @if(Auth::user()->hasRole('admin') || !$user->hasRole('admin'))
                                        @can('user.edit')
                                            <a 
                                                href="{{ route('user.edit', $user->id) }}"
                                                class="btn btn-primary btn-edit">
                                                    {{__('Edit')}}
                                            </a>
                                        @endcan
                                        @can('user.delete')
                                            @if ($user->id != Auth::id())
                                                <button 
                                                    type="button" 
                                                    data-id="{{ $user->id }}"
                                                    class="btn btn-danger btn-delete">
                                                        {{__('Remove')}}
                                                </button>
                                            @endif
                                        @endcan
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
      </div>
</main>
@endsection

@section('sidebar')
    @parent
    @include('elements._sidebar')
@endsection

@section('scripts')
    @parent
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
    <script>
        $('.btn-delete').click(function(e) {
            let redirectUrl = "{{route('user.index')}}"
            let id = $(this).data('id')

            $('.row-'+id).toggleClass('table-danger')

            Swal.fire({
                title: "{{__('Are you sure?')}}",
                html: "{{__('You won\'t be able to revert this!')}}",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#d33',
                confirmButtonText: "{{__('Yes, delete it!')}}",
                cancelButtonText: "{{__('No, cancel!')}}",
                showLoaderOnConfirm: true,
                preConfirm: () => {
                    return fetch("{{ route('user.delete') }}", {
                        headers: {
                            'Accept': 'application/json',
                            'Content-Type': 'application/json'
                        },
                        method: "DELETE",
                        body: JSON.stringify({ 
                            '_token': '{{csrf_token()}}',
                            'id': id
                        })
                    })
                },
                allowOutsideClick: () => !Swal.isLoading()
            }).then((result) => {
                if (result.value) {
                    Swal.fire({
                        title: "{{__('Deleted!')}}",
                        text: "{{__('Your file has been deleted.')}}",
                        type:'success',
                        confirmButtonText: "{{__('Ok')}}",
                    }).then(() => {
                        window.location.href = redirectUrl
                    })
                }
                $('.row-'+id).toggleClass('table-danger')
            })
        })
    </script>
@endsection