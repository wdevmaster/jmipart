<div class="form-group">
    <div class="input-group">
        {{ Form::text(
                'name', 
                old('name'), 
                [
                    'class' => 'form-control'.($errors->has('name') ? ' is-invalid': ''), 
                    'placeholder' => __('Full name')
                ]
            ) 
        }}
    </div>
    @if ($errors->has('name'))
        <div class="invalid-feedback d-block error-name">
            {{ $errors->first('name') }}
        </div>
    @endif
</div>

<hr>

<div class="form-group">
    <div class="input-group">
        {{ Form::email(
                'email', 
                old('email'), 
                [
                    'class' => 'form-control'.($errors->has('email') ? ' is-invalid': ''), 
                    'placeholder' => __('E-mail address')
                ]
            ) 
        }}
    </div>
    @if ($errors->has('email'))
        <div class="invalid-feedback d-block error-email">
            {{ $errors->first('email') }}
        </div>
    @endif
</div>

<div class="form-group">
    <div class="input-group">
        {{ Form::password(
                'password', 
                [
                    'class' => 'form-control'.($errors->has('password') ? ' is-invalid': ''), 
                    'autocomplete'  => 'new-password',
                    'placeholder' => isset($user) &&  $user->exists  ? 
                    __('Type new password if you want to change it') :
                    __('Enter password')
                ]
            ) 
        }}
    </div>
    @if ($errors->has('password'))
        <div class="invalid-feedback d-block error-password">
            {{ $errors->first('password') }}
        </div>
    @endif
</div>

<div class="form-group">
    <div class="col-sm-12">
        <div class="form-check form-check-inline">
            {{  Form::radio(
                    'is_active', 
                    1, 
                    isset($user) ? 
                    $user->is_active : 
                    old('is_active') ?: true 
                ) 
            }}
            <label class="form-check-label" for="is_active">{{ __('Active') }}</label>
        </div>

        <div class="form-check form-check-inline">
            {{  Form::radio(
                    'is_active', 
                    0, 
                    isset($user) ? 
                    !$user->is_active : 
                    !old('is_active') ?: false 
                ) 
            }}
            <label class="form-check-label" for="is_active">{{ __('Inactive') }}</label>
        </div>
    </div>
</div>

<h3>{{ __('Roles') }}</h3>

<div class="form-group row">
    @foreach($roles as $role)
        <div class="col-6 col-sm-2 @unless(($loop->index + 5) % 5)offset-sm-1 @endunless">
            {{  Form::checkbox(
                    "roles[{$role->slug}]", 
                    1, 
                    isset($user) ? $user->hasRole($role->slug) : false, 
                    [
                        'class' => 'form-check-input',
                        'id' => $role->slug
                    ]
                ) 
            }}
            <label 
                class="form-check-label {{ $errors->has('roles') ? 'text-danger' : ''}}" 
                for="{{ $role->slug }}">
                    {{ $role->name }}
            </label>
        </div>
    @endforeach

    <div class="col-12">
        @if ($errors->has('roles'))
            <div class="invalid-feedback d-block error-roles">
                {{ $errors->first('roles') }}
            </div>
        @endif
    </div>
</div>