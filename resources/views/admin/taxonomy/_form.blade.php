<div class="form-group">
    <div class="input-group">
        {{ Form::text(
                'name', 
                old('name'), 
                [
                    'class' => 'form-control form-control-lg'.($errors->has('name') ? ' is-invalid': ''), 
                    'placeholder' => __('Name of the Category Tree')
                ]
            ) 
        }}
    </div>
    @if ($errors->has('name'))
        <div class="invalid-feedback d-block error-name">
            {{ $errors->first('name') }}
        </div>
    @endif
</div>

<hr>

<div class="form-group">
   <div class="input-group">
       {{ Form::text(
               'slug', 
               old('slug'), 
               [
                   'class' => 'form-control form-control-sm'.($errors->has('slug') ? ' is-invalid': ''), 
                   'placeholder' => __('URL (leave empty to auto-generate from name)')
               ]
           ) 
       }}
   </div>
   @if ($errors->has('slug'))
       <div class="invalid-feedback d-block error-slug">
           {{ $errors->first('slug') }}
       </div>
   @endif
</div>