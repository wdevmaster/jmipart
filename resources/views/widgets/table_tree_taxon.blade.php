<table class="table">
    <tbody>
    @forelse($taxons as $taxon)
        <tr class="row-{{$taxon->id}}" data-toggle="collapse" data-target="#children-{{$taxon->id}}" aria-expanded="false">
            <td style="width: 75%">
                @if($src = $taxon->getThumbnailUrl())
                    <img src="{{$src}}">
                @endif
                @can('taxon.edit')
                    <a href="{{ route('taxon.edit', [$taxonomy, $taxon]) }}">{{ $taxon->name }}</a>
                @else
                    {{ $taxon->name }}
                @endcan
                <span  
                    class="badge badge-pill badge-light text-muted">
                    {{ $taxon->children->count() }}
                </span >
            </td>
            <td style="width: 25%">
                <div class="d-flex justify-content-between">
                @can('taxon.create')
                    <a href="{{ route('taxon.create', $taxonomy) }}?parent={{$taxon->id}}" 
                        class="btn btn-sm btn-outline-success btn-create">
                        {{ __('Add Child :category', ['category' => str_singular($taxon->name)]) }}
                    </a>
                @endcan
                @can('taxon.delete')
                    <button 
                        type="button" 
                        data-id="{{ $taxon->id }}"
                        data-type="taxon"
                        class="btn btn-danger btn-delete btn-sm">
                            {{__('Remove')}}
                    </button>
                @endcan
                </div>
            </td>
        </tr>
        @if ($taxon->children->isNotEmpty())
        <tr class="collapse" id="children-{{$taxon->id}}">
            <td colspan="2" class="pl-5 pr-0">
                @component('widgets.table_tree_taxon', [
                    'taxonomy' => $taxonomy,
                    'taxons' => $taxon->children
                ])
                @endcomponent
            </td>
        </tr>
        @endif
    @empty
        <tr>
            <td colspan="2" class="text-center">{{ __('No category tree has been registered yet') }}</td>
        </tr>
    @endforelse
    </tbody>
</table>