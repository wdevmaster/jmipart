
@php
    if ($type == 'draft')
        $statu = 'secondary';
    elseif ($type == 'inactive')
        $statu = 'danger';
    elseif ($type == 'active')
        $statu = 'success';
@endphp

<span class="badge badge-{{$statu}}">
    {{ $slot }}
</span>