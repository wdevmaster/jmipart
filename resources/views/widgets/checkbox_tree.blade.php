<ul>
    @foreach ($taxons as $taxon)
        <li id="{{ $taxon->id }}">
            {{ $taxon->name }}
            @if ($taxon->children->isNotEmpty())
                @component('widgets.checkbox_tree', [
                    'taxons' => $taxon->children
                ])
                @endcomponent
            @endif
        </li>
    @endforeach
</ul>
