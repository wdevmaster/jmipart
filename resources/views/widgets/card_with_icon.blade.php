<div class="card" id="{{isset($id) ? $id : ''}}">
    <div class="card-body bg-light">
        <div class="h1 text-muted text-right m-b-2">
            @if (isset($iconSlot))
                {!! $iconSlot !!}
            @endif
        </div>
        <div class="h4 m-b-0 text-uppercase">
            {{ $slot }}
        </div>
        <small class="text-muted text-uppercase font-weight-bold">{{ $subtitle }}</small>
    </div>
</div>