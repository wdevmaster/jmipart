@if ($paginator->hasPages())
    <div class="shop_toolbar t_bottom">
        <div class="pagination">
            <ul>
                @if (!$paginator->onFirstPage())
                    <li><a href="{{ $paginator->previousPageUrl() }}">&lsaquo;</a></li>
                @endif

                @foreach ($elements as $element)
                    {{-- "Three Dots" Separator --}}
                    @if (is_string($element))
                        <li class="current"><span>{{ $element }}</span></li>
                    @endif

                    {{-- Array Of Links --}}
                    @if (is_array($element))
                        @foreach ($element as $page => $url)
                            @if ($page == $paginator->currentPage())
                                <li class="current"><span>{{ $page }}</span></li>
                            @else
                                <li><a href="{{ $url }}">{{ $page }}</a></li>
                            @endif
                        @endforeach
                    @endif
                @endforeach

                @if ($paginator->hasMorePages())
                    <li><a href="{{ $paginator->nextPageUrl() }}">&rsaquo;</a></li>
                @endif
            </ul>
        </div>
    </div>
@endif
