<?php

namespace Components\Contracts;

interface Buyable
{
    /**
     * Returns the id of the _thing_
     *
     * @return int
     */
    public function getId();

    /**
     * Returns the name of the _thing_
     *
     * @return string
     */
    public function getName();

    /**
     * Returns the price of the item; float is temporary!!
     * 
     * @return float
     */
    public function getPrice();

    /**
     * Returns whether the item has an image
     *
     * @return bool
     */
    public function hasImage();

    /**
     * Returns the URL of the item's thumbnail image, or null if there's no image
     *
     * @return string|null
     */
    public function getThumbnailUrl();

    /**
     * Returns the URL of the item's (main) image, or null if there's no image
     *
     * @return string|null
     */
    public function getImageUrl();
}
