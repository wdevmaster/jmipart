<?php

namespace Components\Cart;

use Illuminate\Support\ServiceProvider;
use Components\Cart\Contracts\CartManager as CartManagerContract;
use Components\Cart\CartManager;

class CartServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        parent::register();
        $this->app->bind(CartManagerContract::class, CartManager::class);

        $this->app->singleton('cart', function ($app) {
            return $app->make(CartManagerContract::class);
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
