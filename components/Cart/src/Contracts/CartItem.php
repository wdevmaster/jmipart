<?php

namespace Components\Cart\Contracts;

use Components\Contracts\CheckoutSubjectItem;

interface CartItem extends CheckoutSubjectItem
{
}
